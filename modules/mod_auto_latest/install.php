<?php

// ========================================================================== //

function info_module_mod_auto_latest(){

    //
    // Описание модуля
    //

    //Заголовок (на сайте)
    $_module['title']        = 'Auto: последние объявления';

    //Название (в админке)
    $_module['name']         = 'Auto: последние объявления';

    //описание
    $_module['description']  = 'Последние объявления Auto';

    //ссылка (идентификатор)
    $_module['link']         = 'mod_auto_latest';

    //позиция
    $_module['position']     = 'maintop';

    //автор
    $_module['author']       = 'k.netesin';

    //текущая версия
    $_module['version']      = '1.0';

    //
    // Настройки по-умолчанию
    //
    $_module['config'] = array();

    return $_module;

}

// ========================================================================== //

function install_module_mod_auto_latest(){

    return true;

}

// ========================================================================== //

function upgrade_module_mod_auto_latest(){

    return true;

}

// ========================================================================== //

?>