<?php

function mod_auto_latest($module_id, $cfg)
{
    $inCore = cmsCore::getInstance();
    $inPage = cmsPage::getInstance();

    define('COMPONENT_NAME', 'auto');
    define('COMPONENT_HOME_URL', '/' . COMPONENT_NAME);
    define('COMPONENT_TEMPLATE_PATH', 'components/auto/admin');
    define('COMPONENT_TEMPLATE_CONTAINER', '_auto_admin_container.tpl');

    $inPage->addHeadCSS('components/' . COMPONENT_NAME . '/styles/default/css/modules.css?v=1');

    cmsCore::loadModel(COMPONENT_NAME);
    $inCore->loadLanguage('components/' . COMPONENT_NAME);
    global $_LANG;

    $component_data_array = include PATH . '/components/' . COMPONENT_NAME . '/includes/component_data.php';
    $routes = $component_data_array['routes'];
    $component_data = $component_data_array['component_data'];

    $model = new cms_model_auto($component_data, $routes, $_LANG);

    $ads = $model->getAdsList(null, 6, false);

    if(!count($ads['list'])){
        return false;
    }

	cmsPage::initTemplate('modules/' . COMPONENT_NAME, 'mod_auto_latest')->
        assign(array(
            'routes'         => $routes,
            'component_data' => $component_data,
            'currency'       => $model->_currency,
            'ads'            => $ads
        ))->display('mod_auto_latest.tpl');

	return true;
}
?>