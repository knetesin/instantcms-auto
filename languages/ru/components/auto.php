<?php
if(!defined('VALID_CMS')) { die('ACCESS DENIED'); }

// region index page lang

$_LANG['TEMPLATE_INDEX_PAGE_TITLE'] = 'Автодоска';
$_LANG['TEMPLATE_INDEX_PAGE_KEYWORDS'] = 'Автодоска';
$_LANG['TEMPLATE_INDEX_PAGE_DESCRIPTION'] = 'Автодоска';

// endregion

// region vendor index page
$_LANG['TEMPLATE_VENDOR_INDEX_PAGE_PATHWAY'] = 'Список моделей %s';
$_LANG['TEMPLATE_VENDOR_INDEX_PAGE_TITLE'] = 'Список моделей %s';
$_LANG['TEMPLATE_VENDOR_INDEX_PAGE_KEYWORDS'] = 'Модели %1$s, список моделей авто %1$s';
$_LANG['TEMPLATE_VENDOR_INDEX_PAGE_DESCRIPTION'] = 'Вы можете найти и приобрести %1$s по доступной цене. Получить самые последние объявления';


$_LANG['TEMPLATE_SHOWVENDOR_PAGE_PATHWAY'] = '%s';
$_LANG['TEMPLATE_SHOWVENDORLIST_PAGE_PATHWAY'] = '%s';
// endregion

// region modelList index page
$_LANG['TEMPLATE_MODELLIST_INDEX_PAGE_PATHWAY'] = 'Список объявлений %s %s';
$_LANG['TEMPLATE_MODELLIST_INDEX_PAGE_TITLE'] = 'Список объявлений %s %s';
$_LANG['TEMPLATE_MODELLIST_INDEX_PAGE_KEYWORDS'] = 'Объявления %1$s %2$s';
$_LANG['TEMPLATE_MODELLIST_INDEX_PAGE_DESCRIPTION'] = 'Список объявлений %1$s %2$s';
// endregion


// region cabinet index page
$_LANG['TEMPLATE_CABINET_INDEX_PAGE_PATHWAY'] = 'Личный кабинет';
$_LANG['TEMPLATE_CABINET_INDEX_PAGE_TITLE'] = 'Личный кабинет';
$_LANG['TEMPLATE_CABINET_INDEX_PAGE_KEYWORDS'] = '';
$_LANG['TEMPLATE_CABINET_INDEX_PAGE_DESCRIPTION'] = '';
// endregion
// region cabinet ads list page
$_LANG['TEMPLATE_CABINET_ADS_LIST_PAGE_PATHWAY'] = 'Список объявлений';
$_LANG['TEMPLATE_CABINET_ADS_LIST_PAGE_TITLE'] = 'Список объявлений';
$_LANG['TEMPLATE_CABINET_ADS_LIST_PAGE_KEYWORDS'] = '';
$_LANG['TEMPLATE_CABINET_ADS_LIST_PAGE_DESCRIPTION'] = '';
// endregion
// region show page
$_LANG['TEMPLATE_SHOW_PAGE_PATHWAY'] = 'Просмотр объявления #%d';
$_LANG['TEMPLATE_SHOW_PAGE_TITLE'] = 'Объявление о продаже %s %d года в г.%s';
$_LANG['TEMPLATE_SHOW_PAGE_KEYWORDS'] = 'Генерируемые кейворды %2$s %3$s г.%1$s %4$s';
$_LANG['TEMPLATE_SHOW_PAGE_DESCRIPTION'] = 'Продам %2$s %3$s в г. %1$s . Цена: %4$s';

$_LANG['TEMPLATE_SHOW_TITLE'] = 'Продажа %s %s';


// region cabinet ad edit page
$_LANG['TEMPLATE_EDIT_AD_PAGE_PATHWAY'] = 'Редактирование объявления #%s';

$_LANG['TEMPLATE_EDIT_AD_PAGE_TITLE'] = 'Редактирование объявления #%s';
$_LANG['TEMPLATE_EDIT_AD_PAGE_KEYWORDS'] = '';
$_LANG['TEMPLATE_EDIT_AD_PAGE_DESCRIPTION'] = '';
// endregion

// region cabinet my questions page
$_LANG['TEMPLATE_CABINET_MY_QUESTIONS_PAGE_PATHWAY'] = 'Мои вопросы';

$_LANG['TEMPLATE_CABINET_MY_QUESTIONS_PAGE_TITLE'] = 'Мои вопросы';
$_LANG['TEMPLATE_CABINET_MY_QUESTIONS_PAGE_KEYWORDS'] = '';
$_LANG['TEMPLATE_CABINET_MY_QUESTIONS_PAGE_DESCRIPTION'] = '';
// endregion



// region add page lang
$_LANG['TEMPLATE_ADD_PAGE_PATHWAY'] = 'Добавить объявление';

$_LANG['TEMPLATE_ADD_PAGE_TITLE'] = 'Добавить объявление';
$_LANG['TEMPLATE_ADD_PAGE_KEYWORDS'] = 'Новое объявление, добавить новое объявление';
$_LANG['TEMPLATE_ADD_PAGE_DESCRIPTION'] = 'Страница добавления нового объявления';
// endregion


// region searchFilter
$_LANG['TEMPLATE_SEARCHFILTER_VENDOR_TITLE'] = 'Фирма:';
$_LANG['TEMPLATE_SEARCHFILTER_MODEL_TITLE'] = 'Модель:';
$_LANG['TEMPLATE_SEARCHFILTER_PRICE_TITLE'] = 'Цена:';
$_LANG['TEMPLATE_SEARCHFILTER_YEAR_TITLE'] = 'Год:';
$_LANG['TEMPLATE_SEARCHFILTER_VOLUME_TITLE'] = 'Объем:';
$_LANG['TEMPLATE_SEARCHFILTER_FUEL_TITLE'] = 'Топливо:';
$_LANG['TEMPLATE_SEARCHFILTER_TRANS_TITLE'] = 'КПП:';
$_LANG['TEMPLATE_SEARCHFILTER_DRIVE_TITLE'] = 'Привод:';
// endregion

// region controlLinks
$_LANG['TEMPLATE_CONTROLLINKS_LIST_LIM'] = 'Все объявления %s';
$_LANG['TEMPLATE_CONTROLLINKS_LIST'] = 'Все объявления';
$_LANG['TEMPLATE_CONTROLLINKS_NEWAUTO_LIM'] = 'Новые автомобили %s';
$_LANG['TEMPLATE_CONTROLLINKS_NEWAUTO'] = 'Новые автомобили';
$_LANG['TEMPLATE_CONTROLLINKS_ADD'] = 'Подать объявление';
$_LANG['TEMPLATE_CONTROLLINKS_CABINET'] = 'Личный кабинет';
// endregion

// region add
$_LANG['TEMPLATE_ADD_PRICEBLOCK_TITLE'] = 'Цена и контактная информация';
$_LANG['TEMPLATE_ADD_TECHDATA_TITLE'] = 'Технические данные';

$_LANG['TEMPLATE_ADD_TECHDATA_VENDOR_TITLE'] = 'Производитель:';
$_LANG['TEMPLATE_ADD_TECHDATA_MODEL_TITLE'] = 'Модель:';
$_LANG['TEMPLATE_ADD_TECHDATA_DATE_CONSTRUCT_TITLE'] = 'Год выпуска:';
$_LANG['TEMPLATE_ADD_TECHDATA_WHEEL_TITLE'] = 'Руль:';
$_LANG['TEMPLATE_ADD_TECHDATA_MILEAGE_TITLE'] = 'Пробег (км):';
$_LANG['TEMPLATE_ADD_TECHDATA_VOLUME_TITLE'] = 'Объем (литров):';
$_LANG['TEMPLATE_ADD_TECHDATA_FUEL_TITLE'] = 'Тип топлива:';
$_LANG['TEMPLATE_ADD_TECHDATA_TRANSMISSION_TITLE'] = 'Коробка передач:';
$_LANG['TEMPLATE_ADD_TECHDATA_DRIVE_TITLE'] = 'Привод:';
$_LANG['TEMPLATE_ADD_TECHDATA_SPECIAL_NOTES_TITLE'] = 'Особые отметки:';
$_LANG['TEMPLATE_ADD_TECHDATA_DESCRIPTION_TITLE'] = 'Описание:';
$_LANG['TEMPLATE_ADD_TECHDATA_DESCRIPTION_NOTE'] = 'В случае размещение рекламы и ссылок на другие сайты, объявления будут удаляться, а контактные данные блокироваться! В одном объявлении должна содержаться информация не более чем об одном автомобиле.';
$_LANG['TEMPLATE_ADD_TECHDATA_GT_DESCRIPTION_NOTE'] = 'Опишите, какие именно доработки были сделаны. Желательно указать мощность и вес автомобиля.';
$_LANG['TEMPLATE_ADD_ACCESS_ISSUES_NOTE'] = 'Разрешить покупателям задавать мне вопросы на странице объявления';
$_LANG['TEMPLATE_ADD_EMAIL_NOTE'] = 'E-mail нужен для отправки уведомлений и регистрационных данных, он не будет показываться в объявлении';

$_LANG['TEMPLATE_ADD_SELECT_VENDOR'] = 'выберите фирму';
$_LANG['TEMPLATE_ADD_SELECT_MODEL'] = 'выберите модель';
$_LANG['TEMPLATE_ADD_SELECT_DATE'] = 'выберите дату';
$_LANG['TEMPLATE_ADD_CHECKBOX_IS_UNDOCUMENTED'] = 'без документов';
$_LANG['TEMPLATE_ADD_CHECKBOX_IS_BROKEN'] = 'битый или не на ходу';
$_LANG['TEMPLATE_ADD_CHECKBOX_IS_CUT'] = 'конструктор';
$_LANG['TEMPLATE_ADD_CHECKBOX_IS_GT'] = 'это GT-машина, либо она подверглась доработке (тюнингу)';

// Ошибки формы
$_LANG['TEMPLATE_ADD_FORM_ERROR_VENDOR'] = 'Укажите производителя';
$_LANG['TEMPLATE_ADD_FORM_ERROR_MODEL'] = 'Укажите модель';
$_LANG['TEMPLATE_ADD_FORM_ERROR_DATE_CONSTRUCT'] = 'Укажите год выпуска из списка';
$_LANG['TEMPLATE_ADD_FORM_ERROR_CAPTCHA'] = 'Проверочный код указан не верно';
$_LANG['TEMPLATE_ADD_FORM_ERROR_VOLUME_TOOMUCH'] = 'Объем двигателя не может быть больше 10 литров';
$_LANG['TEMPLATE_ADD_FORM_ERROR_STATUS'] = 'Укажите статус автомобиля';
$_LANG['TEMPLATE_ADD_FORM_ERROR_INCORRECT_EMAIL'] = 'Email был указан неверно';
$_LANG['TEMPLATE_ADD_FORM_ERROR_EXISTS_EMAIL'] = 'Указанный email уже существует';
$_LANG['TEMPLATE_ADD_FORM_ERROR_REGION'] = 'Укажите регион продажи';
$_LANG['TEMPLATE_ADD_FORM_ERROR_CITY'] = 'Укажите город продажи';
$_LANG['TEMPLATE_ADD_FORM_ERROR_PHONE_NOT_SET'] = 'Телефон не указан';
$_LANG['TEMPLATE_ADD_FORM_ERROR_PHONE_INCORRECT'] = 'Телефон был указан не корректно';
// endregion

// region component data
$_LANG['COMPONENT_DATA_WHEEL_LEFT'] = 'левый';
$_LANG['COMPONENT_DATA_WHEEL_RIGHT'] = 'правый';

$_LANG['COMPONENT_DATA_FUELTYPE_PETROL'] = 'бензин';
$_LANG['COMPONENT_DATA_FUELTYPE_DIESEL'] = 'дизель';
$_LANG['COMPONENT_DATA_FUELTYPE_GAS'] = 'газ';
$_LANG['COMPONENT_DATA_FUELTYPE_HYBRID'] = 'гибрид';
$_LANG['COMPONENT_DATA_FUELTYPE_ELECTRO'] = 'электро';

$_LANG['COMPONENT_DATA_TRANSMISSION_AUTO'] = 'автомат';
$_LANG['COMPONENT_DATA_TRANSMISSION_MANUAL'] = 'механика';

$_LANG['COMPONENT_DATA_DRIVE_4WD'] = '4WD';
$_LANG['COMPONENT_DATA_DRIVE_FRONT'] = 'передний';
$_LANG['COMPONENT_DATA_DRIVE_REAR'] = 'задний';

$_LANG['COMPONENT_DATA_STATUS_IN_STOCK'] = 'в наличии';
$_LANG['COMPONENT_DATA_STATUS_IN_TRANSIT'] = 'в пути';
$_LANG['COMPONENT_DATA_STATUS_UNDER_THE_ORDER'] = 'под заказ';

$_LANG['COMPONENT_DATA_CURRENCY_RUB'] = 'руб';
$_LANG['COMPONENT_DATA_CURRENCY_USD'] = '$';
$_LANG['COMPONENT_DATA_CURRENCY_EUR'] = '€';



$_LANG['COMPONENT_DATA_MAIL_FOR_NEW_USER'] = 'Вы были автоматически зарегистрированы на портале. <br><br> Данные для авторизации: <br> Логин: <b>%s</b> <br> Пароль: <b>%s</b>';



// endregion


// region admin
$_LANG['ADMIN_SETTINGS']            = 'Настройки';
$_LANG['ADMIN_AD_LIST']             = 'Список объявлений';
$_LANG['ADMIN_AD_ADD']              = 'Добавить объявление';
$_LANG['ADMIN_AD_EDIT']             = 'Редактировать объявление';

$_LANG['ADMIN_TAB_SYSTEM'] = 'Системные';
$_LANG['ADMIN_TAB_AD'] = 'Объявления';
$_LANG['ADMIN_TAB_IMAGES'] = 'Изображения';
$_LANG['ADMIN_TAB_GEO'] = 'Геолокация';
$_LANG['ADMIN_TAB_SERVICES'] = 'Платные услуги';

$_LANG['ADMIN_TAB_PRESET'] = 'Предустановленный';
$_LANG['ADMIN_TAB_SET'] = 'Установленный';
$_LANG['ADMIN_TAB_DEFAULT'] = 'По умолчанию';

$_LANG['ADMIN_TAB_SYSTEM_DEBUG'] = 'Режим отладки';
$_LANG['ADMIN_TAB_SYSTEM_UPLOAD_DIR'] = 'Директория загрузки';
$_LANG['ADMIN_TAB_SYSTEM_DATE_START'] = 'Минимальный год выпуска авто';
$_LANG['ADMIN_TAB_SYSTEM_MODERATION_TYPE'] = 'Тип модерации';
$_LANG['ADMIN_TAB_SYSTEM_SHOWLOGO_CATS'] = 'Показывать логотипы категорий';
$_LANG['ADMIN_TAB_SYSTEM_SHOWNULL_CATS'] = 'Показывать пустые категории';

$_LANG['ADMIN_TAB_IMG_QUALITY'] = 'Качество изображений';
$_LANG['ADMIN_TAB_IMG_SAVE_ORIGINAL'] = 'Сохранять оригинал';
$_LANG['ADMIN_TAB_IMG_WITH_WATERMARK'] = 'Накладывать ватермарк';
$_LANG['ADMIN_TAB_IMG_WITH_WATERMARK_SMALL'] = 'Накладывать ватермарк на миниатюры';
// endregion admin

?>
