<div style="background: #c0c0c0; padding: 10px; border-radius: 5px; border: 1px solid #d3d3d3;">
    <h2>Вы были автоматически зарегистрированы на портале <a href="zend.gorod38.ru"></a> в разделе авто</h2>
    <hr>

    <h3>Ваши регистрационные данные следующие:</h3>
    <table>
        <tr>
            <td>Логин:</td>
            <td>
                <strong>{$login}</strong>
            </td>
        </tr>
        <tr>
            <td>Пароль:</td>
            <td>
                <strong>{$password}</strong>
            </td>
        </tr>
    </table>

    <div>
        <span>Для управления объявлением перейдите в <a href="zend.gorod38.ru/{$routes.cabinet.index}">Личный кабинет</a></span>
    </div>

</div>