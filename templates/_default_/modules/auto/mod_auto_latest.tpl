<div class="mod_auto_latest">
    <div class="mod_auto_latest-title">
        <a href="{$routes.index}">Продажа авто в ...</a>
    </div>

    <div class="mod_auto_latest-aditem-container">
        {foreach from=$ads.list item=ad}
            <div class="mod_auto_latest-aditem">
                <a href="{$ad.link}">
                    {if $ad.hasPhoto}
                        <img src="{$ad.photo.small}" alt="{$ad.title_vendor} {$ad.title_model}" title="{$ad.title_vendor} {$ad.title_model}">
                    {else}
                        <img src="/components/{$smarty.const.COMPONENT_NAME}/styles/default/img/no_auto_photo_85x75.png" alt="no image" width="85" height="75">
                    {/if}
                    <div class="mod_auto_latest-aditem_title">{$ad.title_vendor} {$ad.title_model}</div>
                    <div class="mod_auto_latest-aditem_price">{$ad.price[$currency]|number_format:0:"":" "}&nbsp;{$component_data.currency[$currency]}</div>
                </a>
            </div>
        {/foreach}
    </div>
</div>