{include file="_auto_cabinet_menu.tpl"}

{literal}
    <script>
        auto.init('photomanager', {
            id: "{/literal}{$data.id}{literal}",
            photoCount: "{/literal}{$photoList|@count}{literal}",
            maxPhotoCount: "{/literal}{$cfg.ad.max_photo}{literal}"
        });
    </script>
{/literal}

<h3>
    <span>Управление фотографиями {$data.title_vendor} {$data.title_model}</span>
    <span style="font-size: 14px;">(Номер объявления: {$data.id})</span>
</h3>


<table border="0" cellpadding="0" cellspacing="0">
    <tbody>
    <tr class="te_top">
        <td class="te_img1">&nbsp;</td>
        <td class="te_img2"></td>
        <td class="te_img3">&nbsp;</td>
    </tr>
    <tr>
        <td class="te_imgl"></td>
        <td>
            <div style="float: right;">Загруженно фотографий <span id="photo_counter">{$photoList|@count}</span> из <b>{$cfg.ad.max_photo}</b></div>
        </td>
        <td class="te_imgr"></td>
    </tr>
    <tr class="te_bottom">
        <td class="te_img1">&nbsp;</td>
        <td class="te_img2">&nbsp;</td>
        <td class="te_img3">&nbsp;</td>
    </tr>
    </tbody>
</table>

<div id="auto-notice" class="auto-message hidden"></div>

<table border="0" cellpadding="0" cellspacing="0">
    <tbody>
        <tr class="te_top">
            <td class="te_img1">&nbsp;</td>
            <td class="te_img2"><p>Фотографии</p></td>
            <td class="te_img3">&nbsp;</td>
        </tr>
        <tr>
            <td class="te_imgl"></td>
            <td>
                <!-- PHOTO MANAGER CONTENT -->
                <div id="auto-add_photo_btn" class="{if $photoList|@count >= $cfg.ad.max_photo}max-limit{/if}">
                    <div class="auto-add-upload_btn">
                        <input id="fileupload" type="file" name="files" multiple>
                        <div class="auto-add-upload_btn-title">нажмите чтобы добавить фотографии</div>
                        <div class="auto-add-upload_btn-desc">Вы можете загрузить сразу несколько фотографий, выбрав их в окне загрузки</div>
                        <div class="auto-add-upload_btn-desc">для действий с фотографией наведите на неё</div>
                    </div>

                    <div class="auto-add-upload_cap">
                        <div class="auto-add-upload_btn-title">удалите фотографии</div>
                        <div class="auto-add-upload_btn-desc">Извините, вы добавили допустимое количество фотографии, для добавления вам необходимо удалить лишние фотографии</div>
                        <div class="auto-add-upload_btn-desc">для действий с фотографией наведите на неё</div>
                    </div>
                </div>

                <div style="text-align: center;">
                    <div class="auto-add-photo_manager-container">
                        {foreach from=$photoList item=photo}
                            <div class="photo_item {if $photo.data.ordering == 0}main{/if}" data-num="{$photo.data.id}">
                                <a class="photo" href="{$photo.large}" rel="gallery">
                                    <img src="{$photo.small}" alt="">
                                </a>
                                <div class="photo-controls">
                                    <div class="setmain {if $photo.data.ordering == 0}selected{/if}">{if $photo.data.ordering == 0}Основное фото{else}Назначить основным{/if}</div>
                                    <div class="remove">Удалить</div>
                                </div>
                            </div>
                        {/foreach}

                        {if !$photoList}
                            <div id="auto-add-photo_manager-container_null" style="padding: 20px 0;">фотографий еще нет</div>
                        {/if}
                    </div>
                </div>
                <!-- END PHOTO MANAGER CONTENT -->
            </td>
            <td class="te_imgr"></td>
        </tr>
        <tr class="te_bottom">
            <td class="te_img1">&nbsp;</td>
            <td class="te_img2">&nbsp;</td>
            <td class="te_img3">&nbsp;</td>
        </tr>
    </tbody>
</table>

<!--  TEMPLATES SECTION -->
{literal}
    <script id="template-photoItem"  type="text/t-tmpl">
        <div class="photo_item"  data-num="${num}">
            <a class="photo" href="${largePicture}" rel="gallery">
                <img src="${smallPicture}" alt="">
            </a>
            <div class="photo-controls">
                <div class="setmain">Сделать основным</div>
                <div class="remove">Удалить</div>
            </div>
        </div>
    </script>

    <script id="template-photoItem-upload"  type="text/t-tmpl">
        <div class="photo_item upload"  data-uid="${uid}">
            <div style="width: ${progressBar}%" class="progressBar">${progressBar}%</div>
        </div>
    </script>
<!-- END TEMPLATES SECTION -->
{/literal}

