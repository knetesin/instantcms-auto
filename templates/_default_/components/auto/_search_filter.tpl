{if $cfg.ad.carousel.active}
    {include file="_lists/_carousel.tpl"}
{/if}


{literal}
    <script>
        auto.init('searchFilter');
    </script>
{/literal}

{if !$cfg.geo.city.default}
    <div id="geo-selector" class="geo-selector_container">
        <span>Продажа авто:</span>
        <span class="geo-selector_item" data-geo="region">{if $filter.geo.region.id}{$filter.geo.region.name}{else}любой регион{/if}</span>
        <span class="geo-selector_item" data-geo="city">{if $filter.geo.city.id}{$filter.geo.city.name}{else}любой город{/if}</span>
    </div>

    <div id="geo-selector-data" class="geo-selector-data_container" style="display: none; width: 300px">
        <form action="{$routes.ch_geo}">
            <div>
                {if $cfg.geo.region.default}
                    <span>{$filter.geo.region.name}</span>
                    <input type="hidden" name="region" value="{$filter.geo.region.id}">
                {else}
                    <label>
                        <select name="region">
                            {foreach from=$filter.geo.regions item=region key=value}
                                <option value="{$value}" {if $filter.geo.region.id == $value}selected="selected"{/if}>{$region}</option>
                            {/foreach}
                        </select>
                    </label>
                {/if}
            </div>

            <div>
                <label>
                    <select name="city">
                        {if $filter.geo.cities|@count == 0}<option>Выберите регион</option>{/if}
                        {foreach from=$filter.geo.cities item=region key=value}
                            <option value="{$value}" {if $filter.geo.city.id == $value}selected="selected"{/if}>{$region}</option>
                        {/foreach}
                    </select>
                </label>
            </div>

            <div class="geo-selector-btn_container">
                <input type="submit" value="Выбрать">
                <button onclick="$.fancybox.close();">закрыть</button>
            </div>
        </form>
    </div>
{/if}


<div id="searchFilter-container">
    <form class="searchFilter-form" action="{$routes.search}">
        <div class="searchFilter-section">
            <div class="searchFilter-section-title">{$LANG.TEMPLATE_SEARCHFILTER_VENDOR_TITLE}</div>
            <div class="searchFilter-section-content">
                <select class="searchFilter-vendorSelect" name="id_vendor">
                    <option  value="0">любая фирма</option>
                    {foreach from=$filter.vendors item=vendor name=ven}
                        {assign var="prevIndex" value=$smarty.foreach.ven.index-1}
                        {if !$vendor.popular and $filter.vendors[$prevIndex].popular}
                            <optgroup class="select_opt" label="-------------"></optgroup>
                        {/if}
                        <option value="{$vendor.id}" {if $filter.data.id_vendor == $vendor.id}selected="selected"{/if}>{$vendor.title}</option>
                    {/foreach}
                </select>
            </div>
        </div>

        <div class="searchFilter-section">
            <div class="searchFilter-section-title">{$LANG.TEMPLATE_SEARCHFILTER_MODEL_TITLE}</div>
            <div class="searchFilter-section-content">
                <select class="searchFilter-modelSelect" name="id_model">
                    <option value="0">любая модель</option>
                    {foreach from=$filter.models item=model name=model_s}
                        {assign var="prevIndex" value=$smarty.foreach.model_s.index-1}
                        {if !$model.popular and $filter.models[$prevIndex].popular}
                            <optgroup class="select_opt" label="-------------"></optgroup>
                        {/if}
                        <option value="{$model.id}" {if $filter.data.id_model == $model.id}selected="selected"{/if}>{$model.title}</option>
                    {/foreach}
                </select>
                <img style="display: none;" id="ajax_models_loader_pic" src="/components/{$smarty.const.COMPONENT_NAME}/styles/default/img/loading.gif">
            </div>
        </div>

        <div class="searchFilter-section">
            <div class="searchFilter-section-title">{$LANG.TEMPLATE_SEARCHFILTER_PRICE_TITLE}({$component_data.currency[$currency]}):</div>
            <div class="searchFilter-section-content">
                <input class="searchFilter-priceInput" type="text" name="minprice" maxlength="8" value="{$filter.data.minprice}">
                <span>-</span>
                <input class="searchFilter-priceInput" type="text" name="maxprice" maxlength="8" value="{$filter.data.maxprice}">
            </div>
        </div>

        <div class="searchFilter-section">
            <div class="searchFilter-section-title">{$LANG.TEMPLATE_SEARCHFILTER_YEAR_TITLE}</div>
            <div class="searchFilter-section-content">
                <select name="date" class="searchFilter-dateSelect">
                    <option value=""></option>
                    {foreach from=$filter.date item=date}
                        <option value="{$date}" {if $filter.data.date == $date}selected="selected"{/if}>{$date}</option>
                    {/foreach}
                </select>
                <span>-</span>
                <select name="datemax" class="searchFilter-dateSelect">
                    <option value=""></option>
                    {foreach from=$filter.date item=date}
                        <option value="{$date}" {if $filter.data.datemax == $date}selected="selected"{/if}>{$date}</option>
                    {/foreach}
                </select>
            </div>
        </div>

        <div class="searchFilter-section">
            <div class="searchFilter-section-title">{$LANG.TEMPLATE_SEARCHFILTER_VOLUME_TITLE}</div>
            <div class="searchFilter-section-content">
                <select name="min_volume" class="searchFilter-volumeSelect">
                    <option value=""></option>
                    <option value="1.0" {if $filter.data.min_volume == 1.0}selected="selected"{/if}>1.0</option>
                    <option value="1.3" {if $filter.data.min_volume == 1.3}selected="selected"{/if}>1.3</option>
                    <option value="1.5" {if $filter.data.min_volume == 1.5}selected="selected"{/if}>1.5</option>
                    <option value="1.6" {if $filter.data.min_volume == 1.6}selected="selected"{/if}>1.6</option>
                    <option value="1.8" {if $filter.data.min_volume == 1.8}selected="selected"{/if}>1.8</option>
                    <option value="2.0" {if $filter.data.min_volume == 2.0}selected="selected"{/if}>2.0</option>
                    <option value="2.3" {if $filter.data.min_volume == 2.3}selected="selected"{/if}>2.3</option>
                    <option value="2.4" {if $filter.data.min_volume == 2.4}selected="selected"{/if}>2.4</option>
                    <option value="2.5" {if $filter.data.min_volume == 2.5}selected="selected"{/if}>2.5</option>
                    <option value="2.7" {if $filter.data.min_volume == 2.7}selected="selected"{/if}>2.7</option>
                    <option value="3.0" {if $filter.data.min_volume == 3.0}selected="selected"{/if}>3.0</option>
                    <option value="3.5" {if $filter.data.min_volume == 3.5}selected="selected"{/if}>3.5</option>
                    <option value="4.0" {if $filter.data.min_volume == 4.0}selected="selected"{/if}>4.0</option>
                </select>
                <span>-</span>
                <select name="max_volume" class="searchFilter-volumeSelect">
                    <option value=""></option>
                    <option value="1.0" {if $filter.data.max_volume == 1.0}selected="selected"{/if}>1.0</option>
                    <option value="1.3" {if $filter.data.max_volume == 1.3}selected="selected"{/if}>1.3</option>
                    <option value="1.5" {if $filter.data.max_volume == 1.5}selected="selected"{/if}>1.5</option>
                    <option value="1.6" {if $filter.data.max_volume == 1.6}selected="selected"{/if}>1.6</option>
                    <option value="1.8" {if $filter.data.max_volume == 1.8}selected="selected"{/if}>1.8</option>
                    <option value="2.0" {if $filter.data.max_volume == 2.0}selected="selected"{/if}>2.0</option>
                    <option value="2.3" {if $filter.data.max_volume == 2.3}selected="selected"{/if}>2.3</option>
                    <option value="2.4" {if $filter.data.max_volume == 2.4}selected="selected"{/if}>2.4</option>
                    <option value="2.5" {if $filter.data.max_volume == 2.5}selected="selected"{/if}>2.5</option>
                    <option value="2.7" {if $filter.data.max_volume == 2.7}selected="selected"{/if}>2.7</option>
                    <option value="3.0" {if $filter.data.max_volume == 3.0}selected="selected"{/if}>3.0</option>
                    <option value="3.5" {if $filter.data.max_volume == 3.5}selected="selected"{/if}>3.5</option>
                    <option value="4.0" {if $filter.data.max_volume == 4.0}selected="selected"{/if}>4.0</option>
                </select>
            </div>
        </div>

        <div class="searchFilter-section">
            <div class="searchFilter-section-title">{$LANG.TEMPLATE_SEARCHFILTER_FUEL_TITLE}</div>
            <div class="searchFilter-section-content">
                <select name="fueltype" class="searchFilter-fuelSelect">
                    <option value="0"></option>
                    {foreach from=$component_data.fueltype item=fuel key=value}
                        <option value="{$value}" {if $filter.data.fueltype == $value} selected="selected" {/if}>{$fuel}</option>
                    {/foreach}
                </select>
            </div>
        </div>

        <div class="searchFilter-section">
            <div class="searchFilter-section-title">{$LANG.TEMPLATE_SEARCHFILTER_TRANS_TITLE}</div>
            <div class="searchFilter-section-content">
                <select name="transmission"  class="searchFilter-transSelect">
                    <option value="0"></option>
                    {foreach from=$component_data.transmission item=transmission key=value}
                        <option value="{$value}" {if $filter.data.transmission == $value} selected="selected" {/if}>{$transmission}</option>
                    {/foreach}
                </select>
            </div>
        </div>

        <div class="searchFilter-section">
            <div class="searchFilter-section-title">{$LANG.TEMPLATE_SEARCHFILTER_DRIVE_TITLE}</div>
            <div class="searchFilter-section-content">
                <select name="drive"  class="searchFilter-driveSelect">
                    <option value="0"></option>
                    {foreach from=$component_data.drive item=drive key=value}
                        <option value="{$value}" {if $filter.data.drive == $value} selected="selected" {/if}>{$drive}</option>
                    {/foreach}
                </select>
            </div>
        </div>

        <div class="searchFilter-section">
            <div class="searchFilter-section-content searchFilter-withoutTitle">
                <label>
                    <input type="checkbox" name="with_photo" value="1" {if $filter.data.with_photo}checked="checked" {/if}>
                    <span class="label-title">С фото</span>
                </label>
                <label>
                    <input type="checkbox" name="wheel[]" value="1" {if in_array(1, $filter.data.wheel)}checked="checked" {/if}>
                    <span class="label-title">Левый руль</span>
                </label>
                <label>
                    <input type="checkbox" name="foreign" value="1" {if $filter.data.foreign}checked="checked" {/if}>
                    <span class="label-title">Иномарки</span>
                </label>
            </div>
        </div>


        <div id="searchFilter-section-more-container">
            <div class="searchFilter-section">
                <div class="searchFilter-section-title">Наличие птс</div>
                <div class="searchFilter-section-content">
                    <label>
                        <input name="docs" type="radio" value="0" {if $filter.data.docs == 0 }checked="checked" {/if}>
                        <span class="label-title">Неважно</span>
                    </label>
                    <label>
                        <input name="docs" type="radio" value="1" {if $filter.data.docs == 1 }checked="checked" {/if}>
                        <span class="label-title">C документами</span>
                    </label>
                    <label>
                        <input name="docs" type="radio" value="2" {if $filter.data.docs == 2 }checked="checked" {/if}>
                        <span class="label-title">Без документов</span>
                    </label>
                </div>
            </div>

            <div class="searchFilter-section">
                <div class="searchFilter-section-title">Состояние</div>
                <div class="searchFilter-section-content">
                    <label>
                        <input name="damaged" type="radio" value="0"{if $filter.data.damaged == 0 }checked="checked" {/if}>
                        <span class="label-title">Любое</span>
                    </label>
                    <label>
                        <input name="damaged" type="radio" value="2"{if $filter.data.damaged == 2 }checked="checked" {/if}>
                        <span class="label-title">Небитые</span>
                    </label>
                    <label>
                        <input name="damaged" type="radio" value="1"{if $filter.data.damaged == 1 }checked="checked" {/if}>
                        <span class="label-title">Битые или не на ходу</span>
                    </label>
                </div>
            </div>

            <div class="searchFilter-section">
                <div class="searchFilter-section-title">Тип кузова</div>
                <div class="searchFilter-section-content">
                    <label>
                        <input type="checkbox" name="carcase[]" value="3" {if in_array(3, $filter.data.carcase) }checked="checked" {/if}>
                        <span class="label-title">Седан</span>
                    </label>
                    <label>
                        <input type="checkbox" name="carcase[]" value="4" {if in_array(4, $filter.data.carcase) }checked="checked" {/if}>
                        <span class="label-title">Внедорожник</span>
                    </label>
                    <label>
                        <input type="checkbox" name="carcase[]" value="5" {if in_array(5, $filter.data.carcase) }checked="checked" {/if}>
                        <span class="label-title">Хэтчбек</span>
                    </label>
                    <label>
                        <input type="checkbox" name="carcase[]" value="6" {if in_array(6, $filter.data.carcase) }checked="checked" {/if}>
                        <span class="label-title">Универсал</span>
                    </label>
                    <label>
                        <input type="checkbox" name="carcase[]" value="7" {if in_array(7, $filter.data.carcase) }checked="checked" {/if}>
                        <span class="label-title">Минивен</span>
                    </label>
                    <label>
                        <input type="checkbox" name="carcase[]" value="8" {if in_array(8, $filter.data.carcase) }checked="checked" {/if}>
                        <span class="label-title">Микроавтобус</span>
                    </label>
                    <label>
                        <input type="checkbox" name="carcase[]" value="9" {if in_array(9, $filter.data.carcase) }checked="checked" {/if}>
                        <span class="label-title">Купе</span>
                    </label>
                    <label>
                        <input type="checkbox" name="carcase[]" value="10" {if in_array(10, $filter.data.carcase) }checked="checked" {/if}>
                        <span class="label-title">Кабриолет</span>
                    </label>
                    <label>
                        <input type="checkbox" name="carcase[]" value="11" {if in_array(11, $filter.data.carcase) }checked="checked" {/if}>
                        <span class="label-title">Пикап</span>
                    </label>
                </div>
            </div>

            <div class="searchFilter-section">
                <div class="searchFilter-section-title">Дополнительно</div>
                <div class="searchFilter-section-content">
                    <label>
                        <input name="unsold" type="checkbox" value="1" {if $filter.data.unsold}checked="checked" {/if}>
                        <span class="label-title">Непроданные</span>
                    </label>
                    <label>
                        <input type="checkbox" name="wheel[]" value="2" {if in_array(2, $filter.data.wheel)}checked="checked" {/if}>
                        <span class="label-title">Правый руль</span>
                    </label>
                    <label>
                        <input name="is_no_rus_mil" type="checkbox" value="1" {if $filter.data.is_no_rus_mil == 1}checked="checked" {/if}>
                        <span class="label-title">Без пробега по РФ</span>
                    </label>
                    <label>
                        <input name="is_gt" type="checkbox" value="1" {if $filter.data.is_gt}checked="checked" {/if}>
                        <span class="label-title">GT-Машины</span>
                    </label>
                    <label>
                        <input name="is_new" type="checkbox" value="1" {if $filter.data.is_new}checked="checked" {/if}>
                        <span class="label-title">Новые</span>
                    </label>
                    <label>
                        <input name="is_cut" type="checkbox" value="1" {if $filter.data.is_cut}checked="checked" {/if}>
                        <span class="label-title">Конструктор</span>
                    </label>
                </div>
            </div>

        </div>

        <div class="searchFilter-section searchFilter-submit-container">
            <div class="searchFilter-section-content searchFilter-withoutTitle">
                <span id="searchFilter-section-more-showBtn">
                    <span class="searchFilter-image">&nbsp;</span>
                    <span class="searchFilter-section-more-showBtn-title">Расширенный поиск</span>
                </span>
                <input type="submit" title="показать" value="Показать">
            </div>
        </div>

    </form>

</div>

{if $controlLinks}
    <div class="auto-controlLinks">
        {if $do == 'showModelList'}
            <a href="{$routes.showVendor|sprintf:$controlLinks.url}">{$LANG.TEMPLATE_CONTROLLINKS_LIST_LIM|sprintf:$controlLinks.title}</a>
            {*<a href="{$routes.searchNew}">{$LANG.TEMPLATE_CONTROLLINKS_NEWAUTO_LIM|sprintf:$controlLinks.title}</a>*}
            <a href="{$routes.cabinet.index}">{$LANG.TEMPLATE_CONTROLLINKS_CABINET}</a>
        {elseif $do == 'showVendor'}
            <a href="{$routes.vendorList|sprintf:$controlLinks.url}">{$LANG.TEMPLATE_CONTROLLINKS_LIST_LIM|sprintf:$controlLinks.title}</a>
            {*<a href="{$routes.searchNew}">{$LANG.TEMPLATE_CONTROLLINKS_NEWAUTO_LIM|sprintf:$controlLinks.title}</a>*}
            <a href="{$routes.cabinet.index}">{$LANG.TEMPLATE_CONTROLLINKS_CABINET}</a>
        {/if}
        <a href="{$routes.add}" class="auto-controlLinks-right">{$LANG.TEMPLATE_CONTROLLINKS_ADD}</a>
    </div>
{else}
    <div class="auto-controlLinks">
        <a href="{$routes.search}">{$LANG.TEMPLATE_CONTROLLINKS_LIST}</a>
        {*<a href="{$routes.searchNew}">{$LANG.TEMPLATE_CONTROLLINKS_NEWAUTO}</a>*}
        <a href="{$routes.cabinet.index}">{$LANG.TEMPLATE_CONTROLLINKS_CABINET}</a>
        <a class="auto-controlLinks-right" href="{$routes.add}">{$LANG.TEMPLATE_CONTROLLINKS_ADD}</a>
    </div>
{/if}
