{include file="_auto_cabinet_menu.tpl"}

<h3>{$LANG.TEMPLATE_EDIT_AD_PAGE_PATHWAY|sprintf:$ad.id}</h3>

{if $set.edited_at}<div class="auto-message">Дата последнего редактирования: {$set.edited_at|date_format:'%d.%m.%Y в %H:%M'}</div>{/if}

{if $errors}
    <ul class="auto_err_msg">
        {foreach from=$errors item=error}
            <li class="error-field-desc" data-error-selector="{$error.selector}">
                <span>{$error.title}</span>
            </li>
        {/foreach}
    </ul>
{/if}

Поля, отмеченные <span style="color: red;">*</span>, обязательны для заполнения.

{include file="_auto_add_edit_form.tpl"}