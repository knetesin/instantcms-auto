<div class="cabinet-edit-controls">
    <a href="{$routes.cabinet.edit|sprintf:$ad.id}" class="edit {if $subaction2 == 'edit' and $subaction3 == '' }selected{/if}">Редактировать объявление</a>
    <a href="{$routes.cabinet.photomanager|sprintf:$ad.id}" class="photomanager {if $subaction2 == 'edit' and $subaction3 == 'pm'}selected{/if}">Управление фотографиями</a>
    <a href="{$routes.cabinet.questions|sprintf:$ad.id}" class="questions {if $subaction2 == 'edit' and $subaction3 == 'questions'}selected{/if}">Управление вопросами</a>
    <a href="{$routes.show|sprintf:$ad.id}" class="show">Открыть объявление</a>
</div>