<h3>Подать объявление о продаже автомобиля</h3>

{if !$user_id}
    <div class="auto-message">
        <span>Внимание!</span>
        <span>Вы не авторизованы, если у вас уже есть аккаунт <a href="/login">авторизуйтесь</a>. В противном случае вам будет автоматически заведена учетная запись портала. Так же вы можете пройти процедуру <a href="/registration">регистрации</a> самостоятельно </span>

    </div>
{/if}

{if $errors}
    <ul class="auto_err_msg">
        {foreach from=$errors item=error}
            <li class="error-field-desc" data-error-selector="{$error.selector}">
                <span>{$error.title}</span>
            </li>
        {/foreach}
    </ul>
{/if}

<div id="steep">Шаг 1 из 2</div>

Поля, отмеченные <span style="color: red;">*</span>, обязательны для заполнения.

{include file="_auto_add_edit_form.tpl"}