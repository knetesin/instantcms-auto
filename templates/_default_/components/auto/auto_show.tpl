{literal}
    <script>
        $(function(){
            $('.fancybox').fancybox();
        });

        auto.init('show', {
            id: "{/literal}{$ad.data.id}{literal}"
        });
    </script>
{/literal}

{if !$ad.data.auto_status}
    <div class="autoshop_success_msg">Внимание! <b>Машина продана!</b> </div>
{else}
    {if $ad.show == 0}<div class="auto-message closed">Внимание объявление закрыто для показа</div>{/if}
    {if $ad.show == 1}<div class="auto-message pre-moderation">Внимание объявление находится на пре-модерации. В данный момент оно доступно только вам</div>{/if}
    {if $ad.show == 2}<div class="auto-message post-moderation">Внимание объявление находится на пост-модерации.</div>{/if}
{/if}

{if $smarty.const.HAVE_COMPONENT_BILLING and $ad.data.auto_status}
    {include file="_auto_paid_services.tpl"}
{/if}

{if $ad.data.auto_status > 0 && $user_id == $ad.data.id_owner}
    <div id="sell_auto" style="display: none; width: 400px;">
        <h6>Подтвердите продажу авто</h6>
        <p>Внимание! после потдверждения вы не сможете изменять любую информацию касательно данного объявления!</p>
        <div style="margin-top: 15px">
            <button id="set_sell" data-ad-id="{$ad.data.id}">Подтверждаю</button>
            <button onclick="$.fancybox.close();">Отмена</button>
        </div>
    </div>
{/if}

<div id="auto-show-container">
    <div class="auto-show-title">
        <span>{$LANG.TEMPLATE_SHOW_TITLE|sprintf:$ad.data.title_vendor:$ad.data.title_model}</span>
        <span class="auto-show-icTitle">({$ad.data.city_title}, {$ad.data.date_construct} год)</span>
    </div>

    <!-- ad info-block -->
    <div class="auto-show-info">
        <div class="auto-show-info_left">
            <span>опубликовано:</span>
            <span>{$ad.data.created_at|date_format:'%d.%m.%Y'}</span>
        </div>

        <div class="auto-show-info_right">

            {if $smarty.const.HAVE_COMPONENT_SUPPORT}
                <div class="auto-show-callSup">
                    [ <span>{if $user_id != $ad.data.id_owner}пожаловаться{elseif $user_id == $ad.data.id_owner}обратиться в тех. поддержку{/if}</span> ]
                </div>
            {/if}

            <div class="auto-show-views">
                <span>просмотров:</span>
                <span>{$ad.data.count_views}</span>
            </div>

        </div>
    </div>
    <!-- end ad info-block -->

    <!-- ad data block -->
    <div class="auto-show-data">
        <div class="auto-show-photo_container">
            <div class="photo_container-main_photo">
                <table style=" width: 100%;">
                    <tr>
                        <td align="center">
                            {if $ad.data.photo|@count}
                                <a href="{$ad.data.photo[0].large}" id="show_main_photo" rel="card-photo" onclick="return false;" title="нажмите для того чтобы увеличить">
                            {/if}
                            <img src="{if $ad.data.photo|@count}{$ad.data.photo[0].preview}{else}/components/auto/styles/default/img/default_no_foto.jpg{/if}" width="354" />
                            {if $ad.data.photo|@count}
                                    <span></span>
                                </a>
                            {/if}
                        </td>
                    </tr>
                </table>
            </div>

            <div class="photo_container-photo_previews">
                {foreach from=$ad.data.photo item=photo name=photo}
                    <a href="{$photo.large}" data-preview="{$photo.preview}" title="{$ad.data.title_vendor} {$ad.data.title_model}, {$ad.data.date_construct}, {$ad.data.price_list[1]|number_format:0:"":" "} {$component_data.currency[1]}" class="photo_link {if $smarty.foreach.photo.iteration == 1}active{/if}" rel="card-photo" photo-index="{$smarty.foreach.photo.index}">
                        <img src="{$photo.small}"  alt="">
                    </a>
                {/foreach}
            </div>

            <div class="action-panel no-print">
                <div class="ap-buttons">
                    {if $user_id == $ad.data.id_owner}
                        {if $ad.data.auto_status}
                            <a href="{$routes.cabinet.edit|sprintf:$ad.data.id}" class="ap-show-edit ap-btn">
                                <span class="ico"></span>
                                <span class="title">Редактировать объявление</span>
                            </a>
                            <a href="{$routes.cabinet.photomanager|sprintf:$ad.data.id}" class="ap-show-pm ap-btn">
                                <span class="ico"></span>
                                <span class="title">Управление фотографиями</span>
                            </a>
                            <a href="{$routes.cabinet.questions|sprintf:$ad.data.id}" class="ap-show-questions ap-btn">
                                <span class="ico"></span>
                                <span class="title">Список вопросов</span>
                            </a>
                        {else}
                            <a href="{$routes.cabinet.index}" class="ap-show-cabinet ap-btn">
                                <span class="ico"></span>
                                <span class="title">Перейти в личный кабинет</span>
                            </a>
                        {/if}
                    {/if}

                    {if $smarty.const.HAVE_COMPONENT_BILLING and $ad.data.auto_status}
                        {if $user_id}
                            <a id="paid_services_show" class="ap-show-pay_services ap-btn">
                                <span class="ico"></span>
                                <span class="title">Платные услуги</span>
                            </a>
                        {else}
                            <a href="/login" class="ap-show-pay_services ap-btn">
                                <span class="ico"></span>
                                <span class="title">Платные услуги</span>
                            </a>
                        {/if}
                    {/if}

                    <span class="ap-show-print ap-btn" onclick="window.print();">
                        <span class="ico"></span>
                        <span class="title">вывести на печать</span>
                    </span>
                    {if $ad.data.auto_status > 0 && $user_id == $ad.data.id_owner}
                        {if $ad.data.auto_status}
                            <a href="#sell_auto" class="ap-show-sold ap-btn fancybox">
                                <span class="title">Отметить проданным</span>
                                <span class="ico"></span>
                            </a>
                        {/if}
                    {/if}
                </div>

                <!-- share block -->
                <div class="action-panel_item action-panel-bottom">
                    <div id="share_panel">
                        <div style="float:left; font-size: 15px;">опубликовать в:</div>
                        <div style="float:left; margin-left: 15px;">
                            <button title="Опубликовать в ВКонтакте" data-type="vk" data-image="{if $ad.data.photo|@count}{if $smarty.server.HTTPS}https://{else}http://{/if}{$smarty.server.HTTP_HOST}{$ad.data.photo[0].large}{/if}" class="share_soc_icons share_vk"></button>
                            <button title="Опубликовать в Одноклассниках" data-type="ok" class="share_soc_icons share_ok"></button>
                            <button title="Опубликовать в Facebook" data-type="fb" class="share_soc_icons share_fb"></button>
                            <button title="Опубликовать в Google Plus" data-type="gp" class="share_soc_icons share_gp"></button>
                            <button title="Опубликовать в Twitter" data-type="tw" class="share_soc_icons share_tw"></button>
                        </div>
                    </div>
                </div>
                <!-- end share block -->

            </div>

        </div>

        <div class="auto-show-content_container">

            <div class="auto-show-content_container-price">
                <span class="price-value">{$ad.data.price|number_format:0:"":" "}&nbsp;{$component_data.currency[$ad.data.currency]}</span>
                <span class="price-type">{if $ad.data.currency != $currency}&nbsp;~&nbsp;{$ad.data.price_list[$currency]|number_format:0:"":" "}&nbsp;{$component_data.currency[$currency]}{/if}</span>
            </div>

            <div class="auto-show-data_field_container">
                <div>
                    {if $ad.data.fueltype or $ad.data.volume > 0}
                        <div>
                            <span class="auto-show-data_title">Двигатель:</span>
                            <span>{', '|implode:$ad.data.specf.fl}</span>
                        </div>
                    {/if}
                    {if $ad.data.transmission}
                        <div>
                            <span class="auto-show-data_title">Трансмиссия:</span>
                            <span>{$component_data.transmission[$ad.data.transmission]}</span>
                        </div>
                    {/if}
                    {if $ad.data.drive}
                        <div>
                            <span class="auto-show-data_title">Привод:</span>
                            <span>{$component_data.drive[$ad.data.drive]}</span>
                        </div>
                    {/if}
                    <div>
                        <span class="auto-show-data_title">Пробег по России:</span>
                        {if $ad.data.is_without_rus_run == 1}без пробега{else}есть{/if}
                    </div>
                    {if $ad.data.mileage or $ad.data.is_new_auto}
                        <div>
                            <span class="auto-show-data_title">Пробег{if !$ad.data.is_new_auto}, км{/if}:</span>
                            <span>{if $ad.data.is_new_auto}новый автомобиль{else}{$ad.data.mileage|number_format:0:"":" "}{/if}
                        </div>
                    {/if}
                    {if $ad.data.wheel}
                        <div>
                            <span class="auto-show-data_title">Руль:</span>
                            <span>{$component_data.wheel[$ad.data.wheel]}</span>
                        </div>
                    {/if}
                    {if $ad.data.specf.special|@count}
                        <div>
                            <span class="auto-show-data_title">Особые отметки:</span>
                            <span style="font-weight: bold">{', '|implode:$ad.data.specf.special}</span>
                        </div>
                    {/if}
                </div>

                {if $ad.data.description}
                    <div>
                        <span class="auto-show-data_title">Дополнительно:</span>
                        <p>{$ad.data.description}</p>
                    </div>
                {/if}

                {if $ad.data.is_gt and $ad.data.gt_description}
                    <div>
                        <span class="auto-show-data_title">Тюнинг:</span>
                        <p>{$ad.data.gt_description}</p>
                    </div>
                {/if}

                <div>
                    <span class="auto-show-data_title">Город:</span>
                    <span>{$ad.data.city_title}</span>
                </div>
            </div>

            <p class="container-phone">
                {if !$ad.data.auto_status}
                    <span>машина продана, контактные данные закрыты</span>
                {else}
                    <span>+7</span>
                    <a href="javascript: void(0)" id="show_contacts" class="show" style="outline-style: none; " title="Нажмите, чтобы показать телефон">Показать телефон</a>
                {/if}
            </p>

        </div>
    </div>
    <!-- end ad data block -->

    {if $ads.data.list|@count > 0}
        <!-- start ads -->
        <div class="auto-show-similar_offers print">
            <div class="auto-show-similar_offers-title">
                <span>Похожие предложения:</span>
            </div>

            {include file="_lists/_ads.tpl"}
        </div>
        <!-- end ads -->
    {/if}

    {if $ad.data.access_issues or $user_id == $ad.data.id_owner}
        <!-- QA SECTION -->
        <div class="auto-show-similar_offers">
            <div class="auto-show-similar_offers-title">
                <span>Вопросы и ответы:</span>
            </div>

            {include file="_lists/_qa_show.tpl"}
        </div>
        <!-- END QA SECTION -->
    {/if}

</div>





