{literal}
    <script>
        auto.init('currency');
    </script>
{/literal}

<div class="component-auto">
    {if $do == 'index'}
        {include file='auto_index.tpl'}
    {elseif $do == 'showVendor'}
        {include file='auto_index_vendor.tpl'}
    {elseif $do == 'showModelList'}
        {include file='auto_index_model.tpl'}
    {elseif $do == 'cabinet'}
        {if $subaction1 == 'ads'}
            {if $subaction2 == 'edit'}
                {if $subaction3 == 'pm'}
                    {include file='auto_cabinet_ads_photomanager.tpl'}
                {elseif $subaction3 == 'questions'}
                    {include file='auto_cabinet_ads_questions.tpl'}
                {else}
                    {include file='auto_cabinet_ad_edit.tpl'}
                {/if}
            {else}
                {include file='auto_cabinet_ads_list.tpl'}
            {/if}
        {elseif $subaction1 == 'my_questions'}
            {include file='auto_cabinet_my_questions.tpl'}
        {else}
            {include file='auto_cabinet_index.tpl'}
        {/if}
    {elseif $do == 'add'}
        {include file='auto_add.tpl'}
    {elseif $do == 'search' or $do == 'showVendorList'}
        {include file='auto_search.tpl'}
    {elseif $do == 'show'}
        {include file='auto_show.tpl'}
    {elseif $do == 'spares'}
        {include file='auto_spares.tpl'}
    {else}
        {include file='auto_index.tpl'}
    {/if}
</div>

<div id="currency-control" class="no-print">
    {foreach from=$component_data.currency key=value item=cur}
        <div data-currency="{$value}" {if $currency == $value}class="current"{/if}>{$cur}</div>
    {/foreach}
</div>