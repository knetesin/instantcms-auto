{include file="_auto_cabinet_menu.tpl"}

{literal}
<script>
    auto.init('cabinet_questions', {
        id: "{/literal}{$ad.data.id}{literal}"
    });
</script>
{/literal}

<div class="auto-message">Будьте внимательны при составление ответа на вопрос, редактировать ответы на вопросы в дальнейшем не будет возможности.</div>

<div class="auto-cabinet-question-container">
    {foreach from=$questions item=question}
        <div>
            <div class="item-contacts">
                {if $question.is_new}
                    <div class="question-new">Новый</div>
                {/if}
                <div class="contacts-title">Контактные данные:&nbsp;</div>
                <div class="content_item_phone">{$question.email}</div>
                <div class="content_item_phone">{$question.phone}</div>
            </div>
            <div id="question-{$question.id}" class="item_question">

                <div class="question">
                    <div class="questionar">&nbsp;</div>
                    <div>
                        <span class="question-author ">{$question.author}</span>
                        <span class="question-date">({$question.created_at|date_format:'%d.%m.%Y в %H:%M'})</span>
                    </div>
                    <p>{$question.message}</p>
                </div>

                <div class="answer_layer">
                    <div class="answer">
                        <p></p>
                        {if $question.answer}
                            {$question.answer}
                        {else}

                            <form class="answer-form" data-question-id="{$question.id}">
                                <div class="form-field">
                                    <textarea name="answer"></textarea>
                                    <div class="form-field-error"></div>
                                </div>

                                <input type="submit" value="Отправить ответ">
                            </form>
                        {/if}
                        <div>&nbsp;</div>
                    </div>
                </div>
            </div>
        </div>

        <hr>
    {foreachelse}
        <div class="list_no_item">вопросов с ответами еще нет!</div>
    {/foreach}

</div>
