<H3>Добро пожаловать в <b>личный кабинет</b></H3>


<div class="maincabinetbtn">
    <a href="{$routes.cabinet.adslist}">Мои объявления ({$data.count.ad})</a>
</div>

<div class="maincabinetbtn">
    <a href="{$routes.cabinet.my_questions}">Мои вопросы ({$data.count.questions})</a>
</div>

<div class="maincabinetbtn">
    <a href="{$routes.add}">Подать объявление</a>
</div>

<div>
    {if $data.news}
        <h3>Новые вопросы к вашим объявлениям:</h3>
        <div id="newquestionadvt_wrap">
            <table class="list_units">
                <thead>
                    <tr>
                        <td>№ объявления</td>
                        <td>Модель</td>
                        <td>Новых вопросов</td>
                        <td>Панель действий</td>
                    </tr>
                </thead>
                <tbody>
                {foreach from=$data.news item=new}
                    <tr class="newquestionadvt_item">
                        <td>{$new.id}</td>
                        <td>{$new.title_vendor} {$new.title_model}</td>
                        <td>{$new.count_q}</td>
                        <td class="controls-btns">
                            <a href="{$routes.cabinet.questions|sprintf:$new.id}" title="Управление вопросами" class="c-btn edit"></a>
                            <a href="{$routes.show|sprintf:$new.id}" title="Открыть объявление" class="c-btn show"></a>
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
    {else}
        <hr>
        <div style="color: #535353; font-size: 13px; margin-left: 10px;">Новых вопросов к вашим объявлениям нет!</div>
    {/if}
</div>



