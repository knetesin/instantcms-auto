<div class="cat-container">

    {foreach from=$cats item=catColumn name=cat}
        <div class="cats-column">
            {foreach from=$catColumn item=catsContainer}
                <div class="cat-char-container">
                    <i class="cat-fs_title">{$catsContainer.f_title}</i>

                    {foreach from=$catsContainer.data item=cat}
                        <h3 class="cat-container">
                            <a class="cat-link" href="{$cat.url}">
                                {if $cfg.system.showlogocats and $cat.show_logo}
                                    <img class="autoshop_cats_img" src="{$cfg.system.upload_dir}vendors/small/{if $cat.logo}{$cat.logo}{else}nologo.png{/if}">
                                {/if}
                                <span class="cat-title{if $cat.popular} popular{/if}">{$cat.title}</span>
                            </a>
                            <span class="cat-counter">{$cat.count_advt}</span>
                        </h3>
                    {/foreach}
                </div>
            {/foreach}
        </div>
        {foreachelse}
        <div class="list_no_item">Объявлений еще нет. <a href="{$routes.add}">Добавить первое объявление</a></div>
    {/foreach}
</div>