{literal}
<script>
    auto.init('qa', {
        id: "{/literal}{$ad.data.id}{literal}"
    });
</script>
{/literal}

<div class="auto-show-qa-container">

    {if $ad.data.auto_status and $user_id != $ad.data.id_owner}
        <span id="auto-qa-showbtn" class="auto-ajax-link auto-ar_form-show">Задать вопрос продавцу</span>

        <div id="auto-qa-container" class="auto-ar_form-container">
            <form data-ad-id="{$ad.data.id}">
                <span></span>
                <div>
                    <div class="form-field">
                        <div>Ваше имя:</div>
                        <div>
                            <input type="text" name="author">
                        </div>
                        <div class="form-field-error"></div>
                    </div>

                    <div class="form-field">
                        <div>E-mail:</div>
                        <div>
                            <input type="text" name="email">
                        </div>
                        <div class="form-field-error"></div>
                    </div>

                    <div class="form-field">
                        <div>Телефон(необязательно):</div>
                        <div>
                            <input type="text" name="phone">
                        </div>
                        <div class="form-field-error"></div>
                    </div>

                    <div class="form-field">
                        <div>Ваш вопрос:</div>
                        <div>
                            <textarea cols="30" rows="5" name="message" style="width: 96%;"></textarea>
                        </div>
                        <div class="form-field-error"></div>
                    </div>

                    <div style="float: right">
                        {if $user_id}
                            <span>Область видимости:</span>
                            <select name="show_type">
                                <option value="1">Всем</option>
                                <option value="2">Только владельцу</option>
                            </select>
                        {/if}
                    </div>

                    <div class="form-submit-container">
                        <div class="form-submit">
                            <input type="submit">
                        </div>
                        <div class="form-preloader"></div>
                    </div>

                </div>
            </form>
        </div>

        <div id="auto-qa-result" class="auto-message" style="display: none"></div>
    {/if}
</div>

{if $ad.data.questions|@count}
    <div id="question_layer">
        {foreach from=$ad.data.questions item=question}
            {if $question.show_type == 1 or ($question.show_type == 2 and ($user_id == $ad.data.id_owner or $user_id == $question.id_user))}
                <div id="question-{$question.id}" class="item_question">

                    <div class="question">
                        <div class="questionar">&nbsp;</div>
                        <div>
                            <span class="question-author ">{$question.author}</span>
                            <span class="question-date">({$question.created_at|date_format:'%d.%m.%Y в %H:%M'})</span>

                            {if $user_id == $question.id_user and $user_id}
                                <span class="question-owner">ваш вопрос</span>
                            {/if}
                        </div>
                        <p>{$question.message}</p>

                        {if $user_id == $ad.data.id_owner or ($user_id == $question.id_user and $user_id)}
                            <span>Контактные данные:&nbsp;</span>
                            <span class="content_item_phone">{$question.email}</span>
                            <span class="content_item_phone">{$question.phone}</span>
                        {/if}

                        {if $question.show_type == 2 and ($user_id == $ad.data.id_owner or $user_id == $question.id_user)}
                            <div class="question-show_type">Видно только вам</div>
                        {/if}
                    </div>
                    {if $question.answer}
                        <div class="answer_layer">
                            <div class="answer">{$question.answer}<div>&nbsp;</div></div>
                        </div>
                    {/if}
                </div>
            {/if}
        {/foreach}
    </div>
{else}
    <div class="list_no_item">Вопросов еще нет!</div>
{/if}
