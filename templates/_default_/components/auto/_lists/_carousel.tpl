{literal}
    <style>
        .jcarousel-wrapper {
            position: relative;
        }

        /** Carousel **/

        .jcarousel {
            position: relative;
            overflow: hidden;
            width: 100%;
        }

        .jcarousel ul {
            width: 20000em;
            position: relative;
            list-style: none;
            margin: 0;
            padding: 0;
        }

        .jcarousel li {
            float: left;
            border: 1px solid #fff;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            margin: 0px;
        }

        .jcarousel li a{
            text-decoration: none;
        }

        .jcarousel li a:hover .carousel-ad-title{
            color: red;
        }

        .jcarousel li a .carousel-ad-price{
            background: rgb(224, 0, 0);
            color: white;
            text-align: center;
            padding: 2px 0;
        }

        .jcarousel li a .carousel-ad-city{
            color: #000000;
        }

        .jcarousel li a .carousel-ad-title{
            color: #038edd;
            text-decoration: underline;
        }


        .jcarousel img {
            display: block;
            max-width: 100%;
            height: 140px;
            width: 184px;
        }

        /** Carousel Controls **/

        .jcarousel-prev,
        .jcarousel-next {
            position: absolute;
            top: 50%;
            margin-top: -15px;
            width: 30px;
            height: 30px;
            text-align: center;
            background: #4E443C;
            color: #fff;
            text-decoration: none;
            text-shadow: 0 0 1px #000;
            font: 24px/27px Arial, sans-serif;
            -webkit-border-radius: 30px;
            -moz-border-radius: 30px;
            border-radius: 30px;
            -webkit-box-shadow: 0 0 4px #F0EFE7;
            -moz-box-shadow: 0 0 4px #F0EFE7;
            box-shadow: 0 0 4px #F0EFE7;
        }

        .jcarousel-prev {
            left: 15px;
        }

        .jcarousel-next {
            right: 15px;
        }

    </style>

    <script>
        (function($) {
            $(function() {
                var jcarousel = $('.jcarousel');

                jcarousel.on('jcarousel:reload jcarousel:create', function () {
                    var width = jcarousel.innerWidth();

                    if (width >= 600) {
                        width = width / 5;
                    } else if (width >= 350) {
                        width = width / 5;
                    }

                    jcarousel.jcarousel('items').css('width', width + 'px');
                }).jcarousel({
                    wrap: 'circular'
                });

                $('.jcarousel-prev').jcarouselControl({
                    target: '-=1'
                });

                $('.jcarousel-next').jcarouselControl({
                    target: '+=1'
                });

            });
        })(jQuery);
    </script>
{/literal}

{if $carousel.list|@count}
    <div class="jcarousel-wrapper">
        <div class="jcarousel" data-jcarousel="true">
            <ul>
                {foreach from=$carousel.list item=ad}
                    <li>
                        <a href="{$ad.link}">
                            <img src="{$ad.photo.carousel}">
                            <div class="carousel-ad-price">{$ad.price[$currency]|number_format:0:"":" "}&nbsp;{$component_data.currency[$currency]}</div>
                            <div class="carousel-ad-city">{$ad.city_title}</div>
                            <div class="carousel-ad-title">{$ad.title_vendor} {$ad.title_model}</div>
                        </a>
                    </li>
                {/foreach}
            </ul>
            <a href="#" class="jcarousel-next">›</a>
            <a href="#" class="jcarousel-prev">‹</a>
        </div>

    </div>
{/if}