{if $ads.data.list|@count > 0}
    <table class="list_units" style="width: 100%">
        <thead class="{if $ads.ordering}ordering{else}no-ordering{/if}">
        {if $ads.ordering}
            <th class="list_date">
                <a href="{if $ads.url}{$ads.url}{else}{$routes.search}{/if}{$ads.data.html_query}&order=date&direction={if $ads.data.order.direction == 'ASC'}DESC{else}ASC{/if}" class="{if $ads.data.order.direction == 'ASC' and $ads.data.order.field == 'date'}asc order{elseif $ads.data.order.field == 'date'}desc order{/if}">
                    <span>дата</span>
                    <span class="direction"></span
                </a>
            </th>
            <th></th>
            <th>
                <a href="{if $ads.url}{$ads.url}{else}{$routes.search}{/if}{$ads.data.html_query}&order=model&direction={if $ads.data.order.direction == 'ASC'}DESC{else}ASC{/if}" class="{if $ads.data.order.direction == 'ASC' and $ads.data.order.field == 'model'}asc order{elseif $ads.data.order.field == 'model'}desc order{/if}">
                    <span>модель</span>
                    <span class="direction"></span
                </a>
            </th>
            <th class="date_cr">
                <a href="{if $ads.url}{$ads.url}{else}{$routes.search}{/if}{$ads.data.html_query}&order=year&direction={if $ads.data.order.direction == 'ASC'}DESC{else}ASC{/if}" class="{if $ads.data.order.direction == 'ASC' and $ads.data.order.field == 'year'}asc order{elseif $ads.data.order.field == 'year'}desc order{/if}">
                    <span>год</span>
                    <span class="direction"></span
                </a>
            </th>
            <th colspan="2">двигатель</th>
            <th>
                <a href="{if $ads.url}{$ads.url}{else}{$routes.search}{/if}{$ads.data.html_query}&order=mileage&direction={if $ads.data.order.direction == 'ASC'}DESC{else}ASC{/if}" class="{if $ads.data.order.direction == 'ASC' and $ads.data.order.field == 'mileage'}asc order{elseif $ads.data.order.field == 'mileage'}desc order{/if}">
                    <span>пробег</span>
                    <span class="direction"></span
                </a>
            </th>
            <th></th>
            <th>
                <a href="{if $ads.url}{$ads.url}{else}{$routes.search}{/if}{$ads.data.html_query}&order=price&direction={if $ads.data.order.direction == 'ASC'}DESC{else}ASC{/if}" class="{if $ads.data.order.direction == 'ASC' and $ads.data.order.field == 'price'}asc order{elseif $ads.data.order.field == 'price'}desc order{/if}">
                    <span>цена</span>
                    <span class="direction"></span
                </a>
            </th>
            <th width="88">
                <div class="currency_type cur_{$currency}">
                    {foreach from=$component_data.currency key=value item=cur}
                        <span onclick="auto.models.currency.setCurrency({$value}, true);">{$cur}</span>
                    {/foreach}
                </div>
            </th>
        {else}
            <th class="list_date">дата</th>
            <th></th>
            <th>модель</th>
            <th class="date_cr">год</th>
            <th colspan="2">двигатель</th>
            <th>пробег</th>
            <th></th>
            <th>цена</th>
            <th width="88">
                <div class="currency_type cur_{$currency}">
                    {foreach from=$component_data.currency key=value item=cur}
                        <span onclick="auto.models.currency.setCurrency({$value}, true);">{$cur}</span>
                    {/foreach}
                </div>
            </th>
        {/if}
        </thead>
        <tbody>
        {foreach from=$ads.data.list item=ad}
            <tr align="center" title="нажмите для перехода к объявлению" class="aditem{if $ad.show < 2} ad_hidden{/if}" style="{if $ad.background_color and $ad.p_bg_date_u >= $smarty.now} background: {$ad.background_color};{/if} {if !$ad.auto_status}text-decoration: line-through; {/if}">
                <td class="list_date">
                    <a href="{$ad.link}">
                        <span class="ad_hidden-ico" title="Данное объявление видно только вам, оно скрыто из результатов поиска"></span>
                        <span>{$ad.created_at|date_format:'%d-%m'}</span>
                        {if $ad.ordering >$smarty.now}
                            <span class="ad_pin-ico"></span>
                        {else}
                            {if $ad.ordering > $ad.date_created_u}
                                <span class="ad_up-ico"></span>
                            {/if}
                        {/if}
                    </a>
                </td>
                <td width="85">
                    <a href="{$ad.link}">
                        {if $ad.hasPhoto}
                            <img src="{$ad.photo.small}" alt="{$ad.title_vendor} {$ad.title_model}" width="85" height="75" title="{$ad.title_vendor} {$ad.title_model}">
                        {else}
                            <img src="/components/{$smarty.const.COMPONENT_NAME}/styles/default/img/no_auto_photo_85x75.png" alt="no image" width="85" height="75">
                        {/if}
                    </a>
                </td>
                <td><a href="{$ad.link}">{$ad.title_vendor} {$ad.title_model}</a></td>
                <td class="date_cr">
                    <a href="{$ad.link}">{$ad.date_construct}</a>
                </td>
                <td align="right">
                    <a href="{$ad.link}">{if $ad.volume > 0}{$ad.volume}{/if}</a>
                </td>
                <td>
                    <a href="{$ad.link}" style="line-height: 25px;">
                        <div>{$component_data.fueltype[$ad.fueltype]}</div>
                        <div>{$component_data.transmission[$ad.transmission]}</div>
                        <div>{$component_data.drive[$ad.drive]}</div>
                    </a>
                </td>
                <td>
                    <a href="{$ad.link}">
                        {if !$ad.is_new_auto}
                            <div>{if $ad.mileage}{$ad.mileage}{else}-{/if}</div>
                        {else}
                            <span>новый</span>
                        {/if}
                    </a>
                </td>
                <td vlign="midle">
                    <a href="{$ad.link}">
                        {if $ad.is_undocumented}
                            <div class="aditem_spec_icon is_undoc" title="Без документов"></div>
                        {/if}
                        {if $ad.is_broken}
                            <div class="aditem_spec_icon is_broken" title="Битый или не на ходу"></div>
                        {/if}
                        {if $ad.is_cut}
                            <div class="aditem_spec_icon is_cut" title="Конструктор"></div>
                        {/if}
                    </a>
                </td>
                <td colspan="2">
                    <a href="{$ad.link}" style="line-height: 35px;">
                        <div>{$ad.price[$currency]|number_format:0:"":" "}&nbsp;{$component_data.currency[$currency]}</div>
                        <div class="geo">{$ad.city_title}</div>
                    </a>
                </td>
            </tr>
        {/foreach}
        </tbody>
    </table>

    {$ads.data.pager}
{else}
    <div style="text-align: center; margin: 30px 0;">К сожалению, по заданным условиям не найдено ни одного объявления. Попробуйте изменить условия поиска.</div>
{/if}
