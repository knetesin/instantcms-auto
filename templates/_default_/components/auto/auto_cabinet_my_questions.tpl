<H3>Список ваших вопросов</H3>
{literal}
    <style>
        .aditem_q-container{
            background: #EBEBEB;
            padding: 5px;
            margin-bottom: 10px;
        }

        .aditem_q-container .aditem{
            background: rgb(224, 224, 224);
            padding: 5px;
        }

        .qs_btn_container:hover{
            color: red;
        }

        .qs_btn_container .show{
            vertical-align: -12px;
        }

        .qs_btn_container:hover .c-btn.show{
            background-position: -160px 0!important;
        }

    </style>

    <script>
        $(function(){
            $('.show_questions').on('click', function(){
                if(!$(this).hasClass('open')){
                    $(this).parents('.aditem_q-container').find('.auto-cabinet-question-container').slideDown();
                    $(this).find('.show_title').html('Скрыть вопросы');
                } else {
                    $(this).parents('.aditem_q-container').find('.auto-cabinet-question-container').slideUp();
                    $(this).find('.show_title').html('Показать вопросы');
                }
                $(this).toggleClass('open');
            });
        });
    </script>
{/literal}

<div class="list_units">

    {foreach from=$data_q.ads.list item=ad}
        <div class="aditem_q-container">
            <div class="aditem">
                <div class="d-inline">
                    {if $ad.hasPhoto}
                        <img src="{$ad.photo.small}" alt="" width="85" height="75">
                    {else}
                        <img src="/components/{$smarty.const.COMPONENT_NAME}/styles/default/img/no_auto_photo_85x75.png" alt="no image" width="85" height="75">
                    {/if}
                </div>
                <div class="d-inline">
                    {$ad.title_vendor} {$ad.title_model}
                    <div>
                        <span class="show_questions auto-ajax-link">
                            <span class="show_title">Показать вопросы</span>
                            <span>({$data_q.question[$ad.id]|@count})</span>
                        </span>
                    </div>
                </div>
                <div class="d-inline" style="float: right">
                    <div class="controls-btns">
                        <a href="{$routes.show|sprintf:$ad.id}" class="qs_btn_container" title="Открыть объявление">
                            <span class="c-btn show"></span>
                            <span>перейти к объявлению</span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="auto-cabinet-question-container" style="display: none;">
                {foreach from=$data_q.question[$ad.id] item=question}
                    <div>
                        <div class="item-contacts">
                            <div class="contacts-title">Состояние:&nbsp;</div>
                            <div>
                                {if $question.answer}
                                    <span>Ответ был получен {$question.answer_date|date_format:'%d.%m.%Y в %H:%M'}</span>
                                    <div>
                                        <a href="{$routes.show_question|sprintf:$ad.id:$question.id}">перейти к сообщению</a>
                                    </div>
                                {else}
                                    <span>Ответ еще не получен!</span>
                                {/if}
                            </div>
                        </div>

                        <div id="question-{$question.id}" class="item_question">

                            <div class="question">
                                <div class="questionar">&nbsp;</div>
                                <div>
                                    <span class="question-author ">{$question.author}</span>
                                    <span class="question-date">({$question.created_at|date_format:'%d.%m.%Y в %H:%M'})</span>

                                    {if $user_id == $question.id_user and $user_id}
                                        <span class="question-owner">ваш вопрос</span>
                                    {/if}
                                </div>
                                <p>{$question.message}</p>

                                {if $user_id == $ad.data.id_owner or ($user_id == $question.id_user and $user_id)}
                                    <span>Контактные данные:&nbsp;</span>
                                    <div class="content_item_phone">{$question.email}</div>
                                    <div class="content_item_phone">{$question.phone}</div>
                                {/if}

                                {if $question.show_type == 2 and ($user_id == $ad.data.id_owner or $user_id == $question.id_user)}
                                    <div class="question-show_type">Видно только вам и продавцу</div>
                                {/if}
                            </div>
                            {if $question.answer}
                                <div class="answer_layer">
                                    <div class="answer">{$question.answer}<div>&nbsp;</div></div>
                                </div>
                            {/if}
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    {foreachelse}
        <div class="list_no_item">Вы еще не задавали ни одного вопроса!</div>
    {/foreach}


</div>