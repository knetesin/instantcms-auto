{literal}
    <script>
        $(function(){
            $('#paid_services_show').on('click', function(){
                $('#steap-1').show();
                $('#steap-2').hide();
                $.fancybox.open($('#paid_services'));
                $('#pais_services_complete').show();
                $('#service-result').empty();
            });

            $('.paid_service').on('click', function(){
                if(!$(this).hasClass('inactive')){
                    auto.data.container.paidservice = $(this).data('paid-type');
                    $('#paid_service_container').html($(this).clone());
                    $('#steap-1').hide("slide", { direction: "right" }, 300, function(){
                        $('#steap-2').show("slide",{direction: 'right'}, 300);
                    });
                }
            });

            $('#paid_services_back').on('click', function(){
                $('#steap-2').hide("slide", { direction: "right" }, 300, function(){
                    $('#steap-1').show("slide",{direction: 'right'}, 300);
                    $('#pais_services_complete').show();
                    $('#service-result').empty();
                });
            });

            $('#pais_services_complete').on('click', function(){
                var $btn = $(this);
                if(!$btn.hasClass('process')){
                    $btn.addClass('process');

                    auto.models.paidservices.holding(function(data){
                        $btn.removeClass('process');

                        if(data.status == 'ok'){
                            $('#steap-1').find(".paid_service[data-paid-type='" + auto.data.container.paidservice + "']").addClass('inactive');
                            $('#service-result').html('Операция успешно проведена!');
                            $btn.hide();
                        } else {
                            alert(data.message);
                        }

                    });
                }
            });
        })
    </script>
{/literal}

<div id="paid_services"  class="paid-services-container no-print" style="display: none;">
    <div id="steap-1">
        <h4>Привлечь к объявлению больше внимания</h4>

        <div>
            <div class="paid_service{if $ad.ordering >$smarty.now} inactive{/if}" data-paid-type="up">
                <div class="paid_ser_title">Поднять объявление <span class="service_cost">{$cfg.services.up.cost} баллов</span></div>

                {if $ad.ordering >$smarty.now}
                    <div class="inactive-hint">Услуга не может быть активирована! В данный момент объявление прикреплено</div>
                {/if}

                <div class="paid_description-container paid-up">
                    <span class="paid-ico"></span>
                    <div class="paid_description">
                        Объявления отсортированы по дате размещения. Как правило, ваше объявление наиболее активно будут смотреть лишь в первую неделю, а затем интерес к нему начнет снижаться. Чтобы снова привлечь внимание к объявлению, воспользуйтесь услугой "Поднять объявление".
                    </div>
                </div>

            </div>
            <div class="paid_service{if $ad.p_vip_date_u >$smarty.now} inactive{/if}" data-paid-type="premium">
                <div class="paid_ser_title">Продлить Спецразмещение <span class="service_cost">{$cfg.services.vip.cost} баллов</span> <span class="service_duration">{$ad.data.services.duration.vip}</span></div>

                {if $ad.p_vip_date_u >$smarty.now}
                    <div class="inactive-hint">Услуга не может быть активирована! В данный момент услуга активна. Истекает: {$ad.vip_date_expiration|date_format:'%d.%m.%Y в %H:%M'}</div>
                {/if}

                <div class="paid_description-container paid-premium">
                    <span class="paid-ico"></span>
                    <div class="paid_description">
                        Спецразмещение - этот блок фотографий увеличенного размера, показывающийся в верхней части страниц
                    </div>
                </div>
            </div>
            <div class="paid_service{if $ad.ordering >$smarty.now} inactive{/if}" data-paid-type="attach" >
                <div class="paid_ser_title">Прикрепить объявление <span class="service_cost">{$cfg.services.attach.cost} баллов</span> <span class="service_duration">{$ad.data.services.duration.attach}</span></div>

                {if $ad.ordering >$smarty.now}
                    <div class="inactive-hint">Услуга не может быть активирована! В данный момент услуга активна. Истекает: {$ad.ordering|date_format:'%d.%m.%Y в %H:%M'}</div>
                {/if}

                <div class="paid_description-container paid-attach">
                    <span class="paid-ico"></span>
                    <div class="paid_description">
                        Прикрепленные объявления (с изображением кнопки) показываются на первых позициях в списке объявлений.
                        Если по текущим условиям выборки есть несколько прикрепленных объявлений, сначала показываются те, которые были прикреплены позже.
                    </div>
                </div>
            </div>
            <div class="paid_service{if $ad.p_bg_date_u >$smarty.now} inactive{/if}" data-paid-type="up_bg">
                <div class="paid_ser_title">Поднять и выделить объявление цветом <span class="service_cost">{$cfg.services.up_bg.cost} баллов</span> <span class="service_duration">{$ad.data.services.duration.up_bg}</span></div>

                {if $ad.p_bg_date_u >$smarty.now}
                    <div class="inactive-hint">Услуга не может быть активирована! В данный момент услуга активна. Истекает: {$ad.background_date_expiration|date_format:'%d.%m.%Y в %H:%M'}</div>
                {/if}

                <div class="paid_description-container paid-up_bg">
                    <span class="paid-ico"></span>
                    <div class="paid_description">
                        Вы можете одновременно с поднятием объявления выделить голубым цветом строку с объявлением в списке. Объявления, выделенные цветом, привлекают больше внимания.

                        Объявление выделяется цветом на 30 дней.
                    </div>
                </div>
            </div>
        </div>
        <div>
            <span>Ваш баланс: {$billing.balance}</span>
            <a href="/billing">пополнить баланс</a>
        </div>

    </div>

    <div id="steap-2" style="display: none;">
        <h4>Вы выбрали услугу</h4>
        <div id="paid_service_container" class="paid-services-container"></div>

        <div id="service-result"></div>

        <button id="paid_services_back">назад</button>
        <button id="pais_services_complete">оплатить</button>
        <button style="float: right" onclick="$.fancybox.close();">отмена</button>
    </div>

</div>