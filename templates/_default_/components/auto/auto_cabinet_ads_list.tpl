{literal}
    <script>
        auto.init('cabinet_ads');
    </script>
{/literal}

<H3>Список ваших объявлений</H3>

{if $data.list|@count}
    <table class="list_units autoshop_cabinet_list">
        <thead>
            <th>
                <a href="{$routes.cabinet.adslist}{$data.html_query}&order=id&direction={if $data.order.direction == 'ASC'}DESC{else}ASC{/if}" class="{if $data.order.direction == 'ASC' and $data.order.field == 'id'}asc order{elseif $data.order.field == 'id'}desc order{/if}">
                    <span>#</span>
                    <span class="direction"></span
                </a>
            </th>
            <th>
                <a href="{$routes.cabinet.adslist}{$data.html_query}&order=date&direction={if $data.order.direction == 'ASC'}DESC{else}ASC{/if}" class="{if $data.order.direction == 'ASC' and $data.order.field == 'date'}asc order{elseif $data.order.field == 'date'}desc order{/if}">
                    <span>дата создания</span>
                    <span class="direction"></span
                </a>
            </th>
            <th></th>
            <th>
                <a href="{$routes.cabinet.adslist}{$data.html_query}&order=model&direction={if $data.order.direction == 'ASC'}DESC{else}ASC{/if}" class="{if $data.order.direction == 'ASC' and $data.order.field == 'model'}asc order{elseif $data.order.field == 'model'}desc order{/if}">
                    <span>aвтомобиль</span>
                    <span class="direction"></span
                </a>
            </th>
            <th>
                <a href="{$routes.cabinet.adslist}{$data.html_query}&order=views&direction={if $data.order.direction == 'ASC'}DESC{else}ASC{/if}" class="{if $data.order.direction == 'ASC' and $data.order.field == 'views'}asc order{elseif $data.order.field == 'views'}desc order{/if}">
                    <span>просмотров</span>
                    <span class="direction"></span
                </a>
            </th>
            <th>вопросов</th>
            <th>
                <a href="{$routes.cabinet.adslist}{$data.html_query}&order=status&direction={if $data.order.direction == 'ASC'}DESC{else}ASC{/if}" class="{if $data.order.direction == 'ASC' and $data.order.field == 'status'}asc order{elseif $data.order.field == 'status'}desc order{/if}">
                    <span>статус</span>
                    <span class="direction"></span
                </a>
            </th>
            <th>действия</th>
        </thead>
        <tbody>
            {foreach from=$data.list item=ad}
                <tr class="aditem cabinet{if $ad.auto_status == 0} auto_sold{/if}" data-ad-id="{$ad.id}">
                    <td>
                        <span>{$ad.id}</span>
                    </td>
                    <td>
                        <span>{$ad.created_at|date_format:'%d.%m.%Y'}</span>
                    </td>
                    <td>
                        <a href="{$ad.link}">
                            {if $ad.hasPhoto}
                                <img src="{$ad.photo.small}" alt="" width="85" height="75">
                            {else}
                                <img src="/components/{$smarty.const.COMPONENT_NAME}/styles/default/img/no_auto_photo_85x75.png" alt="no image" width="85" height="75">
                            {/if}
                        </a>
                    </td>
                    <td>
                        <span>{$ad.title_vendor} {$ad.title_model}</span>
                    </td>
                    <td>
                        <span>{$ad.count_views}</span>
                    </td>
                    <td class="questions_control" title="перейти к управлению вопросами">
                        <a href="{$routes.cabinet.questions|sprintf:$ad.id}">
                            <span class="count_questions">{$ad.count_q}</span>
                            {if $ad.countnew}<span class="new">(новых: {$ad.count_q_new})</span>{/if}
                        </a>
                    </td>
                    <td {if $ad.status==0}class="auto_sold"{/if}>
                        <div style="font-weight: bold">
                            {if $ad.show == 3}Опубликовано
                            {elseif $ad.show == 2}На постмодерации
                            {elseif $ad.show == 1}На премодерации
                            {elseif $ad.show == 0}не опубликовано
                            {/if}
                        </div>
                        <div class="auto_status">
                            {if $ad.auto_status == 0}
                                <span>Машина продана</span>
                            {elseif $ad.auto_status == 1}
                                <span>В наличии</span>
                            {elseif $ad.auto_status == 2}
                                <span>В пути</span>
                            {elseif $ad.auto_status == 3}
                                <span>Под заказ</span>
                            {/if}
                        </div>
                    </td>
                    <td class="controls-btns">
                        {if $ad.auto_status==0}
                            <a href="{$routes.show|sprintf:$ad.id}" title="Открыть объявление"  class="c-btn show"></a>
                        {else}
                            <a href="{$routes.cabinet.edit|sprintf:$ad.id}" title="Редактирование" class="c-btn edit"></a>
                            <a href="{$routes.cabinet.photomanager|sprintf:$ad.id}" title="Управление фотографиями" class="c-btn pm"></a>
                            <a href="{$routes.show|sprintf:$ad.id}" title="Открыть объявление" class="c-btn show"></a>
                            <div style="margin-top: 10px">
                                <span class="btn_sold"  unit="{$data[list].id}">Отметить как проданное</span>
                            </div>
                        {/if}
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>

    {$data.pager}
{else}
    <div style="text-align: center; padding: 10px;">У вас еще нету добавленных объявлений</div>
{/if}

<table width="100%">
    <tbody>
        <tr>
            <td width="70%">{$pager} </td>
            <td align="right">Всего объявления: <b>{$count}</b></td>

        </tr>
    </tbody>
</table>

<div id="sell_auto" style="display: none; width: 400px;">
    <h6>Подтвердите продажу авто</h6>
    <p>Внимание! после потдверждения вы не сможете изменять любую информацию касательно данного объявления!</p>
    <div style="margin-top: 15px">
        <button id="set_sell" data-ad-id="{$ad.data.id}">Подтверждаю</button>
        <button onclick="$.fancybox.close();">Отмена</button>
    </div>
</div>



