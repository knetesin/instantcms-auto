<!-- AUTO COMPONENT STYLE -->
<link type="text/css" rel="stylesheet" href="/components/{$smarty.const.COMPONENT_NAME}/styles/admin/admin.css">
<link type="text/css" rel="stylesheet" href="/components/{$smarty.const.COMPONENT_NAME}/styles/default/css/main.css">
<script type="text/javascript" src="/components/{$smarty.const.COMPONENT_NAME}/js/auto.js"></script>
<script type="text/javascript" src="/components/{$smarty.const.COMPONENT_NAME}/js/jquery.minicolors.min.js"></script>
{literal}
    <script>
        $(function(){
           $('.colorpicker').minicolors();
        });
    </script>
{/literal}
<!-- END AUTO COMPONENT STYLE -->

<div class="component-auto auto-cabinet">
    {if $opt == 'config'}
        {include file="admin_configs.tpl"}
    {elseif $opt == 'list'}
        {$data}
    {elseif $opt == 'add'}
        {if $sub_opt == 'vendor'}
            <h1>Добавление производителя</h1>
            {include file="forms/admin_add_edit_vendor.tpl"}
        {elseif $sub_opt == 'model'}
            <h1>Добавление модели</h1>
            {include file="forms/admin_add_edit_model.tpl"}
        {elseif $sub_opt == 'ad'}
            <h1>Добавление объявления</h1>
            {include file="forms/admin_add_edit_ad.tpl"}
        {/if}
    {elseif $opt == 'edit'}
        {if $sub_opt == 'vendor'}
            <h1>Редактирование производителя #{$vendor.id}</h1>
            {include file="forms/admin_add_edit_vendor.tpl"}
        {elseif $sub_opt == 'model'}
            <h1>Редактирование модели #{$model.id}</h1>
            {include file="forms/admin_add_edit_model.tpl"}
        {elseif $sub_opt == 'ad'}
            <h1>Редактирование объявления #{$ad.id}</h1>
            {include file="forms/admin_add_edit_ad.tpl"}
        {/if}
    {elseif $opt == 'parsers'}
        {include file="admin_parsers.tpl"}
    {else}
        {include file="admin_index.tpl"}
    {/if}
</div>
