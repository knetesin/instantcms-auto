<ul>
    <li>
        <span>Всего объявлений: </span>
        <strong>{$stats.count.all}</strong>
        <ul>
            <li>
                <span>Активных:</span>
                <strong>{$stats.count.active}</strong>
            </li>
            <li>
                <span>Проданных:</span>
                <strong>{$stats.count.sell}</strong>
            </li>
            <li>
                <span>На модерации:</span>
                <strong>{$stats.count.moderation}</strong>
            </li>
            <li>
                <span>Новых за сегодня:</span>
                <strong>{$stats.count.new}</strong>
            </li>
        </ul>
    </li>
</ul>