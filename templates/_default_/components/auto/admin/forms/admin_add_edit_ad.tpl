<script language="JavaScript" type="text/javascript" src="/components/{$smarty.const.COMPONENT_NAME}/js/ckeditor/ckeditor.js"></script>
<script language="JavaScript" type="text/javascript" src="/components/{$smarty.const.COMPONENT_NAME}/js/ckeditor/adapters/jquery.js"></script>

{literal}
    <script>
        auto.init('add');

        function checkOnSubmit(){
            var $vendor = $("select[name='vendor']"),
                    $model = $("select[name='model']"),
                    $date = $("select[name='date_constr']"),
                    $price = $("input[name='price']"),
                    $region = $("select[name='geo[region]']"),
                    $city = $("select[name='geo[city]']"),
                    err = 0,
                    message = [],
                    fields = [];

            if($vendor && $vendor.val() <= 0){
                fields.push($vendor);
                message.push('Производитель авто не указан!');
            }

            if($model && $model.val() <= 0){
                fields.push($model);
                message.push('Модель авто не указана!');
            }

            var dateVal = $date.val() >= 0 ? $date.val() : 0;
            if($date && dateVal <= 0){
                fields.push($date);
                message.push('Год выпуска не указан!');
            }

            if($price && $price.val() <= 0){
                fields.push($price);
                message.push('Укажите цену авто!');
            }

            if($region && $region.val() <= 0){
                fields.push($region);
                message.push('Укажите регион продажи!');
            }

            if($city && $city.val() <= 0){
                fields.push($city);
                message.push('Укажите город продажи!');
            }

            if(message.length){
                fields[0].focus();
                $.each(fields, function(i, val){
                    val.addClass('checkerr');
                });
                alert(message[0]);
                return false;
            } else {
                return true;
            }
        }
    </script>
{/literal}

{if $errors}
    <ul class="auto_err_msg">
        {foreach from=$errors item=error}
            <li class="error-field-desc" data-error-selector="{$error.selector}">
                <span>{$error.title}</span>
            </li>
        {/foreach}
    </ul>
{/if}

<form enctype="multipart/form-data" action="{$component_url}{if !$ad}&opt=add&sub_opt=ad{else}&opt=edit&sub_opt=ad&uid={$ad.id}{/if}" id="auto-add-form" method="post" onsubmit="return checkOnSubmit();" style="width: 800px;" >
<input type="hidden" value="1" name="submit">

<!-- MAIN SECTION -->
<table border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr class="te_top">
    <td class="te_img1">&nbsp;</td>
    <td class="te_img2">
        <p>{$LANG.TEMPLATE_ADD_TECHDATA_TITLE}</p>
    </td>
    <td class="te_img3">&nbsp;</td>
</tr>
<tr>
    <td class="te_imgl"></td>
    <td>
        <table class="add_data">
            <tbody>

            <tr>
                <td class="data_col1 data_title">
                    <span>{$LANG.TEMPLATE_ADD_TECHDATA_VENDOR_TITLE}</span>
                    {if !$is_edit}<span class="auto-field-req">*</span>{/if}
                </td>
                <td class="data_col2">
                    {if $is_edit and !$cfg.ad.add.can_edit_vendor}
                        <b>{$set.vendor.title}</b>
                    {else}
                        <select name="vendor" id="field-vendor" {if $errors.vendor}class="checkerr"{/if}>
                            <option value="0">{$LANG.TEMPLATE_ADD_SELECT_VENDOR}</option>
                            {foreach from=$data.vendors item=vendor name=foreach_vendor}
                                {assign var="prevIndex" value=$smarty.foreach.foreach_vendor.index-1}
                                {if !$vendor.popular and $data.vendors[$prevIndex].popular}
                                    <optgroup class="select_opt" label="-------------"></optgroup>
                                {/if}
                                <option value="{$vendor.id}" {if $set.vendor.id == $vendor.id}selected="selected"{/if}>{$vendor.title}</option>
                            {/foreach}
                        </select>
                    {/if}
                </td>

                <td class="data_col3 data_title">
                    <span>{$LANG.TEMPLATE_ADD_TECHDATA_MODEL_TITLE}</span>
                    {if !$is_edit}<span class="auto-field-req">*</span>{/if}
                </td>
                <td class="data_col4">
                    {if $is_edit and !$cfg.ad.add.can_edit_vendor}
                        <b>{$set.model.title}</b>
                    {else}
                        <select name="model" id="field-model" {if $errors.model} class="checkerr" {/if}>
                            <option value="0">{$LANG.TEMPLATE_ADD_SELECT_MODEL}</option>
                            {foreach from=$set.models item=model name=foreach_model}
                                {assign var="prevIndex" value=$smarty.foreach.foreach_model.index-1}
                                {if !$model.popular and $set.models[$prevIndex].popular}
                                    <optgroup class="select_opt" label="-------------"></optgroup>
                                {/if}
                                <option value="{$model.id}" {if $set.model.id == $model.id}selected="selected"{/if}>{$model.title}</option>
                            {/foreach}
                        </select>
                        <img style="display: none;" id="ajax_models_loader_pic" src="/components/{$smarty.const.COMPONENT_NAME}/styles/default/img/loading.gif">
                    {/if}
                </td>
            </tr>

            <tr>
                <td class="data_col1 data_title">
                    <span>{$LANG.TEMPLATE_ADD_TECHDATA_DATE_CONSTRUCT_TITLE}</span>
                    <span class="auto-field-req">*</span>
                </td>
                <td class="data_col2">
                    <select name="date_constr" id="field-date_construct" {if $errors.date_constr}class="checkerr"{/if}>
                        <option>{$LANG.TEMPLATE_ADD_SELECT_DATE}</option>
                        {foreach from=$data.date item=date name=foreach_date}
                            <option value="{$date}" {if $set.date_constr == $date} selected="selected" {/if}>{$date}</option>
                        {/foreach}
                    </select>
                </td>

                <td class="data_col3 data_title">
                    <span>{$LANG.TEMPLATE_ADD_TECHDATA_WHEEL_TITLE}</span>
                </td>
                <td class="data_col4">
                    {foreach from=$component_data.wheel key=id item=wheel name=foreach_wheel}
                        <label class="label-block">
                            <input type="radio" name="wheel" value="{$id}" {if $set.wheel == $id}checked="checked"{/if}>
                            <span>{$wheel}</span>
                        </label>
                    {/foreach}
                </td>
            </tr>

            <tr>
                <td class="data_col1 data_title">
                    <span>{$LANG.TEMPLATE_ADD_TECHDATA_MILEAGE_TITLE}</span>
                </td>
                <td class="data_col2">
                    <input name="mileage" type="text" maxlength="6" size="6" {if $set.mileage} value="{$set.mileage}" {/if}{if $set.is_new_auto == 1} disabled="disabled"  value="новое авто" {/if}>
                </td>

                <td class="data_col3 data_title">
                    <span>{$LANG.TEMPLATE_ADD_TECHDATA_VOLUME_TITLE}</span>
                </td>
                <td class="data_col4">
                    <input name="volume" type="text" maxlength="6" size="6" {if $set.volume > 0} value="{$set.volume}" {/if}>
                </td>
            </tr>

            <tr>
                <td class="data_col1"></td>
                <td class="data_col2">
                    <label class="label-block">
                        <input name="is_new_auto" id="new_auto" type="checkbox" value="1" {if $set.is_new_auto == 1} checked="checked" {/if}>
                        <span>новый автомобиль</span>
                    </label>
                    <label class="label-block">
                        <input name="is_without_rus_run" type="checkbox" value="1" {if $set.is_without_rus_run == 1}checked="checked"{/if}>
                        <span>без пробега по РФ</span>
                    </label>
                </td>

                <td class="data_col3 data_title">
                    <span>{$LANG.TEMPLATE_ADD_TECHDATA_FUEL_TITLE}</span>
                </td>
                <td class="data_col4">
                    {foreach from=$component_data.fueltype key=id item=fuel name=foreach_fuel}
                        <label class="label-block">
                            <input name="fueltype" type="radio" value="{$id}" {if $set.fueltype == $id}checked="checked"{/if}>
                            <span>{$fuel}</span>
                        </label>
                    {/foreach}
                </td>
            </tr>

            <tr>
                <td class="data_col1 data_title">
                    <span>{$LANG.TEMPLATE_ADD_TECHDATA_TRANSMISSION_TITLE}</span>
                </td>
                <td class="data_col2">
                    {foreach from=$component_data.transmission key=id item=transmission name=foreach_fuel}
                        <label class="label-block">
                            <input name="transmission" type="radio" value="{$id}" {if $set.transmission == $id}checked="checked"{/if}>
                            <span>{$transmission}</span>
                        </label>
                    {/foreach}
                </td>

                <td class="data_col3 data_title">
                    <span>{$LANG.TEMPLATE_ADD_TECHDATA_DRIVE_TITLE}</span>
                </td>
                <td class="data_col4">
                    {foreach from=$component_data.drive key=id item=drive name=foreach_fuel}
                        <label class="label-block">
                            <input name="drive" type="radio" value="{$id}" {if $set.drive == $id}checked="checked"{/if}>
                            <span>{$drive}</span>
                        </label>
                    {/foreach}
                </td>
            </tr>

            <tr>
                <td class="data_title">
                    <span>{$LANG.TEMPLATE_ADD_TECHDATA_SPECIAL_NOTES_TITLE}</span>
                </td>
                <td colspan="3">
                    <label class="label-block">
                        <input name="is_undocumented" type="checkbox" value="1" {if $set.is_undocumented == 1}checked="checked"{/if}>
                        <span>{$LANG.TEMPLATE_ADD_CHECKBOX_IS_UNDOCUMENTED}</span>
                    </label>
                    <label class="label-block">
                        <input name="is_broken" type="checkbox" value="1" {if $set.is_broken == 1}checked="checked"{/if}>
                        <span>{$LANG.TEMPLATE_ADD_CHECKBOX_IS_BROKEN}</span>
                    </label>
                    <label class="label-block">
                        <input name="is_cut" type="checkbox" value="1" {if $set.is_cut == 1}checked="checked"{/if}>
                        <span>{$LANG.TEMPLATE_ADD_CHECKBOX_IS_CUT}</span>
                    </label>
                </td>
            </tr>

            <tr>
                <td class="data_title">
                    <span>{$LANG.TEMPLATE_ADD_TECHDATA_DESCRIPTION_TITLE}</span>
                </td>
                <td colspan="3">
                    <textarea name="description" class="info">{$set.description}</textarea>
                    <div class="desc">{$LANG.TEMPLATE_ADD_TECHDATA_DESCRIPTION_NOTE}</div>
                </td>
            </tr>

            <tr>
                <td></td>
                <td colspan="3">
                    <label class="label-block">
                        <input name="is_gt" type="checkbox" value="1" {if $set.is_gt}checked="checked"{/if}>
                        <span>{$LANG.TEMPLATE_ADD_CHECKBOX_IS_GT}</span>
                    </label>
                    <div id="cgttext" {if !$set.is_gt}style="display: none;"{/if}>
                        <textarea cols="63" name="gt_description" class="info">{if $set.is_gt}{$set.gt_description}{/if}</textarea>
                        <div class="desc">{$LANG.TEMPLATE_ADD_TECHDATA_GT_DESCRIPTION_NOTE}</div>
                    </div>
                </td>
            </tr>

            </tbody>
        </table>
    </td>
    <td class="te_imgr"></td>
</tr>
<tr class="te_bottom">
    <td class="te_img1">&nbsp;</td>
    <td class="te_img2">&nbsp;</td>
    <td class="te_img3">&nbsp;</td>
</tr>
</tbody>
</table>
<!-- END MAIN SECTION -->

<!-- CONTACT AND PRICE SECTION -->
<table border="0" cellpadding="0" cellspacing="0">
    <tbody>
    <tr class="te_top">
        <td class="te_img1">&nbsp;</td>
        <td class="te_img2">
            <p>{$LANG.TEMPLATE_ADD_PRICEBLOCK_TITLE}</p>
        </td>
        <td class="te_img3">&nbsp;</td>
    </tr>
    <tr>
        <td class="te_imgl"></td>
        <td>
            <table class="add_data">
                <tbody>
                <tr>
                    <td class="data_col1 data_title">
                        <span>Цена:</span>
                        <span class="auto-field-req">*</span>
                    </td>
                    <td>
                        <input type="text" name="price" id="price" maxlength="8" size="8" {if $errors.price}class="checkerr"{/if} {if $set.price} value="{$set.price}" {/if} >
                        <select name="currency" {if $errors.currency}class="checkerr"{/if}>
                            {foreach from=$component_data.currency key=value item=cur name=foreach_currency}
                                <option value="{$value}" {if $set.currency == $value}selected="selected"{/if}>{$cur}</option>
                            {/foreach}
                        </select>
                        <div class="desc">Объявления с намеренно заниженной ценой немедленно удаляются!</div>
                    </td>
                </tr>

                <tr>
                    <td class="data_col1 data_title">
                        <span>Статус:</span>
                        <span class="auto-field-req">*</span>
                    </td>
                    <td>
                        {foreach from=$component_data.status key=id item=status name=foreach_status}
                            <label class="label-block">
                                <input name="status" type="radio" value="{$id}" {if $set.status == $id}checked="checked"{/if}>
                                <span>{$status}</span>
                            </label>
                        {/foreach}
                    </td>
                </tr>

                <tr>
                    <td class="data_col1 data_title">
                        <span>Город продажи:</span>
                        {if !$cfg.geo.city.default and !$is_edit}<span class="auto-field-req">*</span>{/if}</td>
                    <td>
                        {if $cfg.geo.region.default or ($is_edit  and !$cfg.ad.add.can_edit_geo)}
                            <b>{$set.geo.region.name}</b>
                        {else}
                            <select name="geo[region]" {if $errors.region} class="checkerr" {/if}>
                                {foreach from=$set.geo.regions key=value item=region name=foreach_region}
                                    <option value="{$value}" {if $set.geo.region.id == $value}selected="selected"{/if}>{$region}</option>
                                {/foreach}
                            </select>
                        {/if}

                        {if $cfg.geo.city.default or ($is_edit  and !$cfg.ad.add.can_edit_geo)}
                            <b>, {$set.geo.city.name}</b>
                        {else}
                            <select name="geo[city]" {if $errors.city} class="checkerr" {/if}>
                                {foreach from=$set.geo.cities key=value item=city name=foreach_cities}
                                    <option value="{$value}" {if $set.geo.city.id == $value}selected="selected"{/if}>{$city}</option>
                                {/foreach}
                            </select>
                        {/if}

                        {if !$cfg.geo.city.default and !$is_edit}<div class="desc">Указывайте реальный город продажи! Если машина находится в другом городе, объявление будет удалено.</div>{/if}
                    </td>
                </tr>

                <tr>
                    <td class="data_col1 data_title">
                        <span>Телефон:</span>
                        <span class="auto-field-req">*</span></td>
                    <td>
                        <div id="phone_layer">
                            <input name="phone1" type="text" class="phone {if $errors.phone1}checkerr{/if}" maxlength="18" size="15" {if $set.phone1}value="{$set.phone1}"{/if}>
                        </div>
                        <div id="phone2" {if !$set.phone2}style="display: none;"{/if}>
                            <input name="phone2" type="text" class="phone" maxlength="18" size="15" {if $set.phone2} value="{$set.phone2}"{/if}>
                        </div>

                        <div class="desc">Указывайте только ФЕДЕРАЛЬНЫЕ номера мобильных телефонов!</div>
                        {if !$set.phone2}
                            <div id="auto-add-addphone" class="auto-ajax-link">добавить еще номер</div>
                        {/if}
                    </td>
                </tr>

                {if !$user_id}
                    <tr>
                        <td class="data_col1 data_title">
                            <span>E-mail:</span>
                            <span class="auto-field-req">*</span></td>
                        </td>
                        <td>
                            <input name="email" type="text" maxlength="50" size="20" class="{if $errors.email}checkerr{/if}" {if $set.email}value="{$set.email}"{/if}>
                            <div class="desc">{$LANG.TEMPLATE_ADD_EMAIL_NOTE}</div>
                        </td>
                    </tr>
                {/if}

                <tr>
                    <td class="data_col1 data_title"></td>
                    <td>
                        <label class="label-block">
                            <input name="access_issues" type="checkbox" value="1" {if $set.access_issues}checked="checked"{/if}>
                            <span>{$LANG.TEMPLATE_ADD_ACCESS_ISSUES_NOTE}</span>
                        </label>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td class="te_imgr"></td>
    </tr>
    <tr class="te_bottom">
        <td class="te_img1">&nbsp;</td>
        <td class="te_img2">&nbsp;</td>
        <td class="te_img3">&nbsp;</td>
    </tr>
    </tbody>
</table>
<!-- END CONTACT AND PRICE SECTION -->

{if $is_admin}
    {literal}
        <script>
            $(function(){
                $("input[name='admin[bg][color]']").minicolors();
            });
        </script>
    {/literal}
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
        <tr class="te_top">
            <td class="te_img1">&nbsp;</td>
            <td class="te_img2">
                <p>Административная панель</p>
            </td>
            <td class="te_img3">&nbsp;</td>
        </tr>
        <tr>
            <td class="te_imgl"></td>
            <td>
                <table class="add_data">
                    <tbody>
                    <tr>
                        <td class="data_col1 data_title">
                            <span>Модерация:</span>
                        </td>
                        <td>
                            {foreach from=$component_data.moderation key=id item=status name=foreach_moderation}
                                <label class="label-block">
                                    <input name="admin[moderation]" type="radio" value="{$id}" {if $set.show == $id}checked="checked"{/if}>
                                    <span>{$status}</span>
                                </label>
                            {/foreach}
                            <hr>
                        </td>
                    </tr>

                    <tr>
                        <td class="data_col2 data_title">
                            <span>Премиум объявление:</span>
                        </td>

                        <td class="admin-service-container{if !$set.admin.vip.active} disabled{/if}">
                            <label>
                                <input name="admin[vip][active]" type="radio" value="0" {if !$set.admin.vip.active}checked{/if}>
                                <span>нет</span>
                            </label>
                            <label>
                                <input name="admin[vip][active]" type="radio" value="1" {if $set.admin.vip.active}checked{/if}>
                                <span>да</span>
                            </label>

                            <div class="admin-service-info">
                                <span>Длительность</span>
                                <input name="admin[vip][duration]" type="text" value="{$set.admin.vip.duration}">
                                <span>сек</span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="data_col2 data_title">
                            <span>Выделить цветом:</span>
                        </td>
                        <td class="admin-service-container{if !$set.admin.bg.active} disabled{/if}">
                            <label>
                                <input name="admin[bg][active]" type="radio" value="0" {if !$set.admin.bg.active}checked{/if}>
                                <span>нет</span>
                            </label>
                            <label>
                                <input name="admin[bg][active]" type="radio" value="1" {if $set.admin.bg.active}checked{/if}>
                                <span>да</span>
                            </label>

                            <div class="admin-service-info">
                                <div>
                                    <span>Длительность</span>
                                    <input name="admin[bg][duration]" type="text" value="{$set.admin.bg.duration}">
                                    <span>сек</span>
                                </div>
                                <div>
                                    <span>Цветом</span>
                                    <input name="admin[bg][color]" type="text" value="{$set.admin.bg.color}">
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="data_col2 data_title">
                            <span>Прикрепить объявление:</span>
                        </td>
                        <td class="admin-service-container{if !$set.admin.attach.active} disabled{/if}">
                            <label>
                                <input name="admin[attach][active]" type="radio" value="0" {if !$set.admin.attach.active}checked{/if}>
                                <span>нет</span>
                            </label>
                            <label>
                                <input name="admin[attach][active]" type="radio" value="1" {if $set.admin.attach.active}checked{/if}>
                                <span>да</span>
                            </label>
                            <div class="admin-service-info">
                                <span>Длительность</span>
                                <input name="admin[attach][duration]" type="text" value="{$set.admin.attach.duration}">
                                <span>сек</span>
                            </div>
                        </td>
                    </tr>


                    </tbody>
                </table>
            </td>
            <td class="te_imgr"></td>
        </tr>
        <tr class="te_bottom">
            <td class="te_img1">&nbsp;</td>
            <td class="te_img2">&nbsp;</td>
            <td class="te_img3">&nbsp;</td>
        </tr>
        </tbody>
    </table>
{/if}


<div id="submit_add_data">
    <input type="submit" class="" value="{if $is_edit}Сохранить объявление{else}Перейти к добавлению фото{/if}">
</div>
</form>




