{literal}
<script>
    function checkOnSubmit()
    {
        var $title = $('input[name="title"]');

        if(!$title.val()){
            $title.focus();
            alert('Название не может быть пустым!');
            return false;
        }

        return true;
    }
</script>
{/literal}

<form action="{$component_url}{if !$vendor}&opt=add_form&sub_opt=vendor{else}&opt=edit_form&sub_opt=vendor&uid={$vendor.id}{/if}" method="post" enctype="multipart/form-data" onsubmit="return checkOnSubmit();">

    <table width="100%" border="0" cellpadding="10" cellspacing="0" class="proptable">
        <tbody><tr>
            <td width="150"><strong>Название</strong></td>
            <td width="" valign="top">
                <input name="title" type="text" value="{$vendor.title}" style="width: 300px;">
            </td>
        </tr>
        <tr>
            <td><strong>Категория</strong></td>
            <td width="" valign="top">
                <select name="cat">
                    <option>не указана</option>
                    {foreach from=$vendor_cats item=cat}
                        <option value="{$cat.id}" {if $vendor.cats_id == $cat.id}selected="selected"{/if}>{$cat.title}</option>
                    {/foreach}
                </select>
            </td>
        </tr>

        <tr>
            <td><strong>Популярный производитель</strong></td>
            <td width="" valign="top">
                <label>
                    <input name="popular" type="radio" value="0" {if $vendor.popular == 0}checked{/if}>
                    <span>Нет</span>
                </label>
                <label>
                    <input name="popular" type="radio" value="1" {if $vendor.popular == 1}checked{/if}>
                    <span>Да</span>
                </label>
            </td>
        </tr>

        <tr>
            <td><strong>Логотип</strong></td>
            <td>
                {if $vendor.logo}
                    <img src="{$cfg.system.upload_dir}vendors/small/{$vendor.logo}">
                    <a href="{$component_url}&opt=delete&sub_opt=logo&uid={$vendor.id}">Удалить логотип</a>
                    <div>
                        <input type="file" name="logo">
                    </div>
                {else}
                    <input type="file" name="logo">
                {/if}
            </td>
        </tr>

        </tbody></table>
    <p>
        <input name="save" type="submit" id="save" value="Сохранить" class="button">
        <input name="back" type="button" id="back" value="Отмена" onclick="window.location.href='{$component_url}&opt=list&sub_opt=vendor';" class="button">
    </p>
</form>