{literal}
    <script>
        function checkOnSubmit()
        {
            var $title = $('input[name="title"]');

            if(!$title.val()){
                $title.focus();
                alert('Название не может быть пустым!');
                return false;
            }

            return true;
        }
    </script>
{/literal}

<form action="{$component_url}{if !$model}&opt=add_form&sub_opt=model{else}&opt=edit_form&sub_opt=model&uid={$model.id}{/if}" method="post" enctype="multipart/form-data" onsubmit="return checkOnSubmit();">

    <table width="100%" border="0" cellpadding="10" cellspacing="0" class="proptable">
        <tbody>
            <tr>
                <td width="150"><strong>Название</strong></td>
                <td width="" valign="top">
                    <input name="title" type="text" value="{$model.title}" style="width: 300px;">
                </td>
            </tr>

            <tr>
                <td><strong>Производитель</strong></td>
                <td width="" valign="top">
                    <select name="vendor_id">
                        <option value="0">не указан</option>
                        {foreach from=$vendors item=vendor}
                            <option value="{$vendor.id}" {if $model.vendor_id == $vendor.id}selected="selected"{/if}>{$vendor.title}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>

            <tr>
                <td><strong>Тип кузова</strong></td>
                <td width="" valign="top">
                    <select name="carcase_id">
                        <option value="0">не указан</option>
                        {foreach from=$carcase_cats item=cat}
                            <option value="{$cat.id}" {if $model.case_id == $cat.id}selected="selected"{/if}>{$cat.title}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>

            <tr>
                <td><strong>Популярная модель</strong></td>
                <td width="" valign="top">
                    <label>
                        <input name="popular" type="radio" value="0" {if $model.popular == 0}checked{/if}>
                        <span>Нет</span>
                    </label>
                    <label>
                        <input name="popular" type="radio" value="1" {if $model.popular == 1}checked{/if}>
                        <span>Да</span>
                    </label>
                </td>
            </tr>

        </tbody>
    </table>
    <p>
        <input name="save" type="submit" id="save" value="Сохранить" class="button">
        <input name="back" type="button" id="back" value="Отмена" onclick="window.location.href='{$component_url}&opt=list&sub_opt=model';" class="button">
    </p>
</form>