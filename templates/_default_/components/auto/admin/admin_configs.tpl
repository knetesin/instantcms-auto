
<form action="index.php?view=components&amp;do=config&amp;id={$componentId}&opt=saveconfig" method="post" name="optform">

<input type="hidden" name="csrf_token" value="<?php echo cmsUser::getCsrfToken(); ?>" />

<div id="config_tabs" style="margin-top:12px;" class="uitabs">

<ul id="tabs">
    <li>
        <a href="#system">
            <span>{$LANG.ADMIN_TAB_SYSTEM}</span>
        </a>
    </li>
    <li>
        <a href="#ad">
            <span>{$LANG.ADMIN_TAB_AD}</span>
        </a>
    </li>
    <li>
        <a href="#images">
            <span>{$LANG.ADMIN_TAB_IMAGES}</span>
        </a>
    </li>
    <li>
        <a href="#geo">
            <span>{$LANG.ADMIN_TAB_GEO}</span>
        </a>
    </li>
    <li>
        <a href="#services">
            <span>{$LANG.ADMIN_TAB_SERVICES}</span>
        </a>
    </li>
</ul>

<div id="system">
    {literal}
        <script>

        </script>
    {/literal}
    <table width="550" border="0" cellpadding="10" cellspacing="0" class="proptable" style="border:none">

        <tr>
            <td><strong>{$LANG.ADMIN_TAB_SYSTEM_DEBUG}: </strong></td>
            <td width="230">
                <label>
                    <input name="system[debug]" type="radio" value="1" {if $cfg.system.debug == 1}checked="checked"{/if} />
                    <span>{$LANG.YES}</span>
                </label>
                <label>
                    <input name="system[debug]" type="radio" value="0" {if !$cfg.system.debug}checked="checked"{/if} />
                    <span>{$LANG.NO}</span>
                </label>
            </td>
        </tr>


        <tr>
            <td><strong>{$LANG.ADMIN_TAB_SYSTEM_UPLOAD_DIR}: </strong></td>
            <td width="">
                <input name="system[upload_dir]" type="text" value="{$cfg.system.upload_dir}"/>
            </td>
        </tr>

        <tr>
            <td><strong>{$LANG.ADMIN_TAB_SYSTEM_DATE_START}: </strong></td>
            <td width="">
                <input class="uispin" name="system[date_start]" type="text" value="{$cfg.system.date_start}"/>
            </td>
        </tr>


        <tr>
            <td><strong>{$LANG.ADMIN_TAB_SYSTEM_MODERATION_TYPE}:</strong></td>
            <td>
                <select name="system[moderation_type]">
                    <option value="0" {if !$cfg.system.moderation_type}selected="selected"{/if}>Без модерации</option>
                    <option value="1" {if $cfg.system.moderation_type == 1}selected="selected"{/if}>Пре-модерация</option>
                    <option value="2" {if $cfg.system.moderation_type == 2}selected="selected"{/if}>Пост-модерация</option>
                </select>
            </td>
        </tr>

    </table>

    <div>
        <strong>Категории</strong>
    </div>
    <table width="500" border="0" cellpadding="10" cellspacing="0" class="proptable" style="border:none">

        <tr>
            <td><strong>{$LANG.ADMIN_TAB_SYSTEM_SHOWLOGO_CATS}:</strong></td>
            <td>
                <label>
                    <input name="system[showlogocats]" type="radio" value="1" {if $cfg.system.showlogocats == 1}checked="checked"{/if} />
                    <span>{$LANG.YES}</span>
                </label>
                <label>
                    <input name="system[showlogocats]" type="radio" value="0" {if !$cfg.system.showlogocats}checked="checked"{/if} />
                    <span>{$LANG.NO}</span>
                </label>
            </td>
        </tr>

        <tr>
            <td><strong>{$LANG.ADMIN_TAB_SYSTEM_SHOWNULL_CATS}:</strong></td>
            <td>
                <label>
                    <input name="system[showNullCats]" type="radio" value="1" {if $cfg.system.showNullCats == 1}checked="checked"{/if} />
                    <span>{$LANG.YES}</span>
                </label>
                <label>
                    <input name="system[showNullCats]" type="radio" value="0" {if !$cfg.system.showNullCats}checked="checked"{/if} />
                    <span>{$LANG.NO}</span>
                </label>
            </td>
        </tr>

        <tr>
            <td><strong>Группировать русских производителей:</strong></td>
            <td>
                <label>
                    <input name="system[groupRusVendors]" type="radio" value="1" {if $cfg.system.groupRusVendors == 1}checked="checked"{/if} />
                    <span>{$LANG.YES}</span>
                </label>
                <label>
                    <input name="system[groupRusVendors]" type="radio" value="0" {if !$cfg.system.groupRusVendors}checked="checked"{/if} />
                    <span>{$LANG.NO}</span>
                </label>
            </td>
        </tr>

        <tr>
            <td><strong>Количество колонок на главной:</strong></td>
            <td>
                <label>
                    <input class="uispin size-100" min="0" max="10" name="system[countColumnMain]" type="text" value="{$cfg.system.countColumnMain}"/>
                </label>
            </td>
        </tr>


        <tr>
            <td><strong>Количество колонок на других:</strong></td>
            <td>
                <label>
                    <input class="uispin size-100" min="0" max="10" name="system[countColumnOther]" type="text" value="{$cfg.system.countColumnOther}"/>
                </label>
            </td>
        </tr>

        <tr>
            <td><strong>Раздел автозапчастей Bibinet:</strong></td>
            <td>
                <label>
                    <input name="system[spares][active]" type="radio" value="1" {if $cfg.system.spares.active == 1}checked="checked"{/if} />
                    <span>{$LANG.YES}</span>
                </label>
                <label>
                    <input name="system[spares][active]" type="radio" value="0" {if !$cfg.system.spares.active}checked="checked"{/if} />
                    <span>{$LANG.NO}</span>
                </label>
            </td>
        </tr>

    </table>

    <div>
        <div>
            <strong>Курсы валют</strong>
            <a href="index.php?view=components&amp;do=config&amp;id={$componentId}&opt=parse_currency">обновить</a>
            <span>(обновлено: {$show_currency.upd|date_format:'%d.%m.%Y в %T'})</span>
        </div>
    </div>
    <div>
        <table style="width: 500px;">
            <thead>
            <tr>
                <td></td>
                {foreach from=$show_currency.list key=name item=curArr }
                    <td class="currency-title">{$name}</td>
                {/foreach}
            </tr>
            </thead>
            <tbody>
            {foreach from=$show_currency.list key=name item=curArr }
                <tr>
                    <td class="currency-title">{$name}</td>
                    {foreach from=$curArr key=cname item=cur}
                        <td {if $cname == 'rub'}class="main-currency"{/if}>
                            {$cur}
                        </td>
                    {/foreach}
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
</div>

<div id="ad">
    <table width="450" border="0" cellpadding="10" cellspacing="0" class="proptable" style="border:none">

        <tr>
            <td><strong>Лимит фотографий</strong></td>
            <td width="210">
                <input class="size-100 uispin" max="200" min="0" maxlength="3"  name="ad[max_photo]" type="text" value="{$cfg.ad.max_photo}"/>
            </td>
        </tr>

        <tr>
            <td><strong>Блокировка накрутки просмотров</strong></td>
            <td width="210">
                <label>
                    <input name="ad[limit_views]" type="radio" value="1" {if $cfg.ad.limit_views == 1}checked="checked"{/if} />
                    <span>{$LANG.YES}</span>
                </label>
                <label>
                    <input name="ad[limit_views]" type="radio" value="0" {if !$cfg.ad.limit_views}checked="checked"{/if} />
                    <span>{$LANG.NO}</span>
                </label>
            </td>
        </tr>

        <tr>
            <td><strong>Url показа</strong></td>
            <td width="210">
                <select name="ad[show_type]">
                    <option value="show" {if $cfg.ad.show_type == 'show'}selected="selected"{/if}>/{$smarty.const.COMPONENT_NAME}/show/{literal}{id}{/literal}</option>
                    <option value="show_html" {if $cfg.ad.show_type == 'show_html'}selected="selected"{/if}>/{$smarty.const.COMPONENT_NAME}/{literal}{id}.html{/literal}</option>
                    <option value="show_full" {if $cfg.ad.show_type == 'show_full'}selected="selected"{/if}>/{$smarty.const.COMPONENT_NAME}/{literal}{vendor}/{model}/{id}.html{/literal}</option>
                </select>
            </td>
        </tr>
    </table>

    <div><strong>Карусель</strong></div>
    <table width="450" border="0" cellpadding="10" cellspacing="0" class="proptable" style="border:none">

        <tr>
            <td><strong>Активна</strong></td>
            <td width="210">
                <label>
                    <input name="ad[carousel][active]" type="radio" value="1" {if $cfg.ad.carousel.active == 1}checked="checked"{/if} />
                    <span>{$LANG.YES}</span>
                </label>
                <label>
                    <input name="ad[carousel][active]" type="radio" value="0" {if !$cfg.ad.carousel.active}checked="checked"{/if} />
                    <span>{$LANG.NO}</span>
                </label>
            </td>
        </tr>

        <tr>
            <td><strong>Минимальное количество объявлений для показа</strong></td>
            <td width="210">
                <input class="size-100 uispin" max="200" min="0" maxlength="3"  name="ad[carousel][min]" type="text" value="{$cfg.ad.carousel.min}"/>
            </td>
        </tr>

        <tr>
            <td><strong>Режим</strong></td>
            <td width="210">
                <select name="ad[carousel][mode]">
                    <option value="1" {if $cfg.ad.carousel.mode == 1}selected="selected"{/if}>Из категории + другие</option>
                    <option value="2" {if $cfg.ad.carousel.mode == 2}selected="selected"{/if}>Только из категории</option>
                </select>
            </td>
        </tr>

    </table>

    <div><strong>Лимиты списков объявлений</strong></div>
    <table width="450" border="0" cellpadding="10" cellspacing="0" class="proptable" style="border:none">

        <tr>
            <td><strong>Страница поиска</strong></td>
            <td width="210">
                <input class="size-100 uispin" max="200" min="0" maxlength="3"  name="ad[list][search][per_page]" type="text" value="{$cfg.ad.list.search.per_page}"/>
            </td>
        </tr>

        <tr>
            <td><strong>Страница производителя</strong></td>
            <td width="210">
                <input class="size-100 uispin" max="200" min="0" maxlength="3"  name="ad[list][sub_vendor][per_page]" type="text" value="{$cfg.ad.list.sub_vendor.per_page}"/>
            </td>
        </tr>

        <tr>
            <td><strong>Личный кабинет</strong></td>
            <td width="210">
                <input class="size-100 uispin" max="200" min="0" maxlength="3"  name="ad[list][cabinet][per_page]" type="text" value="{$cfg.ad.list.cabinet.per_page}"/>
            </td>
        </tr>

    </table>

    <div><strong>Страница редактирования/добавления</strong></div>
    <table width="450" border="0" cellpadding="10" cellspacing="0" class="proptable" style="border:none">

        <tr>
            <td><strong>Валютные лимиты</strong></td>
            <td width="210">

                <div>
                    <span>Мин.</span>
                    <input class="size-70px uispin" min="0" name="ad[currency][min]" type="text" value="{$cfg.ad.currency.min}"/>
                    <span>руб.</span>
                </div>

                <div>
                    <span>Мак.</span>
                    <input class="size-70px uispin" min="0" name="ad[currency][max]" type="text" value="{$cfg.ad.currency.max}"/>
                    <span>руб.</span>
                </div>
            </td>
        </tr>

        <tr>
            <td><strong>Авторегистрация</strong></td>
            <td width="210">
                <label>
                    <input name="ad[add][autoreg]" type="radio" value="1" {if $cfg.ad.add.autoreg == 1}checked="checked"{/if} />
                    <span>{$LANG.YES}</span>
                </label>
                <label>
                    <input name="ad[add][autoreg]" type="radio" value="0" {if !$cfg.ad.add.autoreg}checked="checked"{/if} />
                    <span>{$LANG.NO}</span>
                </label>
            </td>
        </tr>

        <tr>
            <td><strong>Разрешить менять производителя</strong></td>
            <td width="210">
                <label>
                    <input name="ad[add][can_edit_vendor]" type="radio" value="1" {if $cfg.ad.add.can_edit_vendor == 1}checked="checked"{/if} />
                    <span>{$LANG.YES}</span>
                </label>
                <label>
                    <input name="ad[add][can_edit_vendor]" type="radio" value="0" {if !$cfg.ad.add.can_edit_vendor}checked="checked"{/if} />
                    <span>{$LANG.NO}</span>
                </label>
            </td>
        </tr>

        <tr>
            <td><strong>Разрешить менять геолокацию</strong></td>
            <td width="210">
                <label>
                    <input name="ad[add][can_edit_geo]" type="radio" value="1" {if $cfg.ad.add.can_edit_geo == 1}checked="checked"{/if} />
                    <span>{$LANG.YES}</span>
                </label>
                <label>
                    <input name="ad[add][can_edit_geo]" type="radio" value="0" {if !$cfg.ad.add.can_edit_geo}checked="checked"{/if} />
                    <span>{$LANG.NO}</span>
                </label>
            </td>
        </tr>
    </table>

    <div><strong>Платные услуги для новых объявлений</strong></div>
    <table width="450" border="0" cellpadding="10" cellspacing="0" class="proptable" style="border:none">

        <tr>
            <td><strong>Новое объявление по умолчанию VIP</strong></td>
            <td width="210">
                <label>
                    <input name="ad[new][vip][default]" type="radio" value="1" {if $cfg.ad.new.vip.default == 1}checked="checked"{/if} />
                    <span>{$LANG.YES}</span>
                </label>
                <label>
                    <input name="ad[new][vip][default]" type="radio" value="0" {if !$cfg.ad.new.vip.default}checked="checked"{/if} />
                    <span>{$LANG.NO}</span>
                </label>

                <div>
                    <span>На: </span>
                    <input class="size-70px uispin" min="0" name="ad[new][vip][time]" type="text" value="{$cfg.ad.new.vip.time}"/>
                    <span>сек.</span>
                </div>
            </td>
        </tr>

        <tr>
            <td><strong>Новое объявление выделяется цветом</strong></td>
            <td width="210">
                <label>
                    <input name="ad[new][background][default]" type="radio" value="1" {if $cfg.ad.new.background.default == 1}checked="checked"{/if} />
                    <span>{$LANG.YES}</span>
                </label>
                <label>
                    <input name="ad[new][background][default]" type="radio" value="0" {if !$cfg.ad.new.background.default}checked="checked"{/if} />
                    <span>{$LANG.NO}</span>
                </label>

                <div>
                    <span>На: </span>
                    <input class="size-70px uispin" min="0" name="ad[new][background][time]" type="text" value="{$cfg.ad.new.background.time}"/>
                    <span>сек.</span>
                </div>
                <div>
                    <span>цветом: </span>
                    <input class="size-70px colorpicker" name="ad[new][background][color]" type="text" value="{$cfg.ad.new.background.color}"/>
                </div>
            </td>
        </tr>

    </table>

</div>

<div id="images">
    <div>
        <strong>Основные</strong>
    </div>
    <table width="450" border="0" cellpadding="10" cellspacing="0" class="proptable" style="border:none">

        <tr>
            <td><strong>{$LANG.ADMIN_TAB_IMG_QUALITY}</strong></td>
            <td width="210">
                <input class="size-100 uispin" max="100" min="0" maxlength="3"  name="images[configs][quality]" type="text" value="{$cfg.images.configs.quality}"/>
            </td>
        </tr>

        <tr>
            <td><strong>{$LANG.ADMIN_TAB_IMG_SAVE_ORIGINAL}</strong></td>
            <td width="210">
                <label>
                    <input name="images[configs][save_original]" type="radio" value="1" {if $cfg.images.configs.save_original == 1}checked="checked"{/if} />
                    <span>{$LANG.YES}</span>
                </label>
                <label>
                    <input name="images[configs][save_original]" type="radio" value="0" {if !$cfg.images.configs.save_original}checked="checked"{/if} />
                    <span>{$LANG.NO}</span>
                </label>
            </td>
        </tr>

    </table>

    <div>
        <strong>Размеры</strong>
    </div>
    {literal}
        <script>
            $(function(){
                $('#add_img_t').on('click', function(){
                    var numC = $('#image_manager').attr('data-count');

                    $.tmpl($('#template-img_type').html(), {
                        num: numC
                    }).appendTo($('#image_manager'));

                    $('#image_manager').attr('data-count', ++numC);
                });

                $('#image_manager').on('click', '.remove_photo_t', function(){
                    var $cont = $(this).parents('.photo-format');
                    $cont.fadeOut(800, function(){
                        $cont.remove();
                    });
                });
            });
        </script>
    {/literal}
    <table id="image_manager" width="450" border="0" cellpadding="10" cellspacing="0" class="proptable" style="border:none" data-count="{$cfg.images.ad|@count}">

        {foreach from=$cfg.images.ad key=num item=img}
            <tr class="photo-format" data-num="{$num}">
                <td>
                    {if $img.sys}
                        <span>{$img.name}</span>
                        <input name="images[ad][{$num}][name]" type="hidden" value="{$img.name}" />
                    {else}
                        <input name="images[ad][{$num}][name]" type="text" value="{$img.name}" />
                        <span class="remove_photo_t js-btn">удалить</span>
                    {/if}
                </td>

                <td width="210">

                    <div>
                        <input class="size-3000 uispin" max="3000" min="0" maxlength="4"  name="images[ad][{$num}][weight]" type="text" value="{$img.weight}"/>
                        <span>x</span>
                        <input class="size-3000 uispin" max="3000" min="0" maxlength="4"  name="images[ad][{$num}][height]" type="text" value="{$img.height}"/>
                    </div>

                    <div>
                        <span>Квадратное: </span>
                        <label>
                            <input name="images[ad][{$num}][thumb]" type="radio" value="1" {if $img.thumb == 1}checked="checked"{/if} />
                            <span>{$LANG.YES}</span>
                        </label>
                        <label>
                            <input name="images[ad][{$num}][thumb]" type="radio" value="0" {if !$img.thumb}checked="checked"{/if} />
                            <span>{$LANG.NO}</span>
                        </label>
                    </div>

                    <div>
                        <span>Ватермарк: </span>
                        <label>
                            <input name="images[ad][{$num}][watermark]" type="radio" value="1" {if $img.watermark == 1}checked="checked"{/if} />
                            <span>{$LANG.YES}</span>
                        </label>
                        <label>
                            <input name="images[ad][{$num}][watermark]" type="radio" value="0" {if !$img.watermark}checked="checked"{/if} />
                            <span>{$LANG.NO}</span>
                        </label>
                    </div>

                    <div>
                        <span>Системное: </span>
                        <label>
                            <input name="images[ad][{$num}][sys]" type="radio" value="1" {if $img.sys == 1}checked="checked"{/if} />
                            <span>{$LANG.YES}</span>
                        </label>
                        <label>
                            <input name="images[ad][{$num}][sys]" type="radio" value="0" {if !$img.sys}checked="checked"{/if} />
                            <span>{$LANG.NO}</span>
                        </label>
                    </div>
                    <hr>
                </td>
            </tr>
        {/foreach}

        {literal}
        <script id="template-img_type"  type="text/t-tmpl">
                        <tr class="photo-format" data-num="${num}">
                            <td>
                                <input name="images[ad][${num}][name]" type="text" />
                                <span class="remove_photo_t js-btn">удалить</span>
                            </td>

                            <td width="210">

                                <div>
                                    <input class="size-3000 uispin" max="3000" min="0" maxlength="4"  name="images[ad][${num}][weight]" type="text"/>
                                    <span>x</span>
                                    <input class="size-3000 uispin" max="3000" min="0" maxlength="4"  name="images[ad][${num}][height]" type="text"/>
                                </div>

                                <div>
                                    <span>Квадратное: </span>
                                    <label>
                                        <input name="images[ad][${num}][thumb]" type="radio" value="1" />
                                        <span>{/literal}{$LANG.YES}{literal}</span>
                                    </label>
                                    <label>
                                        <input name="images[ad][${num}][thumb]" type="radio" value="0" />
                                        <span>{/literal}{$LANG.NO}{literal}</span>
                                    </label>
                                </div>

                                <div>
                                    <span>Ватермарк: </span>
                                    <label>
                                        <input name="images[ad][${num}][watermark]" type="radio" value="1" />
                                        <span>{/literal}{$LANG.YES}{literal}</span>
                                    </label>
                                    <label>
                                        <input name="images[ad][${num}][watermark]" type="radio" value="0" />
                                        <span>{/literal}{$LANG.NO}{literal}</span>
                                    </label>
                                </div>

                                <div>
                                    <span>Системное: </span>
                                    <label>
                                        <input name="images[ad][${num}][sys]" type="radio" value="1" />
                                        <span>{/literal}{$LANG.YES}{literal}</span>
                                    </label>
                                    <label>
                                        <input name="images[ad][${num}][sys]" type="radio" value="0" />
                                        <span>{/literal}{$LANG.NO}{literal}</span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                    </script>
        {/literal}
    </table>

    <div>
        <span id="add_img_t" class="js-btn">Добавить формат</span>
    </div>
</div>


<div id="geo">
    {literal}
        <script>
            $(function(){
                $('select[name="geo[country]"]').on('change', function(){
                    auto.models.geo.getRegions(this.value, function(html){
                        $('select[name="geo[region][default]"]').html(html).removeAttr('disabled');
                        $('select[name="geo[region][preset]"]').html(html).removeAttr('disabled');
                    });

                    if(this.value > 0){
                        $('select[name="geo[region][default]"]').attr('disabled', true);
                        $('select[name="geo[region][preset]"]').attr('disabled', true);
                    }
                });


                $('select[name="geo[region][default]"]').on('change', function(){
                    auto.models.geo.getCities(this.value, function(html){
                        $('select[name="geo[city][default]"]').html(html).removeAttr('disabled');
                        $('select[name="geo[city][preset]"]').html(html).removeAttr('disabled');
                    });

                    if(this.value > 0){
                        $('select[name="geo[region][preset]"]').attr('disabled', true);
                    } else{
                        $('select[name="geo[region][preset]"]').removeAttr('disabled');
                        $('select[name="geo[city][default]"]').removeAttr('disabled');
                        $('select[name="geo[city][preset]"]').removeAttr('disabled');
                    }
                });

                $('select[name="geo[city][default]"]').on('change', function(){
                    if(this.value > 0){
                        $('select[name="geo[city][preset]"]').attr('disabled', true);
                    } else{
                        $('select[name="geo[city][preset]"]').removeAttr('disabled');
                    }

                });

                $('select[name="geo[region][preset]"]').on('change', function(){
                    auto.models.geo.getCities(this.value, function(html){
                        $('select[name="geo[city][preset]"]').html(html).removeAttr('disabled');
                    });

                    if(this.value > 0){
                        $('select[name="geo[city][default]"]').attr('disabled', true);
                    } else{
                        $('select[name="geo[city][preset]"]').removeAttr('disabled');
                    }
                });
            });
        </script>
    {/literal}
    <div><h2><span style="color: red;">Внимание!</span> данные на основе компонента <a href="index.php?view=components&do=config&link=geo">геолокации</a></h2></div>
    <div><strong>Настройки страны</strong></div>
    <table width="450" border="0" cellpadding="10" cellspacing="0" class="proptable" style="border:none">

        <tr>
            <td><strong>Страна компонента</strong></td>
            <td>
                <select name="geo[country]">
                    {foreach from=$preset.countries key=value item=country}
                        <option value="{$value}" {if $cfg.geo.country == $value}selected="selected"{/if}>{$country}</option>
                    {/foreach}
                </select>
            </td>
        </tr>


    </table>

    <div><strong>Настройки региона</strong></div>
    <table width="450" border="0" cellpadding="10" cellspacing="0" class="proptable" style="border:none">

        <tr>
            <td><strong>{$LANG.ADMIN_TAB_SET}</strong></td>
            <td>
                <select name="geo[region][default]">
                    {foreach from=$preset.regions key=value item=region}
                        <option value="{$value}" {if $cfg.geo.region.default == $value}selected="selected"{/if}>{$region}</option>
                    {/foreach}
                </select>
            </td>
        </tr>

        <tr>
            <td><strong>{$LANG.ADMIN_TAB_PRESET}</strong></td>
            <td width="210">
                <select name="geo[region][preset]" {if $cfg.geo.region.default > 0}disabled="disabled"{/if}>
                    {foreach from=$preset.regions key=value item=region}
                        <option value="{$value}" {if $cfg.geo.region.preset == $value}selected="selected"{/if}>{$region}</option>
                    {/foreach}
                </select>
            </td>
        </tr>

    </table>

    <div><strong>Настройки города</strong></div>
    <table width="450" border="0" cellpadding="10" cellspacing="0" class="proptable" style="border:none">

        <tr>
            <td><strong>{$LANG.ADMIN_TAB_SET}</strong></td>
            <td>
                <select name="geo[city][default]" {if !$cfg.geo.region.default and $cfg.geo.region.preset}disabled="disabled"{/if}>
                    {foreach from=$preset.cities key=value item=city}
                        <option value="{$value}" {if $cfg.geo.city.default == $value}selected="selected"{/if}>{$city}</option>
                    {/foreach}
                </select>
            </td>
        </tr>

        <tr>
            <td><strong>{$LANG.ADMIN_TAB_PRESET}</strong></td>
            <td width="210">
                <select name="geo[city][preset]" {if $cfg.geo.city.default}disabled="disabled"{/if}>
                    {foreach from=$preset.cities key=value item=city}
                        <option value="{$value}" {if $cfg.geo.city.preset == $value}selected="selected"{/if}>{$city}</option>
                    {/foreach}
                </select>
            </td>
        </tr>

    </table>
</div>

<div id="services">
    <table width="600" border="0" cellpadding="10" cellspacing="0" class="proptable" style="border:none">
        {if $smarty.const.HAVE_COMPONENT_BILLING}
            <h2>Billing: <span style="color: green;">ok</span></h2>
        {else}
            <h2>Billing: <span style="color: red">не установлен</span></h2>
            <div>
                <span>Для того чтобы сервис платных услуг был активен установите компонент <a href="http://www.instantcms.ru/billing/about.html">Биллинг пользователей</a></span>
                <a></a>
            </div>
        {/if}


        {if $smarty.const.HAVE_COMPONENT_BILLING}
            <tr>
                <td><strong>Премиум объявление</strong></td>
                <td>
                    <div>
                        <span>Стоимость: </span>
                        <input class="size-70px uispin" min="0" name="services[vip][cost]" type="text" value="{$cfg.services.vip.cost}"/>
                    </div>

                    <div>
                        <span>Длительность услуги: </span>
                        <input class="size-70px uispin" min="0" name="services[vip][duration]" type="text" value="{$cfg.services.vip.duration}"/>
                        <span>сек.</span>
                    </div>
                </td>
            </tr>

            <tr>
                <td><strong>Прикрепить объявление</strong></td>
                <td>
                    <div>
                        <span>Стоимость: </span>
                        <input class="size-70px uispin" min="0" name="services[attach][cost]" type="text" value="{$cfg.services.attach.cost}"/>
                    </div>

                    <div>
                        <span>Длительность услуги: </span>
                        <input class="size-70px uispin" min="0" name="services[attach][duration]" type="text" value="{$cfg.services.attach.duration}"/>
                        <span>сек.</span>
                    </div>
                </td>
            </tr>

            <tr>
                <td><strong>Поднять объявление</strong></td>
                <td>
                    <div>
                        <span>Стоимость: </span>
                        <input class="size-70px uispin" min="0" name="services[up][cost]" type="text" value="{$cfg.services.up.cost}"/>
                    </div>
                </td>
            </tr>

            <tr>
                <td><strong>Поднять и выделить объявление цветом</strong></td>
                <td>
                    <div>
                        <span>Стоимость: </span>
                        <input class="size-70px uispin" min="0" name="services[up_bg][cost]" type="text" value="{$cfg.services.up_bg.cost}"/>
                    </div>

                    <div>
                        <span>На: </span>
                        <input class="size-70px uispin" min="0" name="services[up_bg][duration]" type="text" value="{$cfg.services.up_bg.duration}"/>
                        <span>сек.</span>
                    </div>
                    <div>
                        <span>цветом: </span>
                        <input class="colorpicker" name="services[up_bg][color]" type="text" value="{$cfg.services.up_bg.color}"/>
                    </div>
                </td>
            </tr>
        {/if}

    </table>

</div>

</div>
<p>
    <input name="opt" type="hidden" value="saveconfig" />
    <input name="save" type="submit" id="save" value="{$LANG.SAVE}" />
    <input name="back" type="button" id="back" value="{$LANG.CANCEL}" onclick="window.location.href='index.php?view=components';"/>
</p>
</form>