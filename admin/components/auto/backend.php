<?php

if(!defined('VALID_CMS_ADMIN')) {
    die('ACCESS DENIED');
}

// region init
$inCore = cmsCore::getInstance();
$inPage = cmsPage::getInstance();
$inUser = cmsUser::getInstance();
$inConf = cmsConfig::getInstance();

define('COMPONENT_NAME', 'auto');
define('COMPONENT_HOME_URL', '/' . COMPONENT_NAME);
define('COMPONENT_TEMPLATE_PATH', 'components/auto/admin');
define('COMPONENT_TEMPLATE_CONTAINER', '_auto_admin_container.tpl');
$componentName = COMPONENT_NAME;
cmsCore::loadModel(COMPONENT_NAME);
$inCore->loadLanguage('components/' . COMPONENT_NAME);
global $_LANG;

// region other components defines
define('HAVE_COMPONENT_BILLING', $inCore->isComponentInstalled('billing'));
define('HAVE_COMPONENT_SUPPORT', $inCore->isComponentInstalled('support'));

if (HAVE_COMPONENT_BILLING) {
    $inCore->loadClass('billing');

    $billingData = array(

    );
}

if (HAVE_COMPONENT_SUPPORT) {
    $inCore->loadClass('support');
}

if($inCore->isComponentInstalled('geo')){
    $inCore->loadModel('geo');
    $geo = new cms_model_geo();

} else {
    cmsCore::halt('geo component is missing!');
}

// endregion
$componentId = (int)$_REQUEST['id'];

// region model
$cfg = $inCore->loadComponentConfig(COMPONENT_NAME);


$component_data_array = include_once PATH . '/components/' . COMPONENT_NAME . '/includes/component_data.php';

$routes = $component_data_array['routes'];
$component_data = $component_data_array['component_data'];
$component_data['status']['0'] = 'Продана';

$component_data['moderation']['0'] = 'Не опубликован';
$component_data['moderation']['1'] = 'Пре-модерация';
$component_data['moderation']['2'] = 'Пост-модерация';
$component_data['moderation']['3'] = 'Опубликован';



$model = new cms_model_auto($component_data, $routes, $_LANG);
// endregion model

$opt = cmsCore::request('opt', 'str', 'index');
$sub_opt = cmsCore::request('sub_opt');

$component_url = "?view=components&do=config&link={$componentName}";

// endregion init

// region toolmenu
$toolmenu[] = array(
    'icon'  => 'new.gif',
    'title' => 'Добавить объявление',
    'link'  => "{$component_url}&opt=add&sub_opt=ad"
);

$toolmenu[] = array(
    'icon'  => 'list.gif',
    'title' => 'Список объявлений',
    'link'  => "{$component_url}&opt=list&sub_opt=ad"
);

$toolmenu[] = array(
    'icon'  => 'auto/vendor_add.png',
    'title' => 'Добавить производителя',
    'link'  => "{$component_url}&opt=add&sub_opt=vendor"
);

$toolmenu[] = array(
    'icon'  => 'list.gif',
    'title' => 'Список производителей',
    'link'  => "{$component_url}&opt=list&sub_opt=vendor"
);

$toolmenu[] = array(
    'icon'  => 'auto/model_add.png',
    'title' => 'Добавить модель',
    'link'  => "{$component_url}&opt=add&sub_opt=model"
);

$toolmenu[] = array(
    'icon'  => 'list.gif',
    'title' => 'Список моделей',
    'link'  => "{$component_url}&opt=list&sub_opt=model"
);

$toolmenu[] = array(
    'icon'  => 'auto/parsers.png',
    'title' => 'Парсеры',
    'link'  => "{$component_url}&opt=parsers"
);

$toolmenu[] = array(
    'icon'  => 'config.gif',
    'title' => 'Настройки',
    'link'  => "{$component_url}&opt=config"
);

cpToolMenu($toolmenu);
// endregion toolmenu

// region check folder access
cpCheckWritable($cfg['system']['upload_dir'], 'folder');
cpCheckWritable($cfg['system']['upload_dir'] . 'vendors', 'folder');
cpCheckWritable($cfg['system']['upload_dir'] . 'ads', 'folder');
// endregion check folder access

// region for currency
if(!count($cfg['currency'])){
    cms_model_auto::parseCurrency();
}

$show_currency = array(
    'list' => array(
        'rub' => array(
            'rub' => $cfg['currency']['1:1'],
            'usd' => $cfg['currency']['1:2'],
            'eur' => $cfg['currency']['1:3'],
        ),
        'usd' => array(
            'rub' => $cfg['currency']['2:1'],
            'usd' => $cfg['currency']['2:2'],
            'eur' => $cfg['currency']['2:3'],
        ),
        'eur' => array(
            'rub' => $cfg['currency']['3:1'],
            'usd' => $cfg['currency']['3:2'],
            'eur' => $cfg['currency']['3:3'],
        )
    ),
    'upd'   => $cfg['currency']['upd']
);
// endregion for currency

$vars = array();

switch($opt){
    case 'config': {

        cpAddPathway($_LANG['ADMIN_SETTINGS'], null);

        // список стран
        $vars['preset']['countries'] = $geo->getCountries();
        if(!$cfg['geo']['country']){
            $cfg['geo']['country'] = 3159;
        }
        // список регионов для страны

        if($cfg['geo']['country']){
            $vars['preset']['regions'] = $geo->getRegions($cfg['geo']['country']);

            if($cfg['geo']['region']['default']){
                $vars['preset']['cities'] = $geo->getCities($cfg['geo']['region']['default']);
            } elseif($cfg['geo']['region']['preset']){
                $vars['preset']['cities'] = $geo->getCities($cfg['geo']['region']['preset']);
            }
        }


        $vars['show_currency'] = $show_currency;
    } break;

    case 'add_form': {
        switch($sub_opt){
            case 'vendor': {
                $model->adminAddVendor(cmsCore::request('title'), cmsCore::request('cat'), cmsCore::request('popular'));

                cmsCore::addSessionMessage($_LANG['AD_DO_SUCCESS'], 'success');
                cmsCore::redirect($component_url . '&opt=list&sub_opt=vendor');
            } break;
            case 'model': {
                $model->adminAddModel(cmsCore::request('title'), cmsCore::request('vendor_id', 'int'), cmsCore::request('carcase_id', 'int'), cmsCore::request('popular'));

                cmsCore::addSessionMessage($_LANG['AD_DO_SUCCESS'], 'success');
                cmsCore::redirect($component_url . '&opt=list&sub_opt=model');
            } break;
        }
    } break;

    case 'edit_form': {
        switch($sub_opt){
            case 'vendor': {
                $vendor = $model->getVendor(cmsCore::request('uid', 'int'));

                if(!$vendor){
                    cmsCore::error404();
                }

                $model->adminEditVendor($vendor['id'], array(
                    'title' => cmsCore::request('title'),
                    'cat'   => cmsCore::request('cat'),
                    'popular'   => cmsCore::request('popular'),
                ));

                cmsCore::addSessionMessage($_LANG['AD_DO_SUCCESS'], 'success');
                cmsCore::redirect($component_url . '&opt=edit&sub_opt=vendor&uid=' . $vendor['id']);
            } break;
            case 'model': {
                $d_model = $model->getModel(cmsCore::request('uid', 'int'));

                if(!$d_model){
                    cmsCore::error404();
                }

                $model->adminEditModel($d_model['id'], array(
                    'title' => cmsCore::request('title'),
                    'vendor_id' => cmsCore::request('vendor_id'),
                    'carcase_id' => cmsCore::request('carcase_id'),
                    'popular'   => cmsCore::request('popular'),
                ));

                cmsCore::addSessionMessage($_LANG['AD_DO_SUCCESS'], 'success');
                cmsCore::redirect($component_url . '&opt=edit&sub_opt=model&uid=' . $d_model['id']);
            } break;
        }
    } break;

    case 'parse_currency': {
        cms_model_auto::parseCurrency();

        cmsCore::addSessionMessage('Курсы валют обновлены', 'success');
        cmsCore::redirect('?view=components&do=config&id='.$id.'&opt=config');
    } break;

    case 'list': {
        function cpFormatPopular($item)
        {
            return $item ? 'Да' : 'Нет';
        }

        function cpFormatLogo($item)
        {
            $inCore = cmsCore::getInstance();
            $cfg = $inCore->loadComponentConfig(COMPONENT_NAME);

            return $item ? '<img src="' . $cfg['system']['upload_dir'] . "vendors/small/" . $item . '">' : '<img src="' . $cfg['system']['upload_dir'] . 'vendors/small/nologo.png">';
        }

        function cpFormatVendorCat($item)
        {
            $inDB = cmsDatabase::getInstance();

            $result = $inDB->query("SELECT title, id FROM cms_auto_cats WHERE id = {$item}") ;

            if ($inDB->num_rows($result)) {
                $cat = $inDB->fetch_assoc($result);

                return $cat['title'];
            } else {
                return '--';
            }
        }

        function cpFormatModelCarcase($item)
        {
            $inDB = cmsDatabase::getInstance();

            $result = $inDB->query("SELECT title, id FROM cms_auto_cats WHERE id = {$item}") ;

            if ($inDB->num_rows($result)) {
                $cat = $inDB->fetch_assoc($result);
                return $cat['title'];
            } else {
                return '--';
            }
        }

        function cpAutoGetList($listtype, $field_name = 'title', $where = '')
        {
            $list = array();

            $inDB = cmsDatabase::getInstance();

            $where = ($where) ? 'WHERE ' . $where : '';

            $sql  = "SELECT id, {$field_name} FROM {$listtype} {$where} ORDER BY {$field_name} ASC";
            $result = $inDB->query($sql) ;

            if ($inDB->num_rows($result)>0) {
                while($item = $inDB->fetch_assoc($result)){
                    $next = sizeof($list);
                    $list[$next]['title'] = $item[$field_name];
                    $list[$next]['id'] = $item['id'];
                }
            }

            return $list;
        }

        function cpFormatVendorTitle($item)
        {
            $inDB = cmsDatabase::getInstance();

            $result = $inDB->query("SELECT title, id FROM cms_auto_vendors WHERE id = {$item}") ;

            if ($inDB->num_rows($result)) {
                $cat = $inDB->fetch_assoc($result);
                return $cat['title'];
            } else {
                return '--';
            }
        }

        switch(cmsCore::request('sub_opt')){
            case 'vendor': {
                cpAddPathway('Список производителей', null);

                $fields = array(
                    array(
                        'title' => 'id',
                        'field' => 'id',
                        'width' => '30',
                        'link'  => "{$component_url}&opt=edit&sub_opt=vendor&uid=%id%"
                    ),
                    array(
                        'title' => '',
                        'field' => 'logo',
                        'width' => '40',
                        'prc'  => 'formatLogo'
                    ),
                    array(
                        'title' => 'Категория',
                        'field' => 'cats_id',
                        'prc'  => 'cpFormatVendorCat',
                        'filter'  => 1,
                        'filterlist'  => cpAutoGetList('cms_auto_cats', 'title', 'lvl = 0'),
                        'width' => '',
                    ),
                    array(
                        'title' => 'Название',
                        'field' => 'title',
                        'filter'  => 15,
                        'width' => '',
                    ),
                    array(
                        'title' => 'Популярный',
                        'field' => 'popular',
                        'width' => '',
                        'prc'  => 'cpFormatPopular',
                        'filter'  => 1,
                        'filterlist'  => array(
                            array('id' => 0, 'title' => 'Нет'),
                            array('id' => 1, 'title' => 'Да'),
                        ),
                    ),
                    array(
                        'title' => 'Кол-во объявлений',
                        'field' => 'count_advt',
                        'width' => '',
                    )
                );

                $actions = array(
                    array(
                        'title' => $_LANG['EDIT'],
                        'icon'  => 'edit.gif',
                        'link'  => $component_url . "&opt=edit&sub_opt=vendor&uid=%id%"
                    ),
                    array(
                        'title'   => $_LANG['DELETE'],
                        'icon'    => 'delete.gif',
                        'confirm' => 'Удалить объявление?',
                        'link'    => $component_url . "&opt=delete&sub_opt=vendor&uid=%id%"
                    )
                );

                ob_start();
                cpListTable('cms_' . cms_model_auto::$_table['vendor'], $fields, $actions, '', 'id DESC', 50);

            } break;
            case 'model': {
                cpAddPathway('Список моделей', null);

                $fields = array(
                    array(
                        'title' => 'id',
                        'field' => 'id',
                        'width' => '30',
                        'link'  => "{$component_url}&opt=edit&sub_opt=model&uid=%id%"
                    ),
                    array(
                        'title' => 'Название',
                        'field' => 'title',
                        'filter'  => 15,
                        'width' => ''
                    ),
                    array(
                        'title' => 'Производитель',
                        'field' => 'vendor_id',
                        'prc'  => 'cpFormatVendorTitle',
                        'filter'  => 15,
                        'filterlist'  => cpGetList('cms_auto_vendors'),
                        'width' => '',
                    ),
                    array(
                        'title' => 'Тип кузова',
                        'field' => 'case_id',
                        'prc'  => 'cpFormatModelCarcase',
                        'filter'  => 1,
                        'filterlist'  => cpAutoGetList('cms_auto_cats', 'title', 'lvl = 1'),
                        'width' => '',
                    ),
                    array(
                        'title' => 'Популярная модель',
                        'field' => 'popular',
                        'width' => '',
                        'prc'  => 'cpFormatPopular',
                        'filter'  => 1,
                        'filterlist'  => array(
                            array('id' => 0, 'title' => 'Нет'),
                            array('id' => 1, 'title' => 'Да'),
                        ),

                    ),
                    array(
                        'title' => 'Кол-во объявлений',
                        'field' => 'count_advt',
                        'width' => '',
                    )
                );

                $actions = array(
                    array(
                        'title' => $_LANG['EDIT'],
                        'icon'  => 'edit.gif',
                        'link'  => $component_url . "&opt=edit&sub_opt=model&uid=%id%"
                    ),
                    array(
                        'title'   => $_LANG['DELETE'],
                        'icon'    => 'delete.gif',
                        'confirm' => 'Удалить модель?',
                        'link'    => $component_url . "&opt=delete&sub_opt=model&uid=%id%"
                    )
                );

                ob_start();
                cpListTable('cms_' . cms_model_auto::$_table['model'], $fields, $actions, '', 'id DESC', 50);

            } break;
            case 'ad': {
                cpAddPathway($_LANG['ADMIN_AD_LIST'], null);

                function adTitleFunc($item)
                {
                    return "{$item['title_vendor']} {$item['title_model']}";
                }

                function adShowFunc($item)
                {
                    switch($item['show']){
                        case 0: return 'Не опубликован'; break;
                        case 1: return 'На премодерации'; break;
                        case 2: return 'На постмодерации'; break;
                        case 3: return 'Опубликован'; break;
                    }
                }

                function adAutoStatusFunc($item)
                {
                    switch($item['show']){
                        case 0: return 'Продана'; break;
                        case 1: return 'В наличии'; break;
                        case 2: return 'В пути'; break;
                        case 3: return 'Под заказ'; break;
                    }
                }

                $fields = array(
                    array(
                        'title' => 'id',
                        'field' => 'id',
                        'width' => '30',
                        'link'  => "{$component_url}&opt=edit&sub_opt=ad&uid=%id%"
                    ),
                    array(
                        'title'  => $_LANG['DATE'],
                        'field'  => 'created_at',
                        'width'  => '100',
                        'filter' => 15,
                        'fdate'  => '%d/%m/%Y'
                    ),
                    array(
                        'title' => $_LANG['TITLE'],
                        'field' => array('title_vendor', 'title_model'),
                        'width' => '',
                        'prc'  => 'adTitleFunc'
                    ),
                    array(
                        'title' => 'Модерация',
                        'field' => 'show',
                        'width' => '',
                        'filter' => 1,
                        'filterlist' => array(
                            array('id' => 0, 'title' => 'Не опубликован'),
                            array('id' => 1, 'title' => 'На премодерации'),
                            array('id' => 2, 'title' => 'На постмодерации'),
                            array('id' => 3, 'title' => 'Опубликован')
                        ),
                        'prc'  => 'adShowFunc'
                    ),
                    array(
                        'title' => 'Статус',
                        'field' => 'auto_status',
                        'width' => '',
                        'filter' => 1,
                        'filterlist' => array(
                            array('id' => 0, 'title' => 'Продана'),
                            array('id' => 1, 'title' => 'В наличии'),
                            array('id' => 2, 'title' => 'В пути'),
                            array('id' => 3, 'title' => 'Под заказ')
                        ),
                        'prc'  => 'adAutoStatusFunc'
                    ),
                    array(
                        'title' => 'Просмотров',
                        'field' => 'count_views',
                        'width' => '',
                        'filter' => 15,
                    )
                );

                $actions = array(
                    array(
                        'title' => $_LANG['EDIT'],
                        'icon'  => 'edit.gif',
                        'link'  => "{$component_url}&opt=edit&sub_opt=ad&uid=%id%"
                    ),
                    array(
                        'title'   => $_LANG['DELETE'],
                        'icon'    => 'delete.gif',
                        'confirm' => 'Удалить объявление?',
                        'link'    => "{$component_url}&opt=delete&sub_opt=ad&uid=%id%"
                    )
                );

                ob_start();
                cpListTable('cms_' . cms_model_auto::$_table['unit'], $fields, $actions, '', 'created_at DESC', 50);

            } break;
        }

        $vars['data'] = ob_get_clean();
    } break;

    case 'add': {
        switch($sub_opt){
            case 'vendor': {
                cpAddPathway('Добавить производителя', null);

                $vars['vendor_cats'] = $model->adminGetVendorCat();
            } break;
            case 'model': {
                cpAddPathway('Добавить модель', null);

                $vars['vendors'] = $model->getVendorsList(false, false);
                $vars['carcase_cats'] = $model->getCarcaseList();
            } break;
            case 'ad': {
                cpAddPathway($_LANG['ADMIN_AD_ADD'], null);

                $vars = array(
                    'data' => array(
                        'vendors' => $model->getVendorsList(false),
                        'date'    => $model->getDateList()
                    ),
                    'user_id' => $inUser->id,
                );


                $preset = array(
                    'access_issues' => true
                );


                $check = $model->checkAddForm();

                if ($check['status'] == 0){

                    $preset = array(
                        'access_issues' => true
                    );


                    if($cfg['geo']['region']['default']){
                        $preset['geo']['region'] = $model->geo->getRegion($cfg['geo']['region']['default']);

                        if(!$cfg['geo']['city']['default']){
                            $preset['geo']['cities'] = $model->geo->getCities($cfg['geo']['region']['default']);
                            $preset['geo']['city']['id'] = ($cfg['geo']['city']['preset']) ? $cfg['geo']['city']['preset'] : null;
                        } else {
                            $preset['geo']['city'] = $model->geo->getCity($cfg['geo']['city']['default']);
                        }
                    } else {
                        $preset['geo']['regions'] = $model->geo->getRegions($cfg['geo']['country']);

                        if($cfg['geo']['region']['preset']){
                            $preset['geo']['region']['id'] = $cfg['geo']['region']['preset'];

                            $preset['geo']['cities'] = $model->geo->getCities($cfg['geo']['region']['preset']);
                            if($cfg['geo']['city']['preset']){
                                $preset['geo']['city']['id'] = $cfg['geo']['city']['preset'];
                            }
                        }
                    }


                    if(count($preset)){
                        $vars['set'] = $preset;
                    }

                } elseif($check['status'] == 2) {

                    $id = $model->insertAd($check['setters']);

                    if($id){
                        $inCore->redirect(sprintf($routes['cabinet']['photomanager'], $id));
                    } else {
                        // TODO: fix it, во время добавления что то пошло не так
                        echo 'error';
                    }
                } else {
                    $vars['check'] = $check;
                    $vars['set'] = $check['setters'];
                    $vars['errors'] = $check['errors'];
                }

            } break;
        }
    } break;

    case 'edit': {
        switch($sub_opt){
            case 'vendor': {
                cpAddPathway('Редактирование производителя', null);

                $vendor = $model->getVendor(cmsCore::request('uid', 'int'));
                if(!$vendor){
                    cmsCore::error404();
                }

                $vars['vendor'] = $vendor;
                $vars['vendor_cats'] = $model->adminGetVendorCat();
            } break;
            case 'model': {
                cpAddPathway('Редактирование модели', null);

                $d_model = $model->getModel(cmsCore::request('uid', 'int'));
                if(!$d_model){
                    cmsCore::error404();
                }

                $vars['model'] = $d_model;
                $vars['vendors'] = $model->getVendorsList(false, false);
                $vars['carcase_cats'] = $model->getCarcaseList();
            } break;
            case 'ad': {
                cpAddPathway($_LANG['ADMIN_AD_EDIT'], null);

                $ad = $model->getAd(cmsCore::request('uid', 'int'));

                if(!$ad){
                    cmsCore::error404();
                }

                $vars = array(
                    'data' => array(
                        'vendors' => $model->getVendorsList(false, true),
                        'date'    => $model->getDateList()
                    ),
                    'geo' => array(
                        'region'  => $cfg['geo']['region']['default'] ? $model->geo->getRegion($cfg['geo']['region']['default']) : null,
                        'city'    => $cfg['geo']['region']['default'] ? ($cfg['geo']['city']['default'] ? $model->geo->getCity($cfg['geo']['city']['default']) : null) : null
                    ),
                    'user_id' => $inUser->id,
                );

                $check = $model->checkAddForm(array(
                    'vendor' => $ad['data']['id_vendor'],
                    'model' => $ad['data']['id_model'],
                    'geo' => array(
                        'region' => $ad['data']['region_id'],
                        'city'   => $ad['data']['city_id']
                    )
                ));

                if ($check['status'] == 0){
                    $preset = $model->formatPresetAdtoEdit($ad);

                    $vars['set'] = $preset;
                    $vars['is_edit'] = true;
                } elseif($check['status'] == 2) {
                    $model->updateAd($ad['id'], $check['setters'], true);

                    cmsCore::addSessionMessage($_LANG['AD_DO_SUCCESS'] . ' <a target="_blank" href="/' . $componentName . "/{$ad['id']}.html" . '/">нажмите чтобы перейти к объявлению</a>', 'success');
                    cmsCore::redirect($component_url . '&opt=list&sub_opt=ad');
                } else {
                    $vars['check'] = $check;
                    $vars['errors'] = $check['errors'];

                    $preset = $model->formatPresetAdtoEdit($ad);

                    $preset = $model->formatPresetAdtoEdit($ad);
                    $vars['set'] = $preset;
                    $vars['is_edit'] = true;
                }


                $vars['ad'] = $ad;
            } break;
        }
    } break;

    case 'delete': {
        switch($sub_opt){
            case 'vendor': {
                $vendor = $model->getVendor(cmsCore::request('uid', 'int'));

                if(!$vendor){
                    cmsCore::error404();
                }

                if($vendor['count_advt']){
                    cmsCore::addSessionMessage('Производителя невозможно удалить. для него есть объявления', 'error');
                    cmsCore::redirect($component_url . '&opt=list&sub_opt=vendor');
                }

                $model->adminRemoveVendor($vendor['id']);

                cmsCore::addSessionMessage($_LANG['AD_DO_SUCCESS'], 'success');
                cmsCore::redirect($component_url . '&opt=list&sub_opt=vendor');
            } break;
            case 'logo': {
                $vendor = $model->getVendor(cmsCore::request('uid', 'int'));
                if(!$vendor){
                    cmsCore::error404();
                }

                $model->adminRemoveVendorLogo($vendor['id']);

                cmsCore::addSessionMessage($_LANG['AD_DO_SUCCESS'], 'success');
                cmsCore::redirect($component_url . '&opt=edit&sub_opt=vendor&uid=' . $vendor['id']);
            } break;
            case 'model': {
                $d_model = $model->getModel(cmsCore::request('uid', 'int'));

                if(!$d_model){
                    cmsCore::error404();
                }

                if($d_model['count_advt']){
                    cmsCore::addSessionMessage('Модель невозможно удалить. для неё есть объявления', 'error');
                    cmsCore::redirect($component_url . '&opt=list&sub_opt=model');
                }

                $model->adminRemoveModel($d_model['id']);

                cmsCore::addSessionMessage($_LANG['AD_DO_SUCCESS'], 'success');
                cmsCore::redirect($component_url . '&opt=list&sub_opt=model');
            } break;
            case 'ad': {
                $ad = $model->getAd(cmsCore::request('uid'), false);
                if(!$ad){
                    cmsCore::error404();
                }

                $model->adminRemoveAd($ad['id']);

                cmsCore::addSessionMessage($_LANG['AD_DO_SUCCESS'], 'success');
                cmsCore::redirect($component_url . '&opt=list&sub_opt=ad');
            } break;
        }
    } break;

    case 'saveconfig' : {

        $images = $_REQUEST['images'];
        $formats = array();
        foreach($images['ad'] as $imgFormat){

            $imgFormat = array(
                'name'      => trim($imgFormat['name']),
                'weight'    => (int)$imgFormat['weight'],
                'height'    => (int)$imgFormat['height'],
                'thumb'     => (bool)$imgFormat['thumb'],
                'watermark' => (bool)$imgFormat['watermark'],
                'sys'       => (bool)$imgFormat['sys'],
            );

            if(!$imgFormat['name']){
                continue;
            }

            $formats[] = $imgFormat;
        }

        $images['ad'] = $formats;

        $cfg = array(
            'system' => $_REQUEST['system'],
            'ad'     => $_REQUEST['ad'],
            'images' => $images,
            'geo'    => $_REQUEST['geo'],
            'currency' => $cfg['currency']
        );

        if(HAVE_COMPONENT_BILLING){
            $cfg['services'] =$_REQUEST['services'];
        }

        //$cfg['filestype'] = mb_strtolower(cmsCore::request('filestype', 'str', 'jpeg,gif,png,jpg,bmp,zip,rar,tar'));

        $inCore->saveComponentConfig(COMPONENT_NAME, $cfg);

        cmsCore::addSessionMessage($_LANG['AD_CONFIG_SAVE_SUCCESS'], 'success');
        cmsCore::redirect('?view=components&do=config&id='.$id.'&opt=config');
    } break;
    default: {
        $vars['stats'] = array(
            'count' => array(
                'all'        => $model->dbGetCount('stats', 'all'),
                'active'     => $model->dbGetCount('stats', 'active'),
                'new'        => $model->dbGetCount('stats', 'new'),
                'sell'       => $model->dbGetCount('stats', 'sell'),
                'moderation' => $model->dbGetCount('stats', 'moderation'),
            )
        );
    } break;
}



/* @var Smarty $smarty*/
$smarty = $inCore->initSmarty(COMPONENT_TEMPLATE_PATH, COMPONENT_TEMPLATE_CONTAINER);
$smarty->assign(array_merge(array(
        'componentId' => $componentId,
        'LANG'        => $_LANG,
        'opt'         => $opt,
        'sub_opt'     => $sub_opt,
        'cfg'         => $cfg,
        'component_data' => $component_data,
        'component_url' => $component_url,
        'is_admin'    => true
    ), $vars)
);


$smarty->assign($vars);

echo $smarty->fetch(COMPONENT_TEMPLATE_CONTAINER);

if($cfg['system']['debug']){
    $model->showQueryStack();
}

?>
