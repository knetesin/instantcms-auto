<?php
/* * * * * * * * * * * * * * * * * * *
 * Created by Netesin Kirill         *
 * e-mail: k.netesin@gmail.com       *
 * * * * * * * * * * * * * * * * * * */
if(!defined('VALID_CMS')) { die('ACCESS DENIED'); }

define('COMPONENT_NAME', 'auto');
define('COMPONENT_HOME_URL', '/' . COMPONENT_NAME);
define('COMPONENT_TEMPLATE_PATH', 'components/' . COMPONENT_NAME);
define('COMPONENT_TEMPLATE_CONTAINER', '_auto_component_container.tpl');

function auto(){
    /* @var Smarty $smarty*/

    // region init
    $inCore = cmsCore::getInstance();
    $inPage = cmsPage::getInstance();
    $inUser = cmsUser::getInstance();
    $inConf = cmsConfig::getInstance();

    // region other components defines

    define('HAVE_COMPONENT_BILLING', $inCore->isComponentInstalled('billing'));
    define('HAVE_COMPONENT_SUPPORT', $inCore->isComponentInstalled('support'));


    if (HAVE_COMPONENT_BILLING) {
        $inCore->loadClass('billing');
        $billing = array(
            'balance' => $inUser->balance
        );

    }

    if (HAVE_COMPONENT_SUPPORT) {
        $inCore->loadClass('support');
    }

    // endregion

    $cfg = $inCore->loadComponentConfig(COMPONENT_NAME);

    $inCore->loadLanguage('components/' . COMPONENT_NAME);
    global $_LANG;

    $component_data_array = include_once PATH . '/components/' . COMPONENT_NAME . '/includes/component_data.php';

    $currency = (int)$inCore->getCookie('currency');
    $currency = $currency > 0 ? $currency : 1;

    $routes = $component_data_array['routes'];
    $component_data = $component_data_array['component_data'];

    // endregion

    // region meta
    $inPage->setKeywords(strlen(!$cfg['metaKeywords']) ?: $cfg['metaKeywords']);
    $inPage->setDescription(strlen(!$cfg['metaDescription']) ?: $cfg['metaDescription']);
    $inPage->setTitle(strlen(!$cfg['metaTitle']) ?: $cfg['metaTitle']);
    // endregion

    // region assets
    $inPage->addHead('<!-- region for auto component -->');

    $inPage->addHeadCSS('components/' . COMPONENT_NAME . '/styles/default/css/main.css?v=2');
    $inPage->addHeadCSS('components/' . COMPONENT_NAME . '/styles/default/css/print.css?v=2');
    $inPage->addHeadJS('components/' . COMPONENT_NAME . '/js/auto.js');
    $inPage->addHeadJS('//code.jquery.com/ui/1.10.4/jquery-ui.js');
    // endregion

    // region pathway
    $inPage->addPathway('Продажа автомобилей', COMPONENT_HOME_URL);
    // endregion

    // region debugMode
    if ($cfg['system']['debug'] and !$inUser->is_admin){
        cmsCore::error404();
    }
    // endregion

    // for instant component en / dis
    if(!$cfg['component_enabled']) {
        cmsCore::error404();
    }

    $inCore->loadModel(COMPONENT_NAME);
    //cms_model_auto::parseCurrency();
    $model = new cms_model_auto($component_data, $routes, $_LANG);

    $do = $inCore->request('do', 'str', 'index');

    $initGallery = (function(&$inPage){
        $inPage->addHeadCSS('components/' . COMPONENT_NAME . '/styles/fancy_box/jquery.fancybox-buttons.css?v=1.0.3');
        $inPage->addHeadCSS('components/' . COMPONENT_NAME . '/styles/fancy_box/jquery.fancybox-thumbs.css?v=1.0.6');
        $inPage->addHeadCSS('components/' . COMPONENT_NAME . '/styles/fancy_box/jquery.fancybox.css?v=2.1.0');
        $inPage->addHeadJS('components/' . COMPONENT_NAME . '/js/fancy_box/jquery.fancybox.js?v=2.1.0');
        $inPage->addHeadJS('components/' . COMPONENT_NAME . '/js/fancy_box/jquery.fancybox-buttons.js?v=1.0.3');
        $inPage->addHeadJS('components/' . COMPONENT_NAME . '/js/fancy_box/jquery.fancybox-thumbs.js?v=1.0.6');
        $inPage->addHeadJS('components/' . COMPONENT_NAME . '/js/fancy_box/jquery.fancybox-media.js?v=1.0.3');
    });
    $reqAssign = array(
        'cfg'            => $cfg,
        'currency'       => $currency,
        'routes'         => $routes,
        'do'             => $do,
        'is_admin'       => $inUser->is_admin,
        'component_data' => $component_data,
        'billing'        => HAVE_COMPONENT_BILLING ? $billing : null
    );

    //var_dump(cmsBilling::process('auto', 'up_ad'));
    switch ($do){
        // region controller api
        case 'api': {

//            if($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') { cmsCore::error404(); }
            $response = '';

            switch(cmsCore::request('action')){
                case 'ch_geo': {
                    $model->setGeo(cmsCore::request('region', 'int'), cmsCore::request('city', 'int'));

                    cmsCore::redirectBack();
                } break;
                case 'photo':{

                    if (!$inUser->id or !$adId = $inCore->request('id','int')){
                        echo json_encode(array(
                            'code'   => 403,
                            'reason' => 'denied'
                        ));
                    }

                    switch ($inCore->request('method')) {
                        case 'add':{
                            $ad = $model->getAd($adId, false);

                            if(!$ad['auto_status']){
                                cmsCore::error404();
                            }

                            if(!$ad or $ad['id_owner'] != $inUser->id ) {
                                echo json_encode(array(
                                    array(
                                        'code'   => 403,
                                        'codeMsg' => '',
                                        'status' => 0,
                                        'reason' => 'denied'
                                    )
                                ));

                                exit();
                            }

                            if (!$ad['auto_status']) {
                                echo json_encode(array(
                                    array(
                                        'code'   => 403,
                                        'codeMsg' => '',
                                        'status' => 0,
                                        'reason' => 'already sold'
                                    )
                                ));

                                exit();
                            }

                            if($model->getPhotoCount($adId) >= $cfg['ad']['max_photo']){
                                echo json_encode(array(
                                    array(
                                        'code' => 500,
                                        'codeMsg' => 'Лимит загрузки фотографий',
                                        'status' => 0,
                                        'reason' => 'limit for ad'
                                    )
                                ));

                                exit();
                            }

                            $response =  json_encode($model->addPhoto($adId));
                        } break;
                        case 'remove': {
                            if(!cmsCore::isAjax()) { cmsCore::error404(); }

                            $photo = $model->getPhoto($adId);

                            if(!$photo) {
                                echo json_encode(array(
                                    'code'   => 403,
                                    'reason' => 'denied'
                                ));

                                exit();
                            }

                            $ad = $model->getAd($photo['id_unit'], false);

                            if(!$ad['auto_status']){
                                cmsCore::error404();
                            }

                            if($ad['id_owner'] != $inUser->id) {
                                echo json_encode(array(
                                    'code'   => 403,
                                    'reason' => 'denied'
                                ));


                                exit();
                            }

                            if (!$ad['auto_status']) {
                                echo json_encode(array(
                                    'code'   => 403,
                                    'reason' => 'already sold'
                                ));

                                exit();
                            }

                            $response =  json_encode($model->removePhoto($photo['id']));

                        } break;
                        case 'setmain': {
                            if(!cmsCore::isAjax()) { cmsCore::error404(); }

                            $photo = $model->getPhoto($adId);

                            if(!$photo) {
                                echo json_encode(array(
                                    'code'   => 403,
                                    'reason' => 'denied'
                                ));

                                exit();
                            }

                            $ad = $model->getAd($photo['id_unit'], false);

                            if(!$ad['auto_status']){
                                cmsCore::error404();
                            }

                            if($ad['id_owner'] != $inUser->id) {
                                echo json_encode(array(
                                    'code'   => 403,
                                    'reason' => 'denied'
                                ));


                                exit();
                            }

                            if (!$ad['auto_status']) {
                                echo json_encode(array(
                                    'code'   => 403,
                                    'reason' => 'already sold'
                                ));

                                exit();
                            }

                            $response = json_encode($model->setMainPhoto($photo['id_unit'], $adId));

                        } break;
                    }

                    echo $response;
                    exit();

                } break;
                case 'geo': {
                    if(!cmsCore::isAjax()) { cmsCore::error404(); }

                    switch(cmsCore::request('method')){
                        case 'getcities': {
                            echo $model->apiGetCitiesForRegion(cmsCore::request('id'));
                        } break;
                        case 'getregions': {
                            echo $model->apiGetRegionsForCountry(cmsCore::request('id'));
                        } break;
                    }

                    exit();
                } break;
                case 'vendors': {
                    if(!cmsCore::isAjax()) { cmsCore::error404(); }

                    switch(cmsCore::request('method')){
                        case 'getmodels': {
                            echo $model->apiGetModelsForVendor(cmsCore::request('id'));
                        } break;
                    }

                    exit();
                } break;
                case 'ad': {
                    if(!cmsCore::isAjax()) { cmsCore::error404(); }

                    if(!$ad = $model->getAd(cmsCore::request('id', 'int'), false) and cmsCore::request('method') != 'answer'){
                        echo json_encode(array(
                            array(
                                'code'   => 403,
                                'status' => 0,
                                'reason' => 'denied'
                            )
                        ));

                        exit();
                    }

                    switch(cmsCore::request('method')){
                        case 'showphone': {
                            $response = $model->apiGetPhoneByAd($ad);
                        } break;
                        case 'question': {
                            switch(cmsCore::request('smethod')){
                                case 'add': {

                                    if(!$ad['auto_status']){
                                        echo json_encode(array(
                                            array(
                                                'code'   => 403,
                                                'status' => 0,
                                                'reason' => 'denied'
                                            )
                                        ));
                                    }

                                    if($ad['id_owner'] == $inUser->id or !$ad['access_issues']){
                                        echo json_encode(array(
                                            array(
                                                'code'   => 403,
                                                'status' => 0,
                                                'reason' => 'denied'
                                            )
                                        ));

                                        exit();
                                    }

                                    $response =  $model->apiInsertQuestion($ad['id'], array(
                                        'author'  => cmsCore::request('author'),
                                        'message' => cmsCore::request('message'),
                                        'email'   => cmsCore::request('email'),
                                        'phone'   => cmsCore::request('phone'),
                                        'show_type'  => ($inUser->id) ? cmsCore::request('show_type') : 1,
                                        'id_user' => $inUser->id,
                                    ));
                                } break;
                                case 'remove': {} break;
                            }
                        } break;

                        case 'answer': {
                            switch(cmsCore::request('smethod')){
                                case 'add': {
                                    $question = $model->getAdQuestion(cmsCore::request('id', 'int'));

                                    if($question['id_owner'] != $inUser->id or $question['answer']){
                                        echo json_encode(array(
                                            array(
                                                'code'   => 403,
                                                'status' => 0,
                                                'reason' => 'denied'
                                            )
                                        ));

                                        exit();
                                    }

                                    $response =  $model->apiInsertAnswer($question['id'], array(
                                        'answer'  => cmsCore::request('answer'),
                                        'id_unit' => $question['id_unit'],
                                        'email'   => $question['email']
                                    ));
                                } break;
                                case 'remove': {} break;
                            }
                        } break;

                        case 'sell': {
                            if(!$ad['auto_status'] or $ad['id_owner'] != $inUser->id){
                                echo json_encode(array(
                                    array(
                                        'code'   => 403,
                                        'status' => 0,
                                        'reason' => 'denied, not owner or already sold'
                                    )
                                ));
                                exit();
                            }

                            $response = $model->apiAdSetSell($ad['id']);
                        } break;

                    }

                    echo $response;
                    exit;


                } break;
                case 'services': {
                    if(!cmsCore::isAjax()) { cmsCore::error404(); }

                    if(!HAVE_COMPONENT_BILLING){
                        cmsCore::error404();
                    }

                    if(!$inUser->id){
                        cmsCore::jsonOutput(array(
                            'code'   => 403,
                            'status' => 0,
                            'message' => 'Вы не авторизованы!'
                        ));
                    }

                    if(!$ad = $model->getAd(cmsCore::request('id', 'int'), false)){
                        cmsCore::jsonOutput(array(
                            'code'   => 403,
                            'status' => 0,
                            'message' => 'Нет доступа'
                        ));
                    }

                    if(!$ad['auto_status']){
                        cmsCore::jsonOutput(array(
                            'code'   => 403,
                            'status' => 0,
                            'reason' => 'denied'
                        ));
                    }

                    // TODO check balance


                    switch($type = cmsCore::request('type')){
                        case 'up': {
                            $cost = (int)$cfg['services']['up']['cost'];
                            if($inUser->balance >= $cost){
                                if($ad['ordering'] > time()){
                                    $response = array(
                                        'code'   => 403,
                                        'message'=> 'Объявление было прикреплено, данная услуга временно недоступна!',
                                        'status' => 'error'
                                    );
                                } else {
                                    $response = $model->apiServices($type, cmsCore::request('id'));
                                    cmsBilling::pay($inUser->id, $cost, 'Поднять объявление');
                                }
                            } else {
                                $response = array(
                                    'code'   => 403,
                                    'message'=> 'У вас недостаточно средств для данной услуги!',
                                    'status' => 'error'
                                );
                            }


                        } break;
                        case 'premium': {
                            $cost = (int)$cfg['services']['vip']['cost'];
                            if($inUser->balance >= $cost){
                                if($ad['p_vip_date_u'] > time()){
                                    $response = array(
                                        'code'   => 403,
                                        'message'=> 'Выбранная услуга для данного объявления еще активна!',
                                        'status' => 'error'
                                    );
                                } else {
                                    $response = $model->apiServices($type, cmsCore::request('id'));
                                    cmsBilling::pay($inUser->id, $cost, 'Спецразмещение');
                                }
                            } else {
                                $response = array(
                                    'code'   => 403,
                                    'message'=> 'У вас недостаточно средств для данной услуги!',
                                    'status' => 'error'
                                );
                            }

                        } break;
                        case 'attach': {
                            $cost = (int)$cfg['services']['attach']['cost'];
                            if($inUser->balance >= $cost){
                                if($ad['ordering'] > time()){
                                    $response = array(
                                        'code'   => 403,
                                        'message'=> 'Выбранная услуга для данного объявления еще активна!',
                                        'status' => 'error'
                                    );
                                } else {
                                    $response = $model->apiServices($type, cmsCore::request('id'));
                                    cmsBilling::pay($inUser->id, $cost, 'Прикрепить объявление');
                                }
                            } else {
                                $response = array(
                                    'code'   => 403,
                                    'message'=> 'У вас недостаточно средств для данной услуги!',
                                    'status' => 'error'
                                );
                            }

                        } break;
                        case 'up_bg': {
                            $cost = (int)$cfg['services']['up_bg']['cost'];
                            if($inUser->balance >= $cost){
                                if($ad['p_bg_date_u'] > time()){
                                    $response = array(
                                        'code'   => 403,
                                        'message'=> 'Выбранная услуга для данного объявления еще активна!',
                                        'status' => 'error'
                                    );
                                } else {
                                    $response = $model->apiServices($type, cmsCore::request('id'), $ad);
                                    cmsBilling::pay($inUser->id, $cost, 'Поднять и выделить объявление цветом');
                                }
                            } else {
                                $response = array(
                                    'code'   => 403,
                                    'message'=> 'У вас недостаточно средств для данной услуги!',
                                    'status' => 'error'
                                );
                            }

                        } break;
                        default: {
                            $response = array(
                                'code'  => 404,
                                'status' => 'error',
                                'message' => 'Что то пошло не так :('
                            );
                        } break;
                    }

                    cmsCore::jsonOutput($response);
                } break;
            }

            exit();

        } break;
        // endregion
        // region controller index page
        case 'index': {
            // region meta
            $inPage->setTitle($_LANG['TEMPLATE_INDEX_PAGE_TITLE']);
            $inPage->setKeywords($_LANG['TEMPLATE_INDEX_PAGE_KEYWORDS']);
            $inPage->setDescription($_LANG['TEMPLATE_INDEX_PAGE_DESCRIPTION']);
            // endregion

            $initGallery($inPage);

            $smarty = $inCore->initSmarty(COMPONENT_TEMPLATE_PATH, COMPONENT_TEMPLATE_CONTAINER);
            $smarty->assign($reqAssign);
            $smarty->assign(array(
                'cats'     => $model->getCatsColumn($cfg['system']['countColumnMain'], $model->getVendorsList(true), array(
                        'type' => 'vendor'
                    )),
                'carousel' => $cfg['ad']['carousel']['active'] ? $model->getCarousel() : null,
                'filter'   => $model->getSearchFilterParamsByQuery()
            ));

        } break;
        // endregion
        // region controller vendors action
        case 'showVendor': {
            $vendor = $model->getVendorByUrlTitle($inCore->request('vendor'));
            if(!$vendor){
                cmsCore::error404();
            }

            $initGallery($inPage);

            $inPage->addPathway(sprintf($_LANG['TEMPLATE_SHOWVENDOR_PAGE_PATHWAY'], $vendor['title']), sprintf($routes['showVendor'], $vendor['title_url']));
            // region meta
            $inPage->setTitle(sprintf($_LANG['TEMPLATE_VENDOR_INDEX_PAGE_TITLE'], $vendor['title']));
            $inPage->setKeywords(sprintf($_LANG['TEMPLATE_VENDOR_INDEX_PAGE_KEYWORDS'], $vendor['title']));
            $inPage->setDescription(sprintf($_LANG['TEMPLATE_VENDOR_INDEX_PAGE_DESCRIPTION'], $vendor['title']));
            // endregion

            $smarty = $inCore->initSmarty(COMPONENT_TEMPLATE_PATH, COMPONENT_TEMPLATE_CONTAINER);
            $smarty->assign($reqAssign);
            $smarty->assign(array(
                'cats'           => $model->getCatsColumn($cfg['system']['countColumnOther'], $model->getModelsByVendor($vendor['id'], true, false), array(
                        'type'       => 'model',
                        'vendor_url' => $vendor['title_url']
                    )),
                'carousel'       => $cfg['ad']['carousel']['active'] ? $model->getCarousel(array(
                        'vendor' => $vendor
                    )) : null,
                'filter' => $model->getSearchFilterParamsByQuery(array(
                        'id_vendor' => $vendor['id']
                    )),
                'controlLinks'   => array('title' => $vendor['title'], 'url' => $vendor['title_url']),
                'vendor'         => $vendor,

                'ads'            => array(
                    'data' => $model->getAdsList(
                            array(
                                'id_vendor' => $vendor['id']
                            ),
                            $cfg['ad']['list']['sub_vendor']['per_page'],
                            false
                        ),
                    'ordering' => false
                )
            ));


            //$mcount > 5 ?  $smarty->assign('scrollSlider', $model->getScrollSlider($item['id'])) :  $smarty->assign('scrollSlider', $model->getScrollSlider(0,0, $item['id']));


        } break;
        // Список объявлений для проиводителя
        case 'showVendorList': {
            // Список объявлений для проиводителя
            $vendor = $model->getVendorByUrlTitle($inCore->request('vendor'));
            if(!$vendor){
                cmsCore::error404();
            }

            $initGallery($inPage);

            $inPage->addPathway(sprintf($_LANG['TEMPLATE_SHOWVENDOR_PAGE_PATHWAY'], $vendor['title']), sprintf($routes['showVendor'], $vendor['title_url']));

            $smarty = $inCore->initSmarty(COMPONENT_TEMPLATE_PATH, COMPONENT_TEMPLATE_CONTAINER);
            $smarty->assign($reqAssign);
            $smarty->assign(array(
                'carousel'       => $cfg['ad']['carousel']['active'] ? $model->getCarousel(array(
                        'vendor' => $vendor
                    )) : null,
                'ads'    => array(
                    'url'      => sprintf($routes['showVendorList'], $vendor['title_url']),
                    'data'     => $model->getAdsList($q = $model->getSearchFilterParamsByQuery(array('id_vendor' => $vendor['id']), true), $cfg['ad']['list']['search']['per_page']),
                    'ordering' => true
                ),
                'filter' => $q
            ));

        } break;
        // endregion
        // region showModelList
        case 'showModelList': {

            if(!$vendor = $model->getVendorByUrlTitle($inCore->request('vendor'))){
                cmsCore::error404();
            }

            if(!$modelByVendor = $model->getModelByUrlTitle($vendor['id'], $inCore->request('model'))){
                cmsCore::error404();
            }

            $inPage->addPathway(sprintf($_LANG['TEMPLATE_SHOWVENDORLIST_PAGE_PATHWAY'], $vendor['title']), sprintf($routes['showVendor'], $vendor['title_url']));
            $inPage->addPathway(sprintf($_LANG['TEMPLATE_MODELLIST_INDEX_PAGE_PATHWAY'], $vendor['title'], $modelByVendor['title']), sprintf($routes['showModel'], $vendor['title_url'], $modelByVendor['title_url']));

            // region meta
            $inPage->setTitle(sprintf($_LANG['TEMPLATE_MODELLIST_INDEX_PAGE_TITLE'], $vendor['title'], $modelByVendor['title']));
            $inPage->setKeywords(sprintf($_LANG['TEMPLATE_MODELLIST_INDEX_PAGE_KEYWORDS'], $vendor['title'], $modelByVendor['title']));
            $inPage->setDescription(sprintf($_LANG['TEMPLATE_MODELLIST_INDEX_PAGE_DESCRIPTION'], $vendor['title'], $modelByVendor['title']));
            // endregion

            $initGallery($inPage);

            $smarty = $inCore->initSmarty(COMPONENT_TEMPLATE_PATH, COMPONENT_TEMPLATE_CONTAINER);
            $smarty->assign($reqAssign);
            $smarty->assign(array(
                    'carousel'       => $cfg['ad']['carousel']['active'] ? $model->getCarousel(array(
                            'vendor' => $vendor,
                            'model'  => $modelByVendor
                        )) : null,
                    'ads' => array(
                        'url'      => sprintf($routes['showModel'], $vendor['title_url'], $modelByVendor['title_url']),
                        'data'     => $model->getAdsList($q = $model->getSearchFilterParamsByQuery(array('id_vendor' => $vendor['id'], 'id_model' => $modelByVendor['id']), true), $cfg['ad']['list']['search']['per_page']),
                        'ordering' => true
                    ),
                    'controlLinks' => array('title' => $vendor['title'], 'url' => $vendor['title_url']),
                    'filter'       => $model->getSearchFilterParamsByQuery(array('id_vendor' => $vendor['id'], 'id_model'  => $modelByVendor['id']), true)
                )
            );

        } break;
        // endregion
        // region controller cabinet
        case 'cabinet': {
            if (!$inUser->id){
                cmsUser::goToLogin();
            }

            $inPage->addPathway($_LANG['TEMPLATE_CABINET_INDEX_PAGE_PATHWAY'], $routes['cabinet']['index']);

            switch($reqAssign['subaction1'] = $inCore->request('action')){
                case 'ads': {
                    $inPage->addPathway('Мои объявления', $routes['cabinet']['adslist']);

                    switch($reqAssign['subaction2'] = $inCore->request('subaction')){
                        case 'edit': {
                            $ad = $model->getAd($adId = cmsCore::request('id', 'int'));

                            if(!$ad){
                                cmsCore::error404();
                            }

                            $reqAssign['ad'] = $ad;
                            if ($ad['id_owner'] != $inUser->id){
                                cmsCore::error404();
                            }

                            switch($reqAssign['subaction3'] = cmsCore::request('type')){
                                case 'pm': {

                                    if(!$ad['auto_status']){
                                        cmsCore::error404();
                                    }

                                    $initGallery($inPage);
                                    $inPage->addHeadJS('components/' . COMPONENT_NAME . '/js/jquery.ui.widget.js');
                                    $inPage->addHeadJS('components/' . COMPONENT_NAME . '/js/jquery.fileupload.js');

                                    $inPage->addPathway('Управление фотографиями');
                                    $inPage->setTitle("Управление фотографиями {$ad['title_vendor']} {$ad['title_model']} (номер объявления: {$adId})");

                                    $smarty = $inCore->initSmarty(COMPONENT_TEMPLATE_PATH, COMPONENT_TEMPLATE_CONTAINER);
                                    $smarty->assign($reqAssign);
                                    $smarty->assign(array(
                                        'photoList' => $ad['data']['photo']
                                    ));

                                    $smarty->assign('data', $ad);
                                } break;
                                case 'questions': {

                                    $inPage->addPathway('Управление вопросами');
                                    $inPage->setTitle("Управление вопросами {$ad['title_vendor']} {$ad['title_model']} (номер объявления: {$adId})");

                                    $smarty = $inCore->initSmarty(COMPONENT_TEMPLATE_PATH, COMPONENT_TEMPLATE_CONTAINER);
                                    $smarty->assign($reqAssign);
                                    $smarty->assign(array(
                                        'questions' => $model->getAdQa($adId, true)
                                    ));

                                    $model->setAdQaReadByAd($adId);

                                    $smarty->assign('data', $ad);
                                } break;
                                default: {
                                    if(!$ad['auto_status']){
                                        cmsCore::error404();
                                    }

                                    $inPage->addHeadJS('components/' . COMPONENT_NAME . '/js/ckeditor/ckeditor.js');
                                    $inPage->addHeadJS('components/' . COMPONENT_NAME . '/js/ckeditor/adapters/jquery.js');

                                    if($inUser->is_admin){
                                        $inPage->addHeadJS('components/' . COMPONENT_NAME . '/js/jquery.minicolors.min.js');
                                    }

                                    $inPage->addPathway(sprintf($_LANG['TEMPLATE_EDIT_AD_PAGE_PATHWAY'], $ad['id']), sprintf($routes['cabinet']['edit'], $ad['id']));

                                    // region meta
                                    $inPage->setTitle(sprintf($_LANG['TEMPLATE_EDIT_AD_PAGE_TITLE'], $ad['id']));
                                    $inPage->setKeywords($_LANG['TEMPLATE_EDIT_AD_PAGE_KEYWORDS']);
                                    $inPage->setDescription($_LANG['TEMPLATE_EDIT_AD_PAGE_DESCRIPTION']);
                                    // endregion

                                    $data = array(
                                        'data' => array(
                                            'vendors' => $model->getVendorsList(false, true),
                                            'date'    => $model->getDateList()
                                        ),
                                        'geo' => array(
                                            'region'  => $cfg['geo']['region']['default'] ? $model->geo->getRegion($cfg['geo']['region']['default']) : null,
                                            'city'    => $cfg['geo']['region']['default'] ? ($cfg['geo']['city']['default'] ? $model->geo->getCity($cfg['geo']['city']['default']) : null) : null
                                        ),

                                        'user_id' => $inUser->id,
                                    );

                                    $check = $model->checkAddForm(array(
                                        'vendor' => $ad['data']['id_vendor'],
                                        'model' => $ad['data']['id_model'],
                                        'geo' => array(
                                            'region' => $ad['data']['region_id'],
                                            'city'   => $ad['data']['city_id']
                                        )
                                    ));

                                    if ($check['status'] == 0){

                                        $smarty = $inCore->initSmarty(COMPONENT_TEMPLATE_PATH, COMPONENT_TEMPLATE_CONTAINER);
                                        $smarty->assign($reqAssign);
                                        $smarty->assign($data);

                                        $preset = $model->formatPresetAdtoEdit($ad);

                                        $smarty->assign(array(
                                            'set' => $preset,
                                            'is_edit' => true
                                        ));

                                    } elseif($check['status'] == 2) {
                                        $model->updateAd($ad['id'], $check['setters']);
                                        //cmsCore::redirect(sprintf($routes['cabinet']['edit'], $ad['id']));
                                        cmsCore::redirect(sprintf($routes['show'], $ad['id']));
                                    } else {

                                        $smarty = $inCore->initSmarty(COMPONENT_TEMPLATE_PATH, COMPONENT_TEMPLATE_CONTAINER);
                                        $smarty->assign($reqAssign);
                                        $smarty->assign($data);
                                        $smarty->assign(array(
                                            'check' => $check,
                                            'set'   => $check['setters'],
                                            'errors' => $check['errors']
                                        ));

                                        $preset = $model->formatPresetAdtoEdit($ad);
                                        $smarty->assign(array(
                                            'set' => $preset,
                                            'is_edit' => true
                                        ));
                                    }
                                } break;
                            }
                        } break;
                        default: {
                            $inPage->addPathway($_LANG['TEMPLATE_CABINET_ADS_LIST_PAGE_PATHWAY'], $routes['cabinet']['adslist']);
                            // region meta
                            $inPage->setTitle($_LANG['TEMPLATE_CABINET_ADS_LIST_PAGE_TITLE']);
                            $inPage->setKeywords($_LANG['TEMPLATE_CABINET_ADS_LIST_PAGE_KEYWORDS']);
                            $inPage->setDescription($_LANG['TEMPLATE_CABINET_ADS_LIST_PAGE_DESCRIPTION']);
                            // endregion

                            $initGallery($inPage);

                            $smarty = $inCore->initSmarty(COMPONENT_TEMPLATE_PATH, COMPONENT_TEMPLATE_CONTAINER);
                            $smarty->assign($reqAssign);

                            $smarty->assign(array(
                                'data'  => $model->getAdsListForCabinet($inUser->id),
                                'count' => $model->getCount('auto_units',"WHERE `id_owner` = {$inUser->id}")
                            ));
                        } break;
                    }
                } break;
                case 'my_questions': {

                    $inPage->addPathway($_LANG['TEMPLATE_CABINET_MY_QUESTIONS_PAGE_PATHWAY']);

                    // region meta
                    $inPage->setTitle($_LANG['TEMPLATE_CABINET_MY_QUESTIONS_PAGE_TITLE']);
                    $inPage->setKeywords($_LANG['TEMPLATE_CABINET_MY_QUESTIONS_PAGE_KEYWORDS']);
                    $inPage->setDescription($_LANG['TEMPLATE_CABINET_MY_QUESTIONS_PAGE_DESCRIPTION']);
                    // endregion

                    $smarty = $inCore->initSmarty(COMPONENT_TEMPLATE_PATH, COMPONENT_TEMPLATE_CONTAINER);
                    $smarty->assign($reqAssign);

                    $smarty->assign(array(
                        'data_q' => $model->getUserQuestions($inUser->id)
                    ));
                } break;
                default: {
                    // region meta
                    $inPage->setTitle($_LANG['TEMPLATE_CABINET_INDEX_PAGE_TITLE']);
                    $inPage->setKeywords($_LANG['TEMPLATE_CABINET_INDEX_PAGE_KEYWORDS']);
                    $inPage->setDescription($_LANG['TEMPLATE_CABINET_INDEX_PAGE_DESCRIPTION']);
                    // endregion

                    $data['countadvt'] = $model->dbGetCount('userAd', $inUser->id);

                    $smarty = $inCore->initSmarty(COMPONENT_TEMPLATE_PATH, COMPONENT_TEMPLATE_CONTAINER);
                    $smarty->assign($reqAssign);
                    $smarty->assign('data', array(
                        'count' => array(
                            'ad' => $model->dbGetCount('userAd', $inUser->id),
                            'questions' => $model->dbGetCount('userQuestions', $inUser->id),
                        ),
                        'news' => $model->getNewsForCabinet()
                    ));

                } break;
            }


        } break;
        // endregion
        // region controller show
        case 'show': {

            $data = $model->getAd($inCore->request('id', 'int'));

            if (!$data){
                cmsCore::error404();
            }

            // Не показывать, либо на премодерации
            if(in_array($data['show'], array(0, 1)) and $inUser->id != $data['id_owner']){
                cmsCore::error404();
            }

            if(cmsCore::inRequest('vendor') and cmsCore::inRequest('model')){
                if(cmsCore::request('vendor') != $data['url_vendor'] or cmsCore::request('model') != $data['url_model']){
                    cmsCore::error404();
                }
            } else {
                cmsCore::redirect(sprintf($routes['show_full'], $data['url_vendor'], $data['url_model'], $data['id']), 301);
            }

            $initGallery($inPage);

            $inPage->addPathway($data['title_vendor'], sprintf($routes['showVendor'], $data['url_vendor']));
            $inPage->addPathway($data['title_model'], sprintf($routes['showModel'], $data['url_vendor'], $data['url_model']));

            $inPage->addPathway(sprintf($_LANG['TEMPLATE_SHOW_PAGE_PATHWAY'], $data['id']));
            // region meta
            $inPage->setTitle(sprintf($_LANG['TEMPLATE_SHOW_PAGE_TITLE'], "{$data['title_vendor']} {$data['title_model']}", $data['date_construct'], $data['city_title']));
            $inPage->setKeywords(sprintf($_LANG['TEMPLATE_SHOW_PAGE_KEYWORDS'], $data['city_title'], $data['title_vendor'], $data['title_model'], $data['price'] . ' ' . $component_data['currency'][$data['currency']]));
            $inPage->setDescription(sprintf($_LANG['TEMPLATE_SHOW_PAGE_DESCRIPTION'], $data['city_title'], $data['title_vendor'], $data['title_model'], $data['price'] . ' ' . $component_data['currency'][$data['currency']]));
            // endregion

            if($cfg['ad']['limit_views']){
                if(!$model->viewCounter($data['id'])){
                    $model->incViews($data['id']);
                    $data['data']['count_views']++;
                }
            } else {
                $model->incViews($data['id']);
                $data['data']['count_views']++;
            }

            $smarty = $inCore->initSmarty(COMPONENT_TEMPLATE_PATH, COMPONENT_TEMPLATE_CONTAINER);
            $smarty->assign($reqAssign);
            $smarty->assign(array(
                'user_id' => $inUser->id,
                'ad'      => $data,
                'ads'    => array(
                    'data'     => $model->getAdsList($q = $model->getSearchFilterParamsByQuery(array(
                            'carcase' => array($data['data']['carcase_id']),
                            'unsold' => 1,
                            'minprice' => $data['data']['price'] / 2,
                            'maxprice' => $data['data']['price'] * 1.5,
                            'currency' => $data['data']['currency'],
                            'nin'      => array($data['data']['id'])
                        )), 5, false),
                    'ordering' => false
                ),
            ));

        } break;
        // endregion
        // region controller add
        case 'add': {
            if(!$cfg['ad']['add']['autoreg'] and !$inUser->id){
                cmsUser::goToLogin();
            }

            $inPage->addHeadJS('components/' . COMPONENT_NAME . '/js/ckeditor/ckeditor.js');
            $inPage->addHeadJS('components/' . COMPONENT_NAME . '/js/ckeditor/adapters/jquery.js');

            if($inUser->is_admin){
                $inPage->addHeadJS('components/' . COMPONENT_NAME . '/js/jquery.minicolors.min.js');
            }

            $inPage->addPathway($_LANG['TEMPLATE_ADD_PAGE_PATHWAY'], $routes['add']);

            // region meta
            $inPage->setTitle($_LANG['TEMPLATE_ADD_PAGE_TITLE']);
            $inPage->setKeywords($_LANG['TEMPLATE_ADD_PAGE_KEYWORDS']);
            $inPage->setDescription($_LANG['TEMPLATE_ADD_PAGE_DESCRIPTION']);
            // endregion

            $data = array(
                'data' => array(
                    'vendors' => $model->getVendorsList(false, true),
                    'date'    => $model->getDateList()
                ),
                'user_id' => $inUser->id,
            );

            $check = $model->checkAddForm();

            if ($check['status'] == 0){

                $smarty = $inCore->initSmarty(COMPONENT_TEMPLATE_PATH, COMPONENT_TEMPLATE_CONTAINER);
                $smarty->assign($reqAssign);
                $smarty->assign($data);

                $preset = array(
                    'access_issues' => true
                );


                if($cfg['geo']['region']['default']){
                    $preset['geo']['region'] = $model->geo->getRegion($cfg['geo']['region']['default']);

                    if(!$cfg['geo']['city']['default']){
                        $preset['geo']['cities'] = $model->geo->getCities($cfg['geo']['region']['default']);
                        $preset['geo']['city']['id'] = ($cfg['geo']['city']['preset']) ? $cfg['geo']['city']['preset'] : null;
                    } else {
                        $preset['geo']['city'] = $model->geo->getCity($cfg['geo']['city']['default']);
                    }
                } else {
                    $preset['geo']['regions'] = $model->geo->getRegions($cfg['geo']['country']);

                    if($cfg['geo']['region']['preset']){
                        $preset['geo']['region']['id'] = $cfg['geo']['region']['preset'];

                        $preset['geo']['cities'] = $model->geo->getCities($cfg['geo']['region']['preset']);
                        if($cfg['geo']['city']['preset']){
                            $preset['geo']['city']['id'] = $cfg['geo']['city']['preset'];
                        }
                    }
                }


                if(count($preset)){
                    $smarty->assign('set', $preset);
                }

                if (!$inUser->id){
                    $smarty->assign('captcha', $inPage->getCaptcha());
                }
            } elseif($check['status'] == 2) {

                $id = $model->insertAd($check['setters']);

                if($id){
                    $inCore->redirect(sprintf($routes['cabinet']['photomanager'], $id));
                } else {
                    // TODO: fix it, во время добавления что то пошло не так
                    echo 'error';
                }

            } else {

                $smarty = $inCore->initSmarty(COMPONENT_TEMPLATE_PATH, COMPONENT_TEMPLATE_CONTAINER);
                $smarty->assign($reqAssign);
                $smarty->assign($data);
                $smarty->assign(array(
                    'check' => $check,
                    'set'   => $check['setters'],
                    'errors' => $check['errors']
                ));

                if (!$inUser->id){
                    $smarty->assign('captcha', $inPage->getCaptcha());
                }
            }

        } break;
        // endregion
        // region controller search
        case 'search': {
            $inPage->addPathway('Поиск авто');
            $inPage->setTitle('Поиск автомобиля');

            $initGallery($inPage);

            $smarty = $inCore->initSmarty(COMPONENT_TEMPLATE_PATH, COMPONENT_TEMPLATE_CONTAINER);
            $smarty->assign($reqAssign);

            $smarty->assign(array(
                'carousel'       => $cfg['ad']['carousel']['active'] ? $model->getCarousel() : null,
                'ads'    => array(
                    'data'     => $model->getAdsList($q = $model->getSearchFilterParamsByQuery(), $cfg['ad']['list']['search']['per_page']),
                    'ordering' => true
                ),
                'filter' => $q
            ));

        } break;
        // endregion
        // region controller spares
        case 'spares': {
            if(!$cfg['system']['spares']['active']){
                cmsCore::error404();
            }

            $inPage->addPathway('Автозапчасти');
            $inPage->setTitle('Автозапчасти');
            $inPage->setKeywords('Покупка и продажа б/у и новых запчастей: шины, диски, аудио, видео, автохимия, аксессуары, тюнинг, автозапчасти');

            $smarty = $inCore->initSmarty(COMPONENT_TEMPLATE_PATH, COMPONENT_TEMPLATE_CONTAINER);
            $smarty->assign($reqAssign);

        } break;
        // endregion
    }

    $inPage->addHead('<!-- end region -->');


    // region display smarty
    if(isset($smarty)){
        $smarty->display(COMPONENT_TEMPLATE_CONTAINER);

        if($cfg['system']['debug']){
            $model->showQueryStack();
        }
    }
    // endregion

}
?>