<?php


return array(
  'component_data' => array(
      'wheel' => array(
          '1' => $_LANG['COMPONENT_DATA_WHEEL_LEFT'],
          '2' => $_LANG['COMPONENT_DATA_WHEEL_RIGHT'],
      ),
      'fueltype' => array(
          '1' => $_LANG['COMPONENT_DATA_FUELTYPE_PETROL'],
          '2' => $_LANG['COMPONENT_DATA_FUELTYPE_DIESEL'],
          '3' => $_LANG['COMPONENT_DATA_FUELTYPE_GAS'],
          '4' => $_LANG['COMPONENT_DATA_FUELTYPE_HYBRID'],
          '5' => $_LANG['COMPONENT_DATA_FUELTYPE_ELECTRO'],
      ),
      'transmission' => array(
          '1' => $_LANG['COMPONENT_DATA_TRANSMISSION_AUTO'],
          '2' => $_LANG['COMPONENT_DATA_TRANSMISSION_MANUAL']
      ),
      'drive' => array(
          '1' => $_LANG['COMPONENT_DATA_DRIVE_4WD'],
          '2' => $_LANG['COMPONENT_DATA_DRIVE_FRONT'],
          '3' => $_LANG['COMPONENT_DATA_DRIVE_REAR']
      ),
      'status' => array(
          '1' => $_LANG['COMPONENT_DATA_STATUS_IN_STOCK'],
          '2' => $_LANG['COMPONENT_DATA_STATUS_IN_TRANSIT'],
          '3' => $_LANG['COMPONENT_DATA_STATUS_UNDER_THE_ORDER'],
      ),
      'currency' => array(
          1 => $_LANG['COMPONENT_DATA_CURRENCY_RUB'],
          2 => $_LANG['COMPONENT_DATA_CURRENCY_USD'],
          3 => $_LANG['COMPONENT_DATA_CURRENCY_EUR'],
      )
  ),
  'routes' => array(
      'ch_geo'     => COMPONENT_HOME_URL . '/api/ch_geo',
      // region add routes
      'index'          => COMPONENT_HOME_URL,
      'add'            => COMPONENT_HOME_URL . '/add/',
      'addNewAuto'     => COMPONENT_HOME_URL . '/add/auto',
      // endregion

      // region show
      'show_question'  => COMPONENT_HOME_URL . '/show/%d/#question-%d',
      'show'           => COMPONENT_HOME_URL . '/show/%d',
      'show_html'      => COMPONENT_HOME_URL . '/%d.html',
      'show_full'      => COMPONENT_HOME_URL . '/%s/%s/%d.html',
      'show_full_test' => COMPONENT_HOME_URL . '/%s_%s_%d.html',

      'showVendor'     => COMPONENT_HOME_URL . '/%s',
      'showVendorList' => COMPONENT_HOME_URL . '/%s/list',
      'showModel'      => COMPONENT_HOME_URL . '/%s/%s',
      // end region

      // region lists
      'vendorList'     => COMPONENT_HOME_URL . '/%s/list',
      //        'modelList'      => COMPONENT_HOME_URL . '/%s/%s/list',
      // endregion

      'search'         => COMPONENT_HOME_URL . '/search',
      'searchNew'      => COMPONENT_HOME_URL . '/search?new=1',

      'cabinet'        => array(
          'index'        => COMPONENT_HOME_URL . '/cabinet',
          'my_questions' => COMPONENT_HOME_URL . '/cabinet/my_questions',
          'adslist'      => COMPONENT_HOME_URL . '/cabinet/ads',
          'edit'         => COMPONENT_HOME_URL . '/cabinet/ads/edit/%s',
          'questions'    => COMPONENT_HOME_URL . '/cabinet/ads/edit/%s/questions',
          'photomanager' => COMPONENT_HOME_URL . '/cabinet/ads/edit/%s/pm'
      )

  )
);