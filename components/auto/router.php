<?php

    function routes_auto(){
        //region api
        $routes[] = array(
            '_uri'  => '/^auto\/api\/(services)$/i',
            'do'    => 'api',
            1    => 'action'
        );


        // region api geo
        $routes[] = array(
            '_uri'  => '/^auto\/api\/(ch_geo)$/i',
            'do'    => 'api',
            1       => 'action'
        );

        $routes[] = array(
            '_uri'  => '/^auto\/api\/(geo)\/(getcities|getregions)\/([0-9]+)$/i',
            'do'    => 'api',
            1    => 'action',
            2    => 'method',
            3    => 'id'
        );
        // endregion
        // region api vendors
        $routes[] = array(
            '_uri'  => '/^auto\/api\/(vendors)\/(getmodels)\/([0-9]+)$/i',
            'do'    => 'api',
            1    => 'action',
            2    => 'method',
            3    => 'id'
        );
        // endregion
        // region api photo
        $routes[] = array(
            '_uri'  => '/^auto\/api\/(photo)\/(add|remove|setmain)\/([0-9]+)$/i',
            'do'    => 'api',
            1    => 'action',
            2    => 'method',
            3    => 'id'
        );
        // endregion
        // region api ad
        $routes[] = array(
            '_uri'  => '/^auto\/api\/(ad)\/([0-9]+)\/(showphone|sold)$/i',
            'do'    => 'api',
            1    => 'action',
            2    => 'id',
            3    => 'method',
        );
        // endregion
        //api methods

        $routes[] = array(
            '_uri'  => '/^auto\/api\/(ad)\/(sell)\/([0-9]+)$/i',
            'do'    => 'api',
            1    => 'action',
            2    => 'method',
            3    => 'id'
        );

        $routes[] = array(
            '_uri'  => '/^auto\/api\/(ad)\/(question|answer)\/(add|delete)\/([0-9]+)$/i',
            'do'    => 'api',
            1    => 'action',
            2    => 'method',
            3    => 'smethod',
            4    => 'id'
        );

        // region add
        $routes[] = array(
            '_uri'  => '/^auto\/add$/i',
            'do'    => 'add'
        );
        // endregion

        // region search
        $routes[] = array(
            '_uri'  => '/^auto\/search$/i',
            'do'    => 'search'
        );
        // endregion

        // region show
        $routes[] = array(
            '_uri'  => '/^auto\/([0-9]+).html$/i',
            'do'    => 'show',
            1       => 'id'
        );

        $routes[] = array(
            '_uri'  => '/^auto\/show\/([0-9]+)$/i',
            'do'    => 'show',
            1       => 'id'
        );

        $routes[] = array(
            '_uri'  => '/^auto\/([a-zA-Z-_]*)_([a-zA-Z-_0-9]*)_([0-9]+).html$/i',
            'do'    => 'show',
            1       => 'vendor',
            2       => 'model',
            3       => 'id'
        );

        $routes[] = array(
            '_uri'  => '/^auto\/([a-zA-Z-_0-9]*)\/([a-zA-Z-_0-9]*)\/([0-9]+).html$/i',
            'do'    => 'show',
            1       => 'vendor',
            2       => 'model',
            3       => 'id'
        );
        // endregion

        // region cabinet
        $routes[] = array(
            '_uri'  => '/^auto\/cabinet$/i',
            'do'    => 'cabinet'
        );

        $routes[] = array(
            '_uri'  => '/^auto\/cabinet\/(my_questions)$/i',
            'do'    => 'cabinet',
            1       => 'action'
        );

        $routes[] = array(
            '_uri'  => '/^auto\/cabinet\/(ads)$/i',
            'do'    => 'cabinet',
            1       => 'action'
        );

        $routes[] = array(
            '_uri'  => '/^auto\/cabinet\/(ads)\/(edit)\/([0-9]*)$/i',
            'do'    => 'cabinet',
            1    => 'action',
            2    => 'subaction',
            3    => 'id'
        );

        $routes[] = array(
            '_uri'  => '/^auto\/cabinet\/(ads)\/(edit)\/([0-9]*)\/(pm|questions)$/i',
            'do'    => 'cabinet',
            1    => 'action',
            2    => 'subaction',
            3    => 'id',
            4    => 'type'
        );
        // endregion

        // region spares
        $routes[] = array(
            '_uri'  => '/^auto\/spares$/i',
            'do'    => 'spares'
        );
        // endregion

        /* Маршруты с вендорами и моделями должны быть в самом низу, т.к побеждает маршрут который первнее */

        // region vendor
        $routes[] = array(
            '_uri'  => '/^auto\/([a-zA-Z-_0-9]*)$/i',
            'do'    => 'showVendor',
            1       => 'vendor'
        );

        $routes[] = array(
            '_uri'  => '/^auto\/([a-zA-Z-_0-9]*)\/list$/i',
            'do'    => 'showVendorList',
            1       => 'vendor'
        );
        //endregion

        // region model
        $routes[] = array(
            '_uri'  => '/^auto\/([a-zA-Z-_0-9]*)\/([a-zA-Z-_0-9]*)$/i',
            'do'    => 'showModelList',
            1    => 'vendor',
            2    => 'model'
        );

        // endregion

        return $routes;

    }

?>