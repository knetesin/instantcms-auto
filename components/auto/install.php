<?php

    function info_component_auto(){
        $_component['title']        = 'Автодоска';
        $_component['description']  = 'Компонент автообъявлений';
        $_component['link']         = 'auto';
        $_component['author']       = 'Netesin Kirill';
        $_component['internal']     = '0';
        $_component['version']      = '1';


        $_component['config']['system'] = array(
            'debug'            => 0,
            'groupRusVendors'  => 0,
            'upload_dir'       => '/upload/' . $_component['link'] . '/',
            'date_start'       => 1940,
            'moderation_type'  => 0,
            'showlogocats'     => 1,
            'showNullCats'     => 0,
            'countColumnMain'  => 4,
            'countColumnOther' => 4,
            'spares' => array(
                'active' => 0
            ),
        );

        $_component['config']['ad'] = array(
            'max_photo'   => 20,
            'limit_views' => 0,
            'show_type'   => 'show_full',
            'carousel' => array(
                'active' => 1,
                'min'    => 5,
                'mode'   => 2
            ),
            'list'        =>
                array(
                    'search'     =>
                        array(
                            'per_page' => 25,
                        ),
                    'sub_vendor' =>
                        array(
                            'per_page' => 5,
                        ),
                    'cabinet'    =>
                        array(
                            'per_page' => 5,
                        ),
                ),
            'currency'    =>
                array(
                    'min' => 5000,
                    'max' => 60000000,
                ),
            'add'         =>
                array(
                    'autoreg'         => 1,
                    'can_edit_vendor' => 0,
                    'can_edit_geo'    => 0,
                ),
            'new'         =>
                array(
                    'vip'        =>
                        array(
                            'default' => 1,
                            'time'    => 3600,
                        ),
                    'background' =>
                        array(
                            'default' => 1,
                            'time'    => 3600,
                            'color'   => '#fffdb8',
                        ),
                ),
        );
        $_component['config']['images'] = array(
            'configs' =>
                array(
                    'quality'       => 90,
                    'save_original' => 1,
                ),
            'ad'      =>
                array(
                    0 =>
                        array(
                            'name'   => 'large',
                            'weight' => 1280,
                            'height' => 0,
                            'thumb'  => 0,
                            'watermark' => 1,
                            'sys'    => 1,
                        ),
                    1 =>
                        array(
                            'name'   => 'small',
                            'weight' => 132,
                            'height' => 94,
                            'thumb'  => 1,
                            'watermark' => 0,
                            'sys'    => 1,
                        ),
                    2 =>
                        array(
                            'name'   => 'preview',
                            'weight' => 355,
                            'height' => 0,
                            'thumb'  => 0,
                            'watermark' => 0,
                            'sys'    => 1,
                        ),
                    3 =>
                        array(
                            'name'   => 'module',
                            'weight' => 65,
                            'height' => 65,
                            'thumb'  => 0,
                            'watermark' => 0,
                            'sys'    => 0,
                        ),
                    4 =>
                        array(
                            'name'   => 'carousel',
                            'weight' => 184,
                            'height' => 140,
                            'thumb'  => 0,
                            'watermark' => 0,
                            'sys'    => 0,
                        ),
                ),
        );

        $_component['config']['geo'] = array(
            'country' => 3159,
            'region' => array(
                'preset'  => 0,
                'default' => 0
            ),
            'city'   => array(
                'preset'  => 0,
                'default' => 0
            ),
        );

        $_component['config']['services'] = array(
            'vip' => array(
                'cost' => 100,
                'duration' => 14400
            ),
            'attach' => array(
                'cost' => 100,
                'duration' => 14400
            ),

            'up' => array(
                'cost' => 50
            ),
            'up_bg' => array(
                'cost' => 100,
                'duration' => 2592000,
                'color' => '#cde9fa'
            )
        );

        $_component['config']['currency'] = array(
            '1:1' => 1,
            '1:2' => '0.028071042193584',
            '1:3' => '0.020202265077961',
            '2:1' => 35.6239,
            '2:2' => 1,
            '2:3' => '0.71968347091076',
            '3:1' => 49.4994,
            '3:2' => '1.3894997459571',
            '3:3' => 1,
            'upd' => '2014-04-13 22:59:43',
        );


        return $_component;

    }

    function install_component_auto(){

        $inCore     = cmsCore::getInstance();
        $inDB       = cmsDatabase::getInstance();
        $inConf     = cmsConfig::getInstance();

        cmsActions::registerAction('auto', array(
           'name'    => 'add_auto',
           'title'   => 'Добавление объявления о продаже авто',
           'message' => 'добавляет объявление о продаже авто %s'
        ));

        include($_SERVER['DOCUMENT_ROOT'].'/includes/dbimport.inc.php');

        dbRunSQL($_SERVER['DOCUMENT_ROOT'].'/components/auto/sql/install.sql', $inConf->db_prefix);

        dbRunSQL($_SERVER['DOCUMENT_ROOT'].'/components/auto/sql/cats.sql', $inConf->db_prefix);
        dbRunSQL($_SERVER['DOCUMENT_ROOT'].'/components/auto/sql/vendors.sql', $inConf->db_prefix);
        dbRunSQL($_SERVER['DOCUMENT_ROOT'].'/components/auto/sql/models.sql', $inConf->db_prefix);


        if ($inCore->isComponentInstalled('billing')){

            $inCore->loadClass('billing');

            cmsBilling::registerAction('auto', array(
                    'name'  => 'up_ad',
                    'title' => 'Поднять объявление'
                )
            );

            cmsBilling::registerAction('auto', array(
                    'name'  => 'set_premium_ad',
                    'title' => 'Спецразмещение'
                )
            );

            cmsBilling::registerAction('auto', array(
                    'name'  => 'attach_ad',
                    'title' => 'Прикрепить объявление'
                )
            );

            cmsBilling::registerAction('auto', array(
                    'name'  => 'up_bg_ad',
                    'title' => 'Поднять и выделить объявление цветом'
                )
            );

        }



        return true;
    }


    function upgrade_component_auto(){

		return true;

	}
?>