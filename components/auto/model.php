<?php
/* * * * * * * * * * * * * * * * * * *
 * Created by Netesin Kirill         *
 * e-mail: k.netesin@gmail.com   *
 * * * * * * * * * * * * * * * * * * */
if(!defined('VALID_CMS')) { die('ACCESS DENIED'); }

class cms_model_auto {

    private $db_prefix;

    public $module_id;

    private $_debugStack = array();

    /**
     * @var cmsDatabase|null
     */
    private $inDB = null;

    private $configs = array();
    /**
     * @var $geo cms_model_geo
     */
    public $geo;

    private $_lang = array();

    static $_table = array(
        'vendor'    => 'auto_vendors',
        'model'     => 'auto_models',
        'questions' => 'auto_questions',
        'answers'   => 'auto_answers',
        'cats'      => 'auto_cats',
        'currency'  => 'auto_currency',
        'unit'      => 'auto_units',
        'photo'     => 'auto_photos',
    );

    static $_component_name = 'auto';

    private $_component_data = array();

    public $_currency = 1;

    private $_routes = array();

    private $geo_selector = array(
        'country' => null,
        'region'  => null,
        'city'    => null
    );

    function __construct($component_data, $routes, $lang = null)
    {
        setlocale(LC_ALL, NULL);

        $this->inCore = cmsCore::getInstance();
        $this->inDB = cmsDatabase::getInstance();

        $configs = $this->inCore->loadComponentConfig(self::$_component_name);
        $this->configs = $configs;

        $this->_lang = $lang;
        $this->_component_data = $component_data;
        $this->_routes = $routes;

        // region currency
        $currency = (int)cmsCore::getCookie('currency');
        $currency = $currency > 0 ? $currency : 1;
        $this->_currency = $currency;
        // endregion currency

        $this->_currencys = $configs['currency'];

        if($this->inCore->isComponentInstalled('geo')){
            $this->inCore->loadModel('geo');
            $this->geo = new cms_model_geo();
        } else {
            cmsCore::halt('geo component is missing!');
        }

        $inConf = cmsConfig::getInstance();
        $this->db_prefix = $inConf->db_prefix . '_';

        $timeP = date('P');
        $this->inDB->query($q = "SET time_zone = '{$timeP}'");
        $this->addInQueryStack($q, $this->inDB->error());

        $this->geo_selector = unserialize(cmsCore::getCookie('geo_selector'));

        if(!$this->geo_selector['country']){
            $this->geo_selector['country'] = (int)$this->configs['geo']['country'];
        }

        if($region = $this->configs['geo']['region']['default']){
            $this->geo_selector['region'] = (int)$region;
        }

        if($city = $this->configs['geo']['city']['default']){
            $this->geo_selector['city'] = (int)$city;
        }
    }

    /* ==================================================================================================== */
    /* ==================================================================================================== */

    public function install()
    {
        return true;
    }


    public function setGeo($region = null, $city = null)
    {
        $this->geo_selector['country'] = (int)$this->configs['geo']['country'];

        if($region){
            $region = $this->geo->getRegion($region);
        }

        if($city){
            $city = $this->geo->getCity($city);
        }

        if($region){
            $this->geo_selector['region'] = ($this->geo_selector['country'] == $region['country_id']) ? (int)$region['id'] : null;
        } else {
            $this->geo_selector['region'] = null;
        }

        if($city){
            $this->geo_selector['city'] = ($this->geo_selector['region'] == $city['region_id']) ? (int)$city['id'] : null;
        } else {
            $this->geo_selector['city'] = null;
        }

        cmsCore::setCookie('geo_selector', serialize($this->geo_selector), time() + ((60*60)*24)*14);

    }

    public function getGeo_selector()
    {
        return $this->geo_selector;
    }

    // region new model
    static function parseCurrency()
    {
        setlocale(LC_ALL, NULL);

        $curs = array(); // массив с данными

        if(!$xml = simplexml_load_file("http://www.cbr.ru/scripts/XML_daily.asp")) die('Ошибка загрузки XML');

        foreach($xml->Valute as $m){ // перебор всех значений
            if($m->CharCode == "USD" || $m->CharCode == "EUR"){
                $curs[(string)$m->CharCode] = str_replace(",", ".", (string)$m->Value);
            }
        }

        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');

        $data = array(
            '1:1' => 1,
            '1:2' => 1 / $curs['USD'],
            '1:3' => 1 / $curs['EUR'],
            '2:1' => $curs['USD'],
            '2:2' => 1,
            '2:3' => $curs['USD'] / $curs['EUR'],
            '3:1' => $curs['EUR'],
            '3:2' => $curs['EUR'] / $curs['USD'],
            '3:3' => 1,
            'upd' => $date
        );

        $core = cmsCore::getInstance();
        $configs = $core->loadComponentConfig(self::$_component_name);
        $configs['currency'] = $data;

        $core->saveComponentConfig(self::$_component_name, $configs);

        return 1;
    }

    // region api methods
    public function apiGetCitiesForRegion($region)
    {
        if(!$region){
            return '<option value="0">выберите регион</option>';
        }

        $html = array();
        foreach($this->geo->getCities($region) as $value => $region){
            $html[] = "<option value='{$value}'>{$region}</option>";
        }

        return implode('', $html);
    }

    public function apiGetRegionsForCountry($country)
    {
        if(!$country){
            return '<option value="0">выберите страну</option>';
        }

        $html = array();
        foreach($this->geo->getRegions($country) as $value => $region){
            $html[] = "<option value='{$value}'>{$region}</option>";
        }

        return implode('', $html);
    }

    public function apiGetModelsForVendor($vendor)
    {
//        $isaddcall = (preg_match('/' . self::$_component_name . '\/add/', $_SERVER['HTTP_REFERER']) or preg_match('link=' . self::$_component_name . '&opt=add&sub_opt=ad', $_SERVER['HTTP_REFERER']) or preg_match('/' . self::$_component_name . '\/cabinet\/ads\/edit/', $_SERVER['HTTP_REFERER']));

        $isaddcall = (cmsCore::request('is_add')) ? 1 : 0;

        $data = $this->getModelsByVendor($vendor, !$isaddcall);

        $html[] = ($isaddcall) ? '<option value="0">выберите модель</option> ': '<option value="0">любая модель</option>';

        foreach($data as $index => $model){
            $html[] = "<option value='{$model['id']}'>{$model['title']}</option>";

            if ($model['popular'] and isset($data[$index + 1]) and !$data[$index + 1]['popular']){
                $html[] = '<optgroup class="select_opt" label="-------------"></optgroup>';
            }
        }

        return implode('', $html);
    }

    public function apiAdSetSell($adId)
    {
        $this->setAdStatus($adId);

        return json_encode(array(
            'status' => 'ok'
        ));
    }

    public function apiGetPhoneByAd($adData)
    {
        if (!$adData){
            return 'ошибка, обратитесь в техподдержку';
        }


        if (!$adData['show']){
            return 'объявление отключено от показа';
        }

//        $hasAccess =

        if (!$adData['auto_status']){
            return 'машина продана, контактные данные закрыты';
        }


        $html = "<span>{$adData['phone1']}</span>";

        if($adData['phone2'] ) {
            $html.= "<div>{$adData['phone2']}</div>";
        }

        return $html;
    }

    public function apiInsertQuestion($adId, $data)
    {
        $error = array();

        // region author
        if(trim($data['author'])){
            if(strlen($data['author']) > 20){
                $error[] = array(
                    'selector' => 'input[name="author"]',
                    'title'    => 'Слишком длинное имя автора'
                );
            }
        } else {
            $error[] = array(
                'selector' => 'input[name="author"]',
                'title'    => 'Укажите имя'
            );
        }
        // endregion

        // region message
        if(trim($data['message'])){
            if(strlen($data['message']) > 500){
                $error[] = array(
                    'selector' => 'textarea[name="message"]',
                    'title'    => 'Текст вопроса слишком длинный'
                );
            }
        } else {
            $error[] = array(
                'selector' => 'textarea[name="message"]',
                'title'    => 'Введите текст вопроса'
            );
        }
        // endregion

        // region phone
        if(trim($data['phone'])){
            if(strlen($data['phone']) > 40){
                $error[] = array(
                    'selector' => 'input[name="phone"]',
                    'title'    => 'Запись телефона должна быть менее 40 символов'
                );
            }
        }
        // endregion

        // region email
        if(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
            $error[] = array(
                'selector' => 'input[name="email"]',
                'title'    => $this->_lang['TEMPLATE_ADD_FORM_ERROR_INCORRECT_EMAIL']
            );
        }
        // endregion email

        // region phone

        // endregion

        if(count($error)){
           return json_encode(array(
               'status' => 'error',
               'error'  => $error
           ));
        }

        $this->insertQuestion($adId, $data);

        return json_encode(array(
            'status' => 'ok'
        ));
    }

    public function apiInsertAnswer($questionId, $data)
    {
        $error = array();


        // region message
        if(trim($data['answer'])){
            if(strlen($data['answer']) > 500){
                $error[] = array(
                    'selector' => 'textarea[name="answer"]',
                    'title'    => 'Текст ответа слишком длинный'
                );
            }
        } else {
            $error[] = array(
                'selector' => 'textarea[name="answer"]',
                'title'    => 'Введите текст ответа'
            );
        }
        // endregion

        if(count($error)){
            return json_encode(array(
                'status' => 'error',
                'error'  => $error
            ));
        }

        $this->insertAnswer($questionId, $data);
        $message = 'На ваш вопрос был получен ответ ' . $_SERVER['HTTP_HOST'] . sprintf($this->_routes['show'], $data['id_unit']) . "#question-{$questionId}";
        cmsCore::mailText($data['email'], 'Instant-auto', sprintf($message, $questionId));

        return json_encode(array(
            'status' => 'ok'
        ));
    }

    public function apiServices($type, $adId, $data = array())
    {
        $now = time();
        $adData = $this->getAd($adId, false);
        switch($type){
            case 'up': {
                $this->dbUpdate($adId, self::$_table['unit'], "`ordering` = '{$now}'");
            } break;
            case 'premium': {
                $vip_date_expiration = date('Y-m-d H:i:s', $now + $this->configs['services']['vip']['duration']);
                $this->dbUpdate($adId, self::$_table['unit'], "`is_vip` = '1', `vip_date_expiration` = '{$vip_date_expiration}'");
            } break;
            case 'attach': {
                $ordering = $now + $this->configs['services']['attach']['duration'];
                $ordering = $data['ordering'] > $ordering ? $data['ordering'] : $ordering;

                $this->dbUpdate($adId, self::$_table['unit'], "`ordering` = '{$ordering}'");
            } break;
            case 'up_bg': {
                $now = ($adData['ordering'] > $now) ? $adData['ordering'] : $now;

                $background_date_expiration = date('Y-m-d H:i:s', $now + $this->configs['services']['up_bg']['duration']);
                $background_color = @isset($this->configs['services']['up_bg']['color']) ? $this->configs['services']['up_bg']['color'] : '';

                $this->dbUpdate($adId, self::$_table['unit'], "`ordering` = '{$now}', `background_date_expiration` = '{$background_date_expiration}', `background_color` = '{$background_color}'");
            } break;
        }

        return array(
            'status' => 'ok',
            'type'   => $type
        );
    }

    // endregion

    public function pServices($type, $adData, $serviceData = array())
    {
        $now = time();

        switch($type){
            case 'up': {
                $this->dbUpdate($adData['id'], self::$_table['unit'], "`ordering` = '{$now}'");
            } break;
            case 'premium': {
                $vip_date_expiration = date('Y-m-d H:i:s', $now + $serviceData['duration']);
                $this->dbUpdate($adData['id'], self::$_table['unit'], "`ordering` = '{$now}', `is_vip` = '1', `vip_date_expiration` = '{$vip_date_expiration}'");
            } break;
            case 'attach': {
                $ordering = $now + $serviceData['duration'];
                $this->dbUpdate($adData['id'], self::$_table['unit'], "`ordering` = '{$ordering}'");
            } break;
            case 'up_bg': {
                $background_date_expiration = date('Y-m-d H:i:s', $now + $serviceData['duration']);
                $background_color = $serviceData['color'];

                $this->dbUpdate($adData['id'], self::$_table['unit'], "`ordering` = '{$now}', `background_date_expiration` = '{$background_date_expiration}', `background_color` = '{$background_color}'");
            } break;
        }
    }

    // region photo
    public function getPhotoCount($adId)
    {
        return $this->dbGetCount('photo', $adId);
    }

    public function addPhoto($adId, $curCount = -1)
    {
        $files = new UploadedFiles();
        $files = $files->getFilesList();

        $photoUploader = new PhotoUploader();

        $photoUploader->setImages($this->configs['images']['ad'])
            ->setUploadDir($this->configs['system']['upload_dir'] . "ads/{$adId}/")
            ->setSaveOriginal($this->configs['images']['configs']['save_original'])
            ->setQuality($this->configs['images']['configs']['quality']);


        $data = array();

        if($curCount == -1){
            $curCount = $this->getPhotoCount($adId);
        }

        if($curCount >= $this->configs['ad']['max_photo']){
            return false;
        }

        foreach($files as $file){
            if(!in_array($extType = exif_imagetype($file["tmp_name"]), array(1,2,3,5,6,7,8,9))){
                $data[] = array(
                    'status'  => 0,
                    'codeMsg' => "Расширение файла " . image_type_to_extension($extType) . " не является допустимым"
                );

                return $data;
            }

            if($filePaths = $photoUploader->upload($file['tmp_name'])){
                $isMain = ($curCount > 0) ? 1 : 0;
                $curCount++;

                $table = $this->db_prefix . self::$_table['photo'];

                $sql = "INSERT INTO `{$table}` VALUES (0, '{$adId}', '{$filePaths['filename']}', '{$isMain}', '')";
                $this->inDB->query($sql);

                $filePaths['status'] = 1;
                $filePaths['id'] = $this->inDB->get_last_id($table);

                $data[] = $filePaths;
            } else {

                switch($file['error']){
                    case 1: $codeMsg = 'Превышен максимально допустимый размер файла'; break;
                    case 2: $codeMsg = 'Превышен максимально установленный размер файла'; break;
                    case 3: $codeMsg = ''; break;
                    case 4: $codeMsg = ''; break;
                    case 5: $codeMsg = ''; break;
                    case 6: $codeMsg = 'Нет директории для записи'; break;
                    case 7: $codeMsg = 'Ошибка записи на диск'; break;
                    default: $codeMsg = 'Загружаемый файл не является корректным изображением'; break;
                }

                $data[] = array(
                    'status'  => 0,
                    'codeMsg' => $codeMsg
                );
            }

        }

        return $data;
    }

    public function getPhotoList($adId, $withData = true)
    {
        $data = $this->dbGetList(self::$_table['photo'], null, array(
            'id_unit' => $adId
        ), 'by `ordering` ASC, id');

        $list = array();

        $sizes  = array();

        foreach($this->configs['images']['ad'] as $image){
            $sizes[] = $image['name'];
        }

        foreach($data as $photo){
            $tempItem = array();
            foreach($sizes as $size){
                $tempItem[$size] = sprintf('%s%s/%s/%s/%s', $this->configs['system']['upload_dir'], 'ads', $adId, $size, $photo['file']);
            }

            if($withData){
                $tempItem['data'] = $photo;
            }

            $list[] = $tempItem;
        }

        return $list;
    }

    public function getPhoto($pId)
    {
        return $this->dbGetOneById($pId, self::$_table['photo']);
    }

    public function removePhoto($pId)
    {
        if(!$photo = $this->getPhoto($pId)){
            return false;
        }

        foreach($this->configs['images']['ad'] as $photoSize){
            @unlink(sprintf('%s%s/%s/%s/%s', PATH, $this->configs['system']['upload_dir'], 'ads', $photo['id_unit'], $photoSize, $photo['file']));
        }

        $table = $this->db_prefix . self::$_table['photo'];

        $sql = "DELETE FROM `{$table}`
                WHERE `id` = '{$pId}'";
        $this->inDB->query($sql);

        return array(
            'id' => $pId
        );
    }

    public function setMainPhoto($adId, $pId)
    {
        $ad = $this->getAd($adId, false);

        if(!$ad){
            return false;
        }

        $table = $this->db_prefix . self::$_table['photo'];

        $this->inDB->query("UPDATE `{$table}` SET `ordering` = 1 WHERE `id_unit` = {$adId}");
        $this->inDB->query("UPDATE `{$table}` SET `ordering` = 0 WHERE `id_unit` = {$adId} AND `id` = {$pId}");

        return $pId;
    }
    // endregion

    // region ad
    public function formatPresetAdtoEdit($adData)
    {

        return array(
            'created_at' => $adData['created_at'],
            'edited_at'  => $adData['edited_at'],
            'vendor' => $this->getVendor($adData['id_vendor']),
            'models' => $this->getModelsByVendor($adData['id_vendor'], false),
            'model'  => $this->getModel($adData['id_model']),
            'geo' => array(
                'region' => $this->geo->getRegion($adData['region_id']),
                'regions' => $this->geo->getRegions($this->configs['geo']['country']),
                'city' => $this->geo->getCity($adData['city_id']),
                'cities' => $this->geo->getCities($adData['region_id'])
            ),
            'date_constr' => $adData['date_construct'],
            'wheel' => $adData['wheel'],

            'is_new_auto'  => $adData['is_new_auto'],
            'mileage'    => $adData['mileage'],
            'is_without_rus_run'  => $adData['is_without_rus_run'],

            'volume' => $adData['volume'],
            'fueltype' => $adData['fueltype'],
            'transmission' => $adData['transmission'],
            'drive' => $adData['drive'],

            'is_undocumented'    => $adData['is_undocumented'],
            'is_broken'    => $adData['is_broken'],
            'is_cut'       => $adData['is_cut'],

            'description'    => $adData['description'],
            'is_gt'    => $adData['is_gt'],
            'gt_description'    => $adData['gt_description'],

            'price'       => $adData['price'],
            'currency'    => $adData['currency'],
            'status'      => $adData['auto_status'],

            'phone1'    => $adData['phone1'],
            'phone2'    => $adData['phone2'],
            'email'     => $adData['email'],
            'show'      => $adData['show'],
            'access_issues'  => $adData['access_issues'],
        );
    }

    public function setAdStatus($adId, $status = 0)
    {
        $this->dbUpdate($adId, self::$_table['unit'], "`auto_status` = '{$status}'");
    }

    public function getAd($id, $forShow = true)
    {
        $joins = null;
        $select = array(
            't1.*',
            'UNIX_TIMESTAMP(t1.created_at) as date_created_u',
            'UNIX_TIMESTAMP(t1.background_date_expiration) as p_bg_date_u',
            'UNIX_TIMESTAMP(t1.vip_date_expiration) as p_vip_date_u'
        );

//        if($this->configs['ad']['show_type'] == 'show_full'){
            $joins = array(
                "INNER JOIN %1\$s" . self::$_table['vendor'] . " as v ON v.id = t1.id_vendor",
                "INNER JOIN %1\$s" . self::$_table['model'] . " as m ON m.id = t1.id_model",
            );
            $select[] = 'v.title_url as url_vendor';
            $select[] = 'm.title_url as url_model';

//        }

        $select = implode(', ', $select);

        $data = $this->dbGetOneById($id, self::$_table['unit'], null, $select, $joins);

        if(!$data){
            return false;
        }

        if(!$forShow){
            return $data;
        }

        // fix for compab
        $fData = $data;
        $fData['data'] = $data;

        $photoList = $this->getPhotoList($id);

        $fData['data']['price_list'] = $this->getCurrencyList($data['price'], $data['currency']);
        $fData['data']['photo'] = $photoList;
        $fData['data']['hasPhoto'] = (count($photoList)) ? true : false;

        $fData['data']['questions'] = $this->getAdQa($data['id']);

        // region specformat fuel_volume
        if($data['fueltype']){
            $fData['data']['specf']['fl'][] = $this->_component_data['fueltype'][$data['fueltype']];
        }

        if($data['volume'] > 0){
            $fData['data']['specf']['fl'][] = $data['volume'];
        }
        // endregion specformat fuel_volume

        // region specformat special
        if($data['is_undocumented']){
            $fData['data']['specf']['special'][] = $this->_lang['TEMPLATE_ADD_CHECKBOX_IS_UNDOCUMENTED'];
        }

        if($data['is_cut']){
            $fData['data']['specf']['special'][] = $this->_lang['TEMPLATE_ADD_CHECKBOX_IS_CUT'];
        }
        if($data['is_broken']){
            $fData['data']['specf']['special'][] = $this->_lang['TEMPLATE_ADD_CHECKBOX_IS_BROKEN'];
        }
        // endregion specformat special

        $formatDuration = (function(DateIntervalEnhanced $time){
            $m = $time->m;
            $d = $time->d;
            $h = $time->h;
            $data = array();

            if($m){
                $data[] = $m . ' мес.';
            }

            if($d){
                $data[] = $d . ' дн.';
            }

            if($h){
                $data[] = $h . ' ч.';
            }

            return implode(', ', $data);
        });

        if(HAVE_COMPONENT_BILLING){
            $timeVip = new DateIntervalEnhanced('PT' . (int)$this->configs['services']['vip']['duration'] . 'S');
            $timeVip->recalculate();
            $timeAttach = new DateIntervalEnhanced('PT' . (int)$this->configs['services']['attach']['duration'] . 'S');
            $timeAttach->recalculate();
            $timeUpBg = new DateIntervalEnhanced('PT' . (int)$this->configs['services']['up_bg']['duration'] . 'S');
            $timeUpBg->recalculate();

            $fData['data']['services']['duration'] = array(
                'vip'    => $formatDuration($timeVip),
                'attach' => $formatDuration($timeAttach),
                'up_bg'  => $formatDuration($timeUpBg),
            );
        }

        return $fData;
    }

    public function getAdsList($where = null, $limit = 0, $pager = true, $use_geo = true)
    {

        $where = isset($where['data']) ? $where['data'] : $where;
        unset($where['vendors']);
        $select = array(
            't1.*',
            'UNIX_TIMESTAMP(t1.created_at) as date_created_u',
            'UNIX_TIMESTAMP(t1.background_date_expiration) as p_bg_date_u',
            'UNIX_TIMESTAMP(t1.vip_date_expiration) as p_vip_date_u',
            "FROM_UNIXTIME(t1.ordering) as date",
            't2.file as photo_name',
        );

        $q = $having = array();
        $user = cmsUser::getInstance();

        $addShowQ = $user->id ? " or (t1.show < 3 and t1.id_owner = '{$user->id}') " : '';
        $q[] = "(t1.show in(2, 3) {$addShowQ})";

        // region user
        if(isset($where['user']) and $where['user'] > 0){
            $q = array();
            $q[] = "id_owner = '{$where['user']}'";
        }
        // endregion

        if(isset($where['id_vendor'])){
            if($where['id_vendor'] <= 0){
                unset($where['id_vendor']);
                unset($where['id_model']);
            } else {
                $q[] = "id_vendor = '{$where['id_vendor']}'";
                if($where['id_model']){
                    $q[] = "id_model = '{$where['id_model']}'";
                }
            }
        }

        if(isset($where['in']) and count($where['in'])){
            $q[] = 't1.id in (' . implode(',', $where['in']) . ')';
        }

        if(isset($where['nin']) and count($where['nin'])){
            $q[] = 't1.id not in (' . implode(',', $where['nin']) . ')';
        }

        // region carcase
        if(isset($where['carcase']) and count($where['carcase'])){
            $q[] = "carcase_id in(" . implode(',', $where['carcase']) . ")";
        }
        // endregion carcase

        // region price
        if($where['minprice'] > 0){
            $currencys = $this->getCurrencyList($where['minprice'], $where['currency'] ? $where['currency']: $this->_currency);
            $having['minprice'] = "sql_price >= '{$currencys[$this->_currency]}'";
        }

        if($where['maxprice'] > 0 and $where['minprice'] <= $where['maxprice']){
            $currencys = $this->getCurrencyList($where['maxprice'], $where['currency'] ? $where['currency']: $this->_currency);
            $having['maxprice'] = "sql_price <= '{$currencys[$this->_currency]}'";
        }

        if($having['minprice'] or $having['maxprice']or $where['order'] == 'price'){
            $pSql = "SET @price1 := {$this->_currencys["{$this->_currency}:1"]}, @price2 := {$this->_currencys["{$this->_currency}:2"]},@price3 := {$this->_currencys["{$this->_currency}:3"]}; ";
            $this->inDB->query($pSql);
            $this->addInQueryStack($pSql, $this->inDB->error());
            $select[] = " (CASE t1.currency WHEN 1 THEN t1.price/@price1 WHEN 2 THEN t1.price/@price2 WHEN 3 THEN t1.price/@price3 END) AS sql_price";
        }

        // endregion price

        // region date
        if(in_array($where['date'], $this->getDateList())){
            $q[] = "date_construct >= '{$where['date']}'";
        }

        if(in_array($where['datemax'], $this->getDateList()) and $where['datemax'] >= $where['date']){
            $q[] = "date_construct <= '{$where['datemax']}'";
        }
        // endregion price

        // region volume
        if(isset($where['min_volume']) and $where['min_volume'] > 0){
            $q[] = "volume >= '{$where['min_volume']}'";
        }

        if(isset($where['max_volume']) and $where['max_volume'] > 0){
            $q[] = "volume <= '{$where['max_volume']}'";
        }
        // endregion volume

        // region fueltype
        if(isset($where['fueltype']) and $where['fueltype'] > 0){
            $q[] = "fueltype = '{$where['fueltype']}'";
        }
        // endregion fueltype

        // region transmission
        if(isset($where['transmission']) and $where['transmission'] > 0){
            $q[] = "transmission = '{$where['transmission']}'";
        }
        // endregion transmission

        // region wheel
        if(isset($where['wheel']) and count($where['wheel'])){
            $q[] = "wheel in(" . implode(',', $where['wheel']) . ")";
        }
        // endregion drive

        // region foreign
        if(isset($where['foreign']) and $where['foreign'] > 0){
            $q[] = "cats_vendor = 1";
        }
        // endregion foreign

        // region unsold
        if(isset($where['unsold']) and $where['unsold'] > 0){
            $q[] = "auto_status > 0";
        }
        // endregion drive

        // region is_no_rus_mil
        if(isset($where['is_no_rus_mil']) and $where['is_no_rus_mil'] > 0){
            $q[] = "is_without_rus_run = 1";
        }
        // endregion is_no_rus_mil

        // region is_gt
        if(isset($where['is_gt']) and $where['is_gt'] > 0){
            $q[] = "is_gt = 1";
        }
        // endregion is_gt

        // region is_new
        if(isset($where['is_new']) and $where['is_new'] > 0){
            $q[] = "is_new_auto = 1";
        }
        // endregion is_new

        // region is_cut
        if(isset($where['is_cut']) and $where['is_cut'] > 0){
            $q[] = "is_cut = 1";
        }
        // endregion is_cut

        // region drive
        if(isset($where['drive']) and $where['drive'] > 0){
            $q[] = "drive = '{$where['drive']}'";
        }
        // endregion drive

        // region with photo
        if($where['with_photo']){
            $joins[] = 'INNER JOIN %2$s' . self::$_table['photo'] . ' as t2 on t1.id = t2.id_unit and t2.ordering = 0';
        } else {
            $joins[] = 'LEFT OUTER JOIN %2$s' . self::$_table['photo'] . ' as t2 on t1.id = t2.id_unit and t2.ordering = 0';
        }
        // endregion with photo

        // region docs
        if(isset($where['docs']) and $where['docs'] > 0){
            switch($where['docs']){
                case 1: $q[] = 'is_undocumented = 0';  break;
                case 2: $q[] = 'is_undocumented = 1';  break;
            }
        }
        // endregion docs

        // region is_broken
        if(isset($where['damaged']) and $where['damaged'] > 0){
            switch($where['damaged']){
                case 1: $q[] = 'is_broken = 1';  break;
                case 2: $q[] = 'is_broken = 0';  break;
            }
        }
        // endregion is_broken

        if($use_geo){
            if($this->geo_selector['region']){
                $q[] = "region_id = '{$this->geo_selector['region']}'";
            }

            if($this->geo_selector['city']){
                $q[] = "city_id = '{$this->geo_selector['city']}'";
            }
        }


        $page = (cmsCore::request('page', 'int', 1));
        if($limit){
            $offset = ($page-1) * $limit;
        }

        $limitStr = $limit > 0 ? "LIMIT {$offset}, {$limit}" : '';

        $direction = in_array(strtolower($where['direction']), array('asc', 'desc')) ? $where['direction'] : 'DESC';

        if(isset($where['for_cabinet']) and $where['for_cabinet']){

            $select[] = '(SELECT COUNT(*) FROM %1$s' . self::$_table['questions'] . ' as tq WHERE t1.id = tq.id_unit) as count_q';
            $select[] = '(SELECT COUNT(*) FROM %1$s' . self::$_table['questions'] . ' as tq WHERE t1.id = tq.id_unit AND tq.is_new = 1) as count_q_new';

            $where['order'] = in_array($where['order'], array('id', 'date', 'model', 'views', 'status')) ? $where['order'] : 'id';

            switch($where['order']){
                case 'id': $order = "id {$direction}"; break;
                case 'date': $order = "created_at {$direction}"; break;
                case 'model': $order = "title_vendor {$direction}, title_model DESC"; break;
                case 'views': $order = "count_views {$direction}"; break;
                case 'status': $order = "`show` {$direction}"; break;
                default: $order = "id {$direction}"; break;
            }
        } elseif(isset($where['from_carousel'])){
            switch($this->configs['ad']['carousel']['mode']){
                case 1: {
                    if(isset($where['carousel_configs']['vendor'])){
                        $order = "id_vendor = {$where['carousel_configs']['vendor']['id']} DESC";
                    } elseif($where['carousel_configs']['model']){
                        $order = "id_model = '{$where['carousel_configs']['model']['id']}' ,id_vendor = '{$where['carousel_configs']['vendor']['id']}'' DESC";
                    } else {
                        $order = "ordering {$direction}";
                    }
                } break;
                default: {
                if(isset($where['carousel_configs']['vendor'])){
                    $q[] = "id_vendor = {$where['carousel_configs']['vendor']['id']}";
                } elseif($where['carousel_configs']['model']){
                    $q[] = "id_model = '{$where['carousel_configs']['model']['id']}'";
                    $q[] = "id_vendor = '{$where['carousel_configs']['vendor']['id']}'";
                }

                $order = "ordering DESC";
                } break;
            }

        } else {
            $where['order'] = in_array($where['order'], array('date', 'year', 'price', 'mileage', 'model')) ? $where['order'] : 'date';
            switch($where['order']){
                case 'date': $order = "ordering {$direction}"; break;
                case 'year': $order = "date_construct {$direction}, ordering DESC"; break;
                case 'price': $order = "sql_price {$direction}, ordering DESC"; break;
                case 'mileage': $order = "mileage {$direction}, ordering DESC"; break;
                case 'model': $order = "title_vendor {$direction}, title_model DESC, ordering DESC"; break;
                default: $order = "ordering {$direction}"; break;
            }

        }

        if($this->configs['ad']['show_type'] == 'show_full'){
            $joins[] = "INNER JOIN %2\$s" . self::$_table['vendor'] . " as v ON v.id = t1.id_vendor";
            $joins[] = "INNER JOIN %2\$s" . self::$_table['model'] . " as m ON m.id = t1.id_model";

            $select[] = 'v.title_url as url_vendor';
            $select[] = 'm.title_url as url_model';
        }

        $whereSql = count($q) ? 'WHERE ' . implode(' AND ', $q) : '';
        $select = sprintf(implode(', ', $select), $this->db_prefix);
        $joins = implode(' ', $joins);
        $having = count($having) ? 'HAVING ' . implode(' AND ', $having) : '';

        $sql = "SELECT %s
                FROM `%2\$s%3\$s` as `t1`
                {$joins}
                {$whereSql}
                {$having}
                ORDER BY {$order} {$limitStr}";

        $sql = sprintf($sql, $select, $this->db_prefix, self::$_table['unit']);
        $result = $this->inDB->query($sql);
        $this->addInQueryStack($sql, $this->inDB->error());
        if($pager){
            $sqlCount = "SELECT COUNT(*) as count
                     FROM (
                           SELECT %s
                           FROM `%2\$s%3\$s` as `t1`
                           {$joins}
                           {$whereSql}
                           {$having}
                          ) as tc";

            $count = $this->dbGetCount('search', sprintf($sqlCount, $select, $this->db_prefix, self::$_table['unit']));
            $count = $count['count'];
        }
         // region format data
        $sizes  = array();
        foreach($this->configs['images']['ad'] as $image){
            $sizes[] = $image['name'];
        }

        $list = array();

        while ($ad = $this->inDB->fetch_assoc($result)){
            switch($this->configs['ad']['show_type']){
                case 'show_full': {
                    $ad['link'] = sprintf($this->_routes['show_full'], $ad['url_vendor'], $ad['url_model'], $ad['id']);
                } break;
                case 'show': {
                    $ad['link'] = sprintf($this->_routes['show'], $ad['id']);
                } break;
                case 'show_html': {
                    $ad['link'] = sprintf($this->_routes['show_html'], $ad['id']);
                } break;
                default: {
                    $ad['link'] = sprintf($this->_routes['show_html'], $ad['id']);
                } break;
            }

            foreach($sizes as $size){
                $ad['photo'][$size] = sprintf('%s%s/%s/%s/%s', $this->configs['system']['upload_dir'], 'ads', $ad['id'], $size, $ad['photo_name']);
            }

            $ad['price'] = $this->getCurrencyList($ad['price'], $ad['currency']);
            $ad['hasPhoto'] = $ad['photo_name'] ? true : false;

            $list[] = $ad;
        }

        $orderField = $where['order'];
        $directionField = $where['direction'];
        unset($where['user']);
        unset($where['direction']);
        unset($where['order']);
        unset($where['geo']);

        // endregion
        return array(
            'list'  => $list,
            'order' => array(
                'field'     => $orderField,
                'direction' => $directionField
            ),
            'html_query' => '?' . http_build_query(array_merge(array('page' => $page), $where)),
            'pager' => ($limit and $pager) ? cmsPage::getPagebar($count, $page, $limit, '?page=%page%&' . http_build_query(array_merge(array('order' => $orderField, 'direction' => $directionField),$where))) : null,
        );
    }

    public function getAdsListForCabinet($userId)
    {
        if(!$userId){
            return false;
        }

        return $this->getAdsList(array(
            'user'      => $userId,
            'for_cabinet' => 'true',
            'order'     => cmsCore::request('order'),
            'direction' => cmsCore::request('direction')
        ), $this->configs['ad']['list']['cabinet']['per_page'], true, false);
    }

    public function getNewsForCabinet()
    {
        $inUser = cmsUser::getInstance();

        if(!$inUser->id){
            return array();
        }

        $tableName = self::$_table['questions'];
        $data = $this->dbGetList(self::$_table['unit'], "t1.*, (SELECT COUNT(*) FROM %1\$s{$tableName} as t2 WHERE t2.id_unit = t1.id and t2.is_new = 1) as count_q", "WHERE t1.id_owner = {$inUser->id} HAVING count_q > 0");

        return $data;
    }

    public function getAdQuestion($questionId)
    {
        $table_q = $this->db_prefix . self::$_table['questions'];
        $table_a = $this->db_prefix . self::$_table['answers'];
        $table_unit = $this->db_prefix . self::$_table['unit'];

        $SQL = "SELECT q.*, u.id_owner, a.message as answer
                FROM `{$table_q}` as q
                LEFT JOIN {$table_a} as a ON a.id_question = q.id
                INNER JOIN {$table_unit} as u ON u.id = q.id_unit
                WHERE `q`.`id` = '{$questionId}'
                LIMIT 1";

        $data = $this->inDB->fetch_assoc($this->inDB->query($SQL));

        if(!$data){
            return false;
        }

        return $data;
    }

    public function getAdQa($adId, $all = false)
    {
        $tableAnswers = self::$_table['answers'];
        $inner = ($all) ? "LEFT JOIN %1\$s{$tableAnswers} as ta ON t1.id = ta.id_question" : "INNER JOIN %1\$s{$tableAnswers} as ta ON t1.id = ta.id_question";
        return $this->dbGetList(self::$_table['questions'], 't1.*, ta.message as answer, ta.created_at as answer_date', array('t1`.`id_unit' => $adId), 'by is_new DESC, id DESC', $inner);
    }

    public function setAdQaReadByAd($adId)
    {
        if(!$adId){
            return false;
        }

        $table = self::$_table['questions'];

        $sql = "UPDATE `{$this->db_prefix}{$table}`
                SET is_new = 0
                WHERE `is_new` = 1 and id_unit = '{$adId}'";

        $this->inDB->query($sql);

        $this->addInQueryStack($sql, $this->inDB->error());

        return true;
    }

    public function getUserQuestions($userId)
    {
        if(!$userId){
            return false;
        }

        $tableAnswers = self::$_table['answers'];
        $inner[] = "LEFT JOIN %1\$s{$tableAnswers} as ta ON t1.id = ta.id_question";

        $data = $this->dbGetList(self::$_table['questions'], 't1.*, ta.message as answer, ta.created_at as answer_date', array(
            't1`.`id_user' => $userId
        ), 'by id DESC', implode(' ', $inner));

        if(!count($data)){
            return false;
        }

        $idsList = $list = array();

        foreach($data as $question){
            $idsList[] = $question['id_unit'];
            $list['question'][$question['id_unit']][] = $question;
        }

        $list['ads'] = $this->getAdsList(array(
            'in' => $idsList
        ), 0, true, false);

        return $list;
    }


    public function insertAd($data)
    {
        $inUser = cmsUser::getInstance();
        $userId = $inUser->id;

        $date = new DateTime();
        $data['created_at'] = $date->format('Y-m-d H:i:s');

        $data['carcase_id'] = $this->getCarcaseByModel($data['model']['id']);
        $data['is_vip'] = 0;
        $data['background_type'] = 0;
        $data['vip_date_expiration'] = 0;
        $data['background_date_expiration'] = 0;

        $data['show'] = $this->configs['system']['moderation_type'] ? $this->configs['system']['moderation_type'] : 3;

        if($this->configs['ad']['new']['vip']['default']){
            $data['is_vip'] = $this->configs['ad']['new']['vip']['default'] ? $this->configs['ad']['new']['vip']['default'] : 0;
            $data['vip_date_expiration'] = date('Y-m-d H:i:s', $date->getTimestamp() + $this->configs['ad']['new']['vip']['time']);
        }

        if($this->configs['ad']['new']['background']['default']){
            $data['background_date_expiration'] = date('Y-m-d H:i:s', $date->getTimestamp() + $this->configs['ad']['new']['background']['time']);
            $data['background_color'] = isset($this->configs['ad']['new']['background']['color']) ? $this->configs['ad']['new']['background']['color'] : '';
        }


        if ($this->configs['ad']['add']['autoreg'] and !$userId ){

            $lastId =  $this->inDB->fetch_assoc($this->inDB->query("SELECT `id` FROM `{$this->db_prefix}users` ORDER BY `id` DESC LIMIT 1"));
            $lastId = $lastId['id'] + 1;

            $item['login'] = "auto_" . $lastId . mt_rand(10, 999);
            $item['password']  = md5($pass = self::$_component_name . substr(md5(uniqid(mt_rand(), 1)), 0, 7));
            $item['regdate']   = $data['created_at'];

            //вставить запись нового пользователя в таблицу пользователей

            $query = "INSERT INTO `{$this->db_prefix}users`
                      SET `group_id` = 1,
                          `login` = '{$item['login']}',
                          `nickname` = '{$item['login']}',
                          `password` = '{$item['password']}',
                          `regdate` = '{$item['regdate']}',
                          `email`   = '{$data['email']}',
                          `is_locked` = 0";

            $this->inDB->query($query);
            $item['user_id'] = $this->inDB->get_last_id("{$this->db_prefix}users");

            $this->inDB->query("INSERT INTO `{$this->db_prefix}user_profiles` SET `user_id`={$item['user_id']}, `allow_who` = 'all'");

            // region messages
            cmsUser::sendMessage(USER_MASSMAIL, $inUser->id, sprintf($this->_lang['COMPONENT_DATA_MAIL_FOR_NEW_USER'], $item['login'], $pass));
            /* @var $smarty Smarty */
            $smarty = cmsCore::getInstance()->initSmarty('emails/' . self::$_component_name, 'registration.tpl');
            $smarty->assign(array(
                'login'     => $item['login'],
                'password'  => $pass,
                'routes'    => $this->_routes
            ));

            cmsCore::mailText($data['email'], 'Instant-auto', $smarty->fetch('registration.tpl'));
            // endregion messages

            cmsActions::log('add_user', array(
                'object'      => '',
                'user_id'     => $item['user_id'],
                'object_url'  => '',
                'object_id'   => $item['user_id'],
                'target'      => '',
                'target_url'  => '',
                'target_id'   => 0,
                'description' => ''
            ));

            cmsUser::getInstance()->signInUser($item['login'], $pass, true);
            $userId = $item['user_id'];
        }

        $sql = "INSERT INTO `{$this->db_prefix}" . self::$_table['unit'] . "` SET
                `ordering` = UNIX_TIMESTAMP('{$data['created_at']}'),
                `id_vendor` = '{$data['vendor']['id']}',
                `title_vendor` = '{$data['vendor']['title']}',
                `cats_vendor` = '{$data['vendor']['cats_id']}',
                `id_model` = '{$data['model']['id']}',
                `title_model` = '{$data['model']['title']}',
                `carcase_id` = '{$data['carcase_id']}',
                `date_construct` = '{$data['date_constr']}',
                `id_owner` = '{$userId}',
                `price` = '{$data['price']}',
                `currency` = '{$data['currency']}',
                `volume` = '{$data['volume']}',
                `fueltype` = '{$data['fueltype']}',
                `transmission` = '{$data['transmission']}',
                `drive` = '{$data['drive']}',
                `mileage` = '{$data['mileage']}',
                `wheel` = '{$data['wheel']}',
                `is_without_rus_run` = '{$data['is_without_rus_run']}',
                `is_new_auto` = '{$data['is_new_auto']}',
                `is_broken` = '{$data['is_broken']}',
                `is_cut` = '{$data['is_cut']}',
                `is_undocumented` = '{$data['is_undocumented']}',
                `is_gt` = '{$data['is_gt']}',
                `email` = '{$data['email']}',
                `phone1` = '{$data['phone1']}',
                `phone2` = '{$data['phone2']}',
                `created_at` = '{$data['created_at']}',
                `description` = '{$data['description']}',
                `gt_description` = '{$data['gt_description']}',
                `region_id` = '{$data['geo']['region']['id']}',
                `region_title` = '{$data['geo']['region']['name']}',
                `city_id` = '{$data['geo']['city']['id']}',
                `city_title` = '{$data['geo']['city']['name']}',
                `access_issues` = '{$data['access_issues']}',
                `auto_status` = '{$data['status']}',
                `show` = '{$data['show']}',
                `is_vip` = '{$data['is_vip']}',
                `vip_date_expiration` = '{$data['vip_date_expiration']}',
                `background_color` = '{$data['background_color']}',
                `background_date_expiration` = '{$data['background_date_expiration']}'
                  ";

        $this->inDB->query($sql);
        $lastId = $this->inDB->get_last_id($this->db_prefix . self::$_table['unit']);

        if($inUser->is_admin){
            if($data['admin']['vip']['active']){
                $this->pServices('premium', array(
                    'id' => $lastId
                ), array(
                    'duration' => $data['admin']['vip']['duration']
                ));
            }

            if($data['admin']['bg']['active']){
                $this->pServices('up_bg', array(
                    'id' => $lastId
                ), array(
                    'duration' => $data['admin']['bg']['duration'],
                    'color'    => $data['admin']['bg']['color'],
                ));
            }

            if($data['admin']['attach']['active']){
                $this->pServices('attach', array(
                    'id' => $lastId
                ), array(
                    'duration' => $data['admin']['attach']['duration']
                ));
            }
        }

        //регистрируем событие
        cmsActions::log('add_auto', array(
            'object'      => $data['vendor']['title'] . ' ' . $data['model']['title'],
            'user_id'     => $userId,
            'object_url'  => sprintf($this->_routes['show_html'], $lastId),
            'object_id'   => $lastId,
            'target'      => '',
            'target_url'  => '',
            'target_id'   => '',
            'description' => ''
        ));

        $this->CheckCountAndPopular(); // ОБДУМАТЬ СПОСОБЫ ЗАМЕНЫ? ТРЕБОВАЛЬНО

        return $lastId;
    }

    public function updateAd($adId, $data, $fromCp = false)
    {
        $inUser = cmsUser::getInstance();

        $data['carcase_id'] = $this->getCarcaseByModel($data['model']['id']);
        $data['edited_at'] = date('Y-m-d H:i:s');

        if($fromCp){
            $data['show'] = $data['admin']['moderation'];
        } else {
            $data['show'] = $this->configs['system']['moderation_type'] ? $this->configs['system']['moderation_type'] : 3;
        }

        $sql = "UPDATE `{$this->db_prefix}" . self::$_table['unit'] . "` SET
                `id_vendor` = '{$data['vendor']['id']}',
                `title_vendor` = '{$data['vendor']['title']}',
                `cats_vendor` = '{$data['vendor']['cats_id']}',
                `id_model` = '{$data['model']['id']}',
                `title_model` = '{$data['model']['title']}',
                `carcase_id` = '{$data['carcase_id']}',
                `date_construct` = '{$data['date_constr']}',
                `price` = '{$data['price']}',
                `currency` = '{$data['currency']}',
                `volume` = '{$data['volume']}',
                `fueltype` = '{$data['fueltype']}',
                `transmission` = '{$data['transmission']}',
                `drive` = '{$data['drive']}',
                `mileage` = '{$data['mileage']}',
                `wheel` = '{$data['wheel']}',
                `is_without_rus_run` = '{$data['is_without_rus_run']}',
                `is_new_auto` = '{$data['is_new_auto']}',
                `is_broken` = '{$data['is_broken']}',
                `is_cut` = '{$data['is_cut']}',
                `is_undocumented` = '{$data['is_undocumented']}',
                `is_gt` = '{$data['is_gt']}',
                `email` = '{$data['email']}',
                `phone1` = '{$data['phone1']}',
                `phone2` = '{$data['phone2']}',
                `edited_at` = '{$data['edited_at']}',
                `description` = '{$data['description']}',
                `gt_description` = '{$data['gt_description']}',
                `region_id` = '{$data['geo']['region']['id']}',
                `region_title` = '{$data['geo']['region']['name']}',
                `city_id` = '{$data['geo']['city']['id']}',
                `city_title` = '{$data['geo']['city']['name']}',
                `access_issues` = '{$data['access_issues']}',
                `auto_status` = '{$data['status']}',
                `show` = '{$data['show']}'
                WHERE `id` = '{$adId}'";

        $this->inDB->query($sql);

        if($inUser->is_admin){
            if($data['admin']['vip']['active']){
                $this->pServices('premium', array(
                    'id' => $adId
                ), array(
                    'duration' => $data['admin']['vip']['duration']
                ));
            }

            if($data['admin']['bg']['active']){
                $this->pServices('up_bg', array(
                    'id' => $adId
                ), array(
                    'duration' => $data['admin']['bg']['duration'],
                    'color'    => $data['admin']['bg']['color'],
                ));
            }

            if($data['admin']['attach']['active']){
                $this->pServices('attach', array(
                    'id' => $adId
                ), array(
                    'duration' => $data['admin']['attach']['duration']
                ));
            }
        }

        $this->CheckCountAndPopular(); // ОБДУМАТЬ СПОСОБЫ ЗАМЕНЫ? ТРЕБОВАЛЬНО

        return true;
    }

    public function insertQuestion($adId, $data)
    {
        $date = new DateTime();
        $createdAt = $date->format('Y-m-d H:i:s');

        $data['show_type'] = in_array($data['show_type'], array(1, 2)) ? $data['show_type'] : 1;

        $this->inDB->query($sql = "
            INSERT INTO `{$this->db_prefix}" . self::$_table['questions'] . "` SET
            id_unit = '{$adId}',
            id_user = '{$data['id_user']}',
            author = '{$data['author']}',
            created_at = '{$createdAt}',
            email = '{$data['email']}',
            phone = '{$data['phone']}',
            message = '{$data['message']}',
            is_new = 1,
            show_type = '{$data['show_type']}'
        ");

        $lastId = $this->inDB->get_last_id($this->db_prefix . self::$_table['questions']);

        if(!$lastId){
            return false;
        }

        return $lastId;
    }

    public function insertAnswer($questionId, $data)
    {
        $date = new DateTime();
        $createdAt = $date->format('Y-m-d H:i:s');

        $this->inDB->query($sql = "
            INSERT INTO `{$this->db_prefix}" . self::$_table['answers'] . "` SET
            id_question = '{$questionId}',
            id_unit = '{$data['id_unit']}',
            created_at = '{$createdAt}',
            message = '{$data['answer']}'
        ");

        $lastId = $this->inDB->get_last_id($this->db_prefix . self::$_table['answers']);

        if(!$lastId){
            return false;
        }

        return $lastId;
    }

    // endregion

    public function getCatsColumn($colCount, $data, $routeCfg = 'vendor')
    {
        $dataCount = $maxCount = count($data);

        $list = array();

        $prev_f_title = '';
        $colIndex = 0;
        $count_in_col = 0;
        $f_title_index = 0;

        foreach($data as $index => $cat){
            if($this->configs['system']['groupRusVendors'] and isset($cat['cats_id']) and $cat['cats_id'] == 2){
                $f_title = 'АБВ';
            } else {
                $f_title = mb_substr($cat['title'], 0, 1);
            }

            $next_f_title = isset($data[$index+1]) ? mb_substr($data[$index+1]['title'], 0, 1) : '';

            if($f_title !== $prev_f_title){
                $list[$colIndex][$f_title_index]['f_title'] = $f_title;
            }

            switch($routeCfg['type']){
                case 'vendor': {
                    $cat['url'] = sprintf($this->_routes['showVendor'], $cat['title_url']);
                    $cat['show_logo'] = true;
                } break;
                case 'model': {
                    $cat['url'] = sprintf($this->_routes['showModel'], $routeCfg['vendor_url'], $cat['title_url']);
                } break;
            }

            $list[$colIndex][$f_title_index]['data'][] = $cat;

            $count_in_col++;
            $dataCount--;

            if($f_title !== $next_f_title){
                $f_title_index++;

                if($colIndex + 1 < $colCount and $count_in_col >= ceil($maxCount/$colCount)){
                    $count_in_col = 0;
                    $colIndex++;
                }
            }


            $prev_f_title = $f_title;
        }

        return $list;
    }

    public function getVendor($vendorId)
    {
        return $this->dbGetOneById($vendorId, self::$_table['vendor']);
    }

    public function getVendorsList($withConfig = true, $withPopular = false, $dynCount = true)
    {
        $where = '';
        $select = '*';
        if($dynCount and $this->geo_selector['region'] or $this->geo_selector['city']){
            $geo = array();
            if($this->geo_selector['region']){
                $geo[] = "u.region_id = {$this->geo_selector['region']}";
            }

            if($this->geo_selector['city']){
                $geo[] = "u.city_id = {$this->geo_selector['city']}";
            }
            $geo = implode(' AND ', $geo);
            $geo = $geo ? " AND {$geo} " : '';
            $where = ($withConfig) ? ($this->configs['system']['showNullCats'] ? null :'HAVING `count_advt` > 0') : null;
            $select = "t1.*, (SELECT COUNT(*) FROM %1\$s" . self::$_table['unit'] . " as u WHERE t1.id = u.id_vendor {$geo}) as count_advt";
        } else {
            $where = ($withConfig) ? ($this->configs['system']['showNullCats'] ? null :'WHERE `count_advt` > 0') : null;
        }

       $order = $withPopular ? 'by `popular` DESC , `title` ASC' : 'by `title` ASC';

        return $this->dbGetList(self::$_table['vendor'], $select, $where, $order);
    }

    public function getVendorByUrlTitle($urlTitle)
    {
        return $this->getByUrl($urlTitle, self::$_table['vendor']);
    }

    public function getModel($model)
    {
        return $this->dbGetOneById($model, self::$_table['model']);
    }

    public function getModelsList($withConfig = true, $withPopular = false)
    {
        $where = ($withConfig) ? ($this->configs['system']['showNullCats'] ? null :'WHERE `count_advt` > 0') : null;

        $order = $withPopular ? 'by `popular` DESC , `title` ASC' : 'by `title` ASC';

        return $this->dbGetList(self::$_table['model'], null, $where, $order);
    }

    public function getModelByUrlTitle($vendorId, $urlTitle)
    {
        return $this->getByUrl($urlTitle, self::$_table['model'], array("`vendor_id` = '{$vendorId}'"));
    }

    public function getModelsByVendor($vendorId, $withConfig = true, $withPopular = true, $dynCount = true)
    {
        $where = array("`vendor_id` = {$vendorId}");
        $select = '*';
        $having = '';
        if($dynCount and $this->geo_selector['region'] or $this->geo_selector['city']){
            $geo = array();
            if($this->geo_selector['region']){
                $geo[] = "u.region_id = {$this->geo_selector['region']}";
            }

            if($this->geo_selector['city']){
                $geo[] = "u.city_id = {$this->geo_selector['city']}";
            }
            $geo = implode(' AND ', $geo);
            $geo = $geo ? " AND {$geo} " : '';
            $having = ($withConfig) ? ($this->configs['system']['showNullCats'] ? null :' HAVING `count_advt` > 0 ') : null;
            $select = "t1.*, (SELECT COUNT(*) FROM %1\$s" . self::$_table['unit'] . " as u WHERE t1.id = u.id_model {$geo}) as count_advt";
        } else {
            $where[] = ($withConfig) ? ($this->configs['system']['showNullCats'] ? '1=1' :'`count_advt` > 0') : '1=1';
        }


        $order = $withPopular ? 'by `popular` DESC , `title` ASC' : 'by `title` ASC';

        return $this->dbGetList(self::$_table['model'], $select, 'WHERE ' . implode(' AND ', $where) . $having, $order);
    }

    public function getCarcaseList()
    {
        return $this->dbGetList(self::$_table['cats'], null, 'WHERE lvl = 1');
    }

    public function getCarcaseListIds()
    {
        $list = $this->dbGetList(self::$_table['cats'], null, 'WHERE lvl = 1');

        $data = array();
        foreach($list as $carcase){
            $data[] = $carcase['id'];
        }

        return $data;
    }

    public function getCarcaseByModel($modelId)
    {
        $model = $this->getModel($modelId);

        return $model['case_id'];
    }

    public function getDateList()
    {
        $date = array();

        for ( $i = date('Y', time()); $i >= $this->configs['system']['date_start']; $i--){
            $date[] = $i;
        }

        return $date;
    }

    public function getSearchFilterParamsByQuery($q = null, $merge = false)
    {
        if(!$q or $merge){
            $presetQ = $q;
            $q = array(
                'id_vendor'    => cmsCore::request('id_vendor', 'int'),
                'id_model'     => cmsCore::request('id_model', 'int'),
                // region price
                'minprice'     => cmsCore::request('minprice', 'int'),
                'maxprice'     => cmsCore::request('maxprice', 'int'),
                // endregion price
                // region date
                'date'         => cmsCore::request('date', 'int'),
                'datemax'      => cmsCore::request('datemax', 'int'),
                // endregion datez
                // region volume
                'min_volume'   => (float)cmsCore::request('min_volume'),
                'max_volume'   => (float)cmsCore::request('max_volume'),
                // endregion volume
                'fueltype'     => cmsCore::request('fueltype', 'int'),
                'transmission' => cmsCore::request('transmission', 'int'),
                'drive'        => cmsCore::request('drive', 'int'),
                'with_photo'   => cmsCore::request('with_photo', 'int'),
                'foreign'      => cmsCore::request('foreign', 'int'),

                'docs'         => cmsCore::request('docs', 'int'),
                'damaged'      => cmsCore::request('damaged', 'int'),
                'carcase'      => cmsCore::request('carcase', 'array_int'),
                'wheel'        => cmsCore::request('wheel', 'array_int'),

                'is_new'      => cmsCore::request('is_new', 'int'),
                'is_gt'       => cmsCore::request('is_gt', 'int'),
                'is_no_rus_mil'       => cmsCore::request('is_no_rus_mil', 'int'),
                'unsold'       => cmsCore::request('unsold', 'int'),
                'is_cut'       => cmsCore::request('is_cut', 'int'),
                'order'        => cmsCore::request('order'),
                'direction'    => cmsCore::request('direction')
            );

            $q = ($merge) ? array_merge($q, $presetQ) : $q;
        }

        $data = array(
            'vendors' => $this->getVendorsList(true, true),
            'date'    => $this->getDateList(),
        );

        if(isset($q['nin']) and count($q['nin'])){
            $data['data']['nin'] = $q['nin'];
        }

        if($q['order']){
            $data['data']['order'] = $q['order'];
        }

        if($q['direction']){
            $data['data']['direction'] = $q['direction'];
        }

        if(isset($q['id_vendor'])){
            if($q['id_vendor'] > 0 and $this->getVendor($q['id_vendor'])){
                $data['data']['id_vendor'] =  $q['id_vendor'];

                $data['models'] = $this->getModelsByVendor($q['id_vendor']);
                if($q['id_model'] and $this->getModel($q['id_model'])){
                    $data['data']['id_model'] = $q['id_model'];
                }
            }
        }

        // region date
        if($q['date'] > $q['datemax'] and $q['datemax']){
            $tmp = $q['datemax'];
            $q['datemax'] = $q['date'];
            $q['date'] = $tmp;
        }

        if(in_array($q['date'], $this->getDateList())){
            $data['data']['date'] = $q['date'];
        }

        if(in_array($q['datemax'], $this->getDateList())){
            $data['data']['datemax'] = $q['datemax'];
        }
        // endregion date

        if(isset($q['currency'])){
            $data['data']['currency'] = $q['currency'];
        }

        // region price
        if($q['minprice'] > $q['maxprice'] and $q['maxprice']){
            $tmp = $q['maxprice'];
            $q['maxprice'] = $q['minprice'];
            $q['minprice'] = $tmp;
        }

        if($q['minprice'] > 0){
            $data['data']['minprice'] = $q['minprice'];
        }

        if($q['maxprice'] > 0){
            $data['data']['maxprice'] = $q['maxprice'];
        }
        // endregion date

        // region volume
        if($q['min_volume'] > $q['max_volume'] and $q['max_volume']){
            $tmp = $q['max_volume'];
            $q['max_volume'] = $q['min_volume'];
            $q['min_volume'] = $tmp;
        }

        if($q['min_volume'] > 0){
            $data['data']['min_volume'] = $q['min_volume'];
        }

        if($q['max_volume'] > 0){
            $data['data']['max_volume'] = $q['max_volume'];
        }
        // endregion volume

        // region fueltype
        if(in_array($q['fueltype'], array_keys($this->_component_data['fueltype']))){
            $data['data']['fueltype'] = $q['fueltype'];
        }
        // endregion fueltype

        // region transmission
        if(in_array($q['transmission'], array_keys($this->_component_data['transmission']))){
            $data['data']['transmission'] = $q['transmission'];
        }
        // endregion transmission

        // region drive
        if(in_array($q['drive'], array_keys($this->_component_data['drive']))){
            $data['data']['drive'] = $q['drive'];
        }
        // endregion drive

        // region wheel
        if(is_array($q['wheel']) and count($q['wheel'])){
            foreach($q['wheel'] as $wheel){
                if(in_array($wheel, array_keys($this->_component_data['wheel']))){
                    $data['data']['wheel'][] = $wheel;
                }
            }
        }
        // endregion wheel

        // region docs
        if(in_array($q['docs'], array(1, 2))){
            $data['data']['docs'] = $q['docs'];
        }
        // endregion docs

        // region damaged
        if(in_array($q['damaged'], array(1, 2))){
            $data['data']['damaged'] = $q['damaged'];
        }
        // endregion docs

        // region carcase
        if(is_array($q['carcase']) and count($q['carcase'])){
            $carcaseList = $this->getCarcaseListIds();
            foreach($q['carcase'] as $carcase){
                if(in_array($carcase, $carcaseList)){
                    $data['data']['carcase'][] = $carcase;
                }
            }
        }
        // endregion

        // region is_cut
        if($q['is_cut']){
            $data['data']['is_cut'] = 1;
        }
        // endregion is_cut

        // region foreign
        if($q['foreign']){
            $data['data']['foreign'] = 1;
        }
        // endregion foreign

        // region unsold
        if($q['unsold']){
            $data['data']['unsold'] = 1;
        }
        // endregion unsold

        // region is_no_rus_mil
        if($q['is_no_rus_mil']){
            $data['data']['is_no_rus_mil'] = 1;
        }
        // endregion is_no_rus_mil

        // region is_gt
        if($q['is_gt']){
            $data['data']['is_gt'] = 1;
        }
        // endregion is_gt

        // region is_new
        if($q['is_new']){
            $data['data']['is_new'] = 1;
        }
        // endregion is_new
        if($q['with_photo']){
            $data['data']['with_photo'] = 1;
        }

        // region geo
        $geo_selector = $this->getGeo_selector();
        $data['geo']['regions'] = $this->geo->getRegions($this->configs['geo']['country']);

        if($regionId = $geo_selector['region']){
            $data['geo']['region'] = $this->geo->getRegion($regionId);
            $data['geo']['cities'] = $this->geo->getCities($regionId);

            if($cityId = $geo_selector['city']){
                $data['geo']['city'] = $this->geo->getCity($cityId);
            }
        }
        // endregion geo

        return $data;
    }

    private function checkPhone($phone)
    {
        return preg_match('/^\+[7] \d{3} \d{3}[-]\d{4}/i', $phone);
    }

    private function checkPrice($price, $currency)
    {
        $priceList = $this->getCurrencyList($price, $currency);

        switch($priceList[1]){
            case $priceList[1] < $this->configs['ad']['currency']['min']: return 'less'; break;
            case $priceList[1] > $this->configs['ad']['currency']['max']: return 'more'; break;
            default: return 'ok'; break;
        }
    }

    private function getCurrencyList($price, $type = 1)
    {
        switch($type){
            case 1: {
                $data = array(
                    1 => $price,
                    2 => round($price / $this->_currencys['2:1']),
                    3 => round($price / $this->_currencys['3:1'])
                );
            } break;
            case 2: {
                $data = array(
                    1 => round($price * $this->_currencys['2:1']),
                    2 => $price,
                    3 => round($price * $this->_currencys['2:3'])
                );
            } break;
            case 3: {
                $data = array(
                    1 => round($price * $this->_currencys['3:1']),
                    2 => round($price / $this->_currencys['3:2']),
                    3 => $price
                );
            } break;
        }

        return $data;
    }

    public function checkAddForm($presetData = array())
    {
        $inUser = cmsUser::getInstance();
        $submit = cmsCore::request('submit', 'int');

        if (!$submit){
            return array(
                'status' => 0
            );
        }


        $formData = array(
            'vendor' => cmsCore::request('vendor', 'int'),
            'model'  => cmsCore::request('model', 'int'),

            'date_constr' => cmsCore::request('date_constr', 'int'),
            'wheel' => cmsCore::request('wheel', 'int'),

            'is_new_auto'  => cmsCore::request('is_new_auto', 'int') ? 1 : 0,
            'mileage'    => cmsCore::request('mileage', 'int'),
            'is_without_rus_run'  => cmsCore::request('is_without_rus_run', 'int') ? 1 : 0,

            'volume' => (float)cmsCore::request('volume'),
            'fueltype' => cmsCore::request('fueltype', 'int'),
            'transmission' => cmsCore::request('transmission', 'int'),
            'drive' => cmsCore::request('drive', 'int'),

            'is_undocumented'    => cmsCore::request('is_undocumented', 'int') ? 1 : 0,
            'is_broken'    => cmsCore::request('is_broken', 'int') ? 1 : 0,
            'is_cut'    => cmsCore::request('is_cut', 'int') ? 1 : 0,

            'description'    => cmsCore::request('description', 'html'),
            'is_gt'    => cmsCore::request('is_gt', 'int') ? 1 : 0,
            'gt_description'    => cmsCore::request('gt_description', 'html'),

            'price'       => cmsCore::request('price', 'int'),
            'currency'    => cmsCore::request('currency', 'int'),
            'status'      => cmsCore::request('status', 'int'),
            'geo'    => array(
                'region' => $this->configs['geo']['region']['default'] ? $this->configs['geo']['region']['default'] : (int)$_REQUEST['geo']['region'],
                'city' => $this->configs['geo']['city']['default'] ? $this->configs['geo']['city']['default'] : (int)$_REQUEST['geo']['city'],
            ),

            'phone1'    => cmsCore::request('phone1'),
            'phone2'    => cmsCore::request('phone2'),
            'email'    => cmsCore::request('email'),

            'access_issues'  => cmsCore::request('access_issues', 'int') ? 1 : 0,

        );

        if(count($presetData)){
            if($this->configs['ad']['add']['can_edit_vendor']){
                unset($presetData['vendor']);
                unset($presetData['model']);
            }

            if($this->configs['ad']['add']['can_edit_geo']){
                unset($presetData['geo']);
            }

            $formData = array_merge($formData, $presetData);

        }

        $setters = $errors = $temp = array();

        // region vendor and model
        if ($formData['vendor'] and $dbVendor = $this->getVendor($formData['vendor'])){
            $temp['vendor'] = $setters['vendor'] = $dbVendor;
            $temp['models'] = $setters['models']  = $this->getModelsByVendor($dbVendor['id'], false);

            $temp['model'] = $setters['model'] = ($formData['model'] and $dbModel = $this->getModel($formData['model'])) ? $dbModel : null;

        } else{
            $temp['vendor'] = $setters['vendor'] = null;
        }

        if(!$temp['vendor']){
            $errors['vendor'] = array(
                'selector' => "select[name='vendor']",
                'title'    => $this->_lang['TEMPLATE_ADD_FORM_ERROR_VENDOR']
            );
        }

        if(!$temp['model']){
            $errors['model'] = array(
                'selector' => "select[name='model']",
                'title'    => $this->_lang['TEMPLATE_ADD_FORM_ERROR_MODEL']
            );
        }

        // endregion
        // region date construct
        if(in_array($formData['date_constr'], $this->getDateList())){
            $temp['date_constr'] = $setters['date_constr'] = $formData['date_constr'];
        } else {
            $errors['date_constr'] = array(
                'selector' => "select[name='date_constr']",
                'title'    => $this->_lang['TEMPLATE_ADD_FORM_ERROR_DATE_CONSTRUCT']
            );
        }
        // endregion
        // region volume
        if($formData['volume']){
            if($formData['volume'] > 10){
                $errors['model'] = array(
                    'selector' => "input[name='volume']",
                    'title'    => $this->_lang['TEMPLATE_ADD_FORM_ERROR_VOLUME_TOOMUCH']
                );
            } else {
                $temp['volume'] = $setters['volume'] = $formData['volume'];
            }
        }
        // endregion
        // region wheel
        if(array_key_exists($formData['wheel'], $this->_component_data['wheel'])){
            $temp['wheel'] = $setters['wheel'] = $formData['wheel'];
        } else {
            //TODO: дописать если руль нужен будет обязательным параметром
        }
        // endregion
        // region is_new_auto
        if($formData['is_new_auto']){
            $temp['is_new_auto'] = $setters['is_new_auto'] = $formData['is_new_auto'];
        }
        // endregion
        // region mileage
        if(!$formData['is_new_auto'] and $formData['mileage'] > 0){
            $temp['mileage'] = $setters['mileage'] = $formData['mileage'];
        }
        // endregion
        // region is_without_rus_run
        $temp['is_without_rus_run'] = $setters['is_without_rus_run'] = $formData['is_without_rus_run'];
        // endregion
        // region fueltype
        if(array_key_exists($formData['fueltype'], $this->_component_data['fueltype'])){
            $temp['fueltype'] = $setters['fueltype'] = $formData['fueltype'];
        } else {
            //TODO: дописать если нужен будет обязательным параметром
        }
        // endregion
        // region transmission
        if(array_key_exists($formData['transmission'], $this->_component_data['transmission'])){
            $temp['transmission'] = $setters['transmission'] = $formData['transmission'];
        } else {
            //TODO: дописать если нужен будет обязательным параметром
        }
        // endregion
        // region drive
        if(array_key_exists($formData['drive'], $this->_component_data['drive'])){
            $temp['drive'] = $setters['drive'] = $formData['drive'];
        } else {
            //TODO: дописать если нужен будет обязательным параметром
        }
        // endregion
        // region is_undocumented
        $temp['is_undocumented'] = $setters['is_undocumented'] = $formData['is_undocumented'];
        // endregion
        // region is_broken
        $temp['is_broken'] = $setters['is_broken'] = $formData['is_broken'];
        // endregion
        // region is_cut
        $temp['is_cut'] = $setters['is_cut'] = $formData['is_cut'];
        // endregion
        // region description

        $temp['description'] = $setters['description'] = $formData['description'] = self::clearHtml($formData['description'], '<b><strong><ol><ul><li><p><br>');
        // endregion
        // region is_gt
        $temp['is_gt'] = $setters['is_gt'] = $formData['is_gt'];
        // endregion
        // region gt_description
        if($formData['is_gt']){
            $temp['gt_description'] = $setters['gt_description'] = $formData['gt_description'] = self::clearHtml($formData['gt_description'], '<b><strong><ol><ul><li><p><br>');
        }
        // endregion

        // region geo
        $temp['geo']['regions'] = $setters['geo']['regions'] = $this->geo->getRegions($this->configs['geo']['country']);

        if($formData['geo']['region'] and $dbRegion = $this->geo->getRegion($formData['geo']['region'])){
            $temp['geo']['region'] = $setters['geo']['region'] = $dbRegion;
            $temp['geo']['cities'] = $setters['geo']['cities'] = $this->geo->getCities($dbRegion['id']);

            if($formData['geo']['city'] and $dbCity = $this->geo->getCity($formData['geo']['city'])){
                $temp['geo']['city'] = $setters['geo']['city'] = $dbCity;
            } else {
                $errors['city'] = array(
                    'selector' => "select[name='geo[city]']",
                    'title'    => $this->_lang['TEMPLATE_ADD_FORM_ERROR_CITY']
                );
            }

        } else {
            $setters['regions'] = $this->geo->getRegions($this->configs['geo']['country']);
            $errors['region'] = array(
                'selector' => "select[name='geo[region]']",
                'title'    => $this->_lang['TEMPLATE_ADD_FORM_ERROR_REGION']
            );
        }
        // endregion
        // region status

        if(array_key_exists($formData['status'], $this->_component_data['status'])){
            $temp['status'] = $setters['status'] = $formData['status'];
        } else {
            $errors['status'] = array(
                'selector' => "input[name='status']",
                'title'    => $this->_lang['TEMPLATE_ADD_FORM_ERROR_STATUS']
            );
        }
        // endregion
        // region phones
        if($formData['phone1']){
            if(!$this->checkPhone($formData['phone1'])){
                $errors['phone1'] = array(
                    'selector' => "input[name='phone1']",
                    'title'    => $this->_lang['TEMPLATE_ADD_FORM_ERROR_PHONE_INCORRECT']
                );
            }
            $temp['phone1'] = $setters['phone1'] = $formData['phone1'];
        } else {
            $errors['phone1'] = array(
                'selector' => "input[name='phone1']",
                'title'    => $this->_lang['TEMPLATE_ADD_FORM_ERROR_PHONE_NOT_SET']
            );
        }

        if($formData['phone2']){
            if(!$this->checkPhone($formData['phone2'])){
                $errors['phone2'] = array(
                    'selector' => "input[name='phone2']",
                    'title'    => $this->_lang['TEMPLATE_ADD_FORM_ERROR_PHONE_INCORRECT']
                );
            }
            $temp['phone2'] = $setters['phone2'] = $formData['phone2'];
        }
        // endregion
        // region email
        if(!$inUser->id){
            if(!filter_var($formData['email'], FILTER_VALIDATE_EMAIL)){
                $errors['email'] = array(
                    'selector' => "input[name='email']",
                    'title'    => $this->_lang['TEMPLATE_ADD_FORM_ERROR_INCORRECT_EMAIL']
                );
            } else {
                //filter var ne pustit '` itp
                $exists_email = $this->inDB->fetch_assoc($this->inDB->query("SELECT `id` FROM `{$this->db_prefix}users` WHERE `email` = '{$formData['email']}'"));

                if($exists_email){
                    $errors['email'] = array(
                        'selector' => "input[name='email']",
                        'title'    => $this->_lang['TEMPLATE_ADD_FORM_ERROR_EXISTS_EMAIL']
                    );
                }
            }

            $temp['email'] = $setters['email'] = $formData['email'];
        }
        // endregion
        // region access_issues
        $temp['access_issues'] = $setters['access_issues'] = $formData['access_issues'];
        // endregion
        // region captcha
        if (!$inUser->id and cmsCore::checkCaptchaCode(cmsCore::request('code'))){
            $temp['code'] = $setters['code'] = true;
        } elseif(!$inUser->id){
            $errors['captcha'] = array(
                'selector' => "input[name='code']",
                'title'    => $this->_lang['TEMPLATE_ADD_FORM_ERROR_CAPTCHA']
            );
        }
        // endregion

        // region price
        if(in_array($formData['currency'], array_keys($this->_component_data['currency']))){
            $temp['currency'] = $setters['currency'] = $formData['currency'];

            if($formData['price']){
                switch($this->checkPrice($formData['price'], $formData['currency'])){
                    case 'less': {
                        $errors['price'] = array(
                            'selector' => "input[name='price']",
                            'title'    => 'Цена автомобиля не может быть такой маленькой'
                        );
                    } break;
                    case 'more': {
                        $errors['price'] = array(
                            'selector' => "input[name='price']",
                            'title'    => 'Цена автомобиля не может быть такой большой'
                        );
                    } break;
                    case 'ok': {

                    } break;
                }

                $temp['price'] = $setters['price'] = $formData['price'];

            } else {
                $errors['price'] = array(
                    'selector' => "input[name='price']",
                    'title'    => 'Цена обязательна для заполнения'
                );
            }

        } else {
            $errors['currency'] = array(
                'selector' => "select[name='currency']",
                'title'    => 'Валюта указана не верно'
            );
        }
        // endregion

        if($inUser->is_admin){
            $temp['admin'] = $setters['admin'] = $formData['admin'] = $_REQUEST['admin'];
        }

        $status = count($errors) ? 1 : 2;

        return array(
            'data' => $formData,
            'setters' => $setters,
            'errors' => $errors,
            'status' => $status
        );

    }

    public function incViews($id)
    {
        $this->dbUpdate($id, self::$_table['unit'], '`count_views` = `count_views` + 1');
    }

    public function getCarousel($cData = null)
    {
        cmsPage::getInstance()->addHeadJS('components/' . self::$_component_name . '/js/jcarousel.js');

        $data = $this->getAdsList(array(
            'with_photo'       => true,
            'from_carousel'    => true,
            'carousel_configs' => $cData
        ), 15, false);

        if(count($data['list']) >= $this->configs['ad']['carousel']['min']){
            return $data;
        }

        return null;
    }


    // region db
    private function dbGetList($tableName, $select = '*', $where = '',  $order = 'by `id` DESC', $joins = '')
    {
        $select = (!$select) ? '*' : $select;


        if(is_array($where) and count($where)){
            $whereString = array();
            foreach($where as $field => $value){
                $whereString[] = "`{$field}` = '{$value}'";
            }

            $whereString = 'WHERE ' . implode(' AND ', $whereString);
        } elseif(is_string($where)) {
            $whereString = $where;
        } else {
            $whereString = null;
        }

        $sql = "SELECT {$select}
                FROM `%1\$s{$tableName}` as `t1`
                {$joins}
                {$whereString}
                ORDER {$order}";

        $sql = sprintf($sql, $this->db_prefix);
        $result = $this->inDB->query($sql);

        if ($this->configs['system']['debug']){
            $this->addInQueryStack($sql, $this->inDB->error());
        }

        $data = array();
        $i = 0;

        while ($item = $this->inDB->fetch_assoc($result)){
            $data[$i++] = $item;
        }

        return $data;
    }

    public function dbGetCount($type, $value)
    {
        switch($type){
            case 'stats': {
                switch($value){
                    case 'all': $count = $this->getCount(self::$_table['unit'], ''); break;
                    case 'new': {
                        $date = new DateTime();
                        $date = $date->format('Y-m-d');

                        $count = $this->getCount(self::$_table['unit'], "WHERE `created_at` >= '{$date}'");
                    } break;
                    case 'active': $count = $this->getCount(self::$_table['unit'], 'WHERE `show` in(2, 3)'); break;
                    case 'sell': $count = $this->getCount(self::$_table['unit'], 'WHERE `auto_status` = 0'); break;
                    case 'moderation': $count = $this->getCount(self::$_table['unit'], 'WHERE `show` in(1, 2)'); break;

                }
                return $count;
            } break;
            case 'stat_all': {
                $count = $this->getCount(self::$_table['unit'], 'WHERE `show` in(2, 3)');
                return $count;
            } break;
            case 'photo': return $this->getCount(self::$_table['photo'], "WHERE `id_unit` ='{$value}'"); break;
            case 'search': {

                $count =  $this->inDB->fetch_assoc($this->inDB->query($value));
                $this->addInQueryStack($value, $this->inDB->error());

                return $count;
            } break;
            case 'userAd': {
                return $this->getCount(self::$_table['unit']," WHERE `id_owner` = '{$value}'");
            } break;
            case 'userQuestions': {
                return $this->getCount(self::$_table['questions']," WHERE `id_user` = '{$value}'");
            } break;
        }
    }

    /**
     * @param        $table
     * @param string $where
     *
     * @return mixed
     */
    public function getCount($table, $where = 'WHERE `count_advt` > 0')
    {
        $result = $this->inDB->fetch_assoc($this->inDB->query($query = "SELECT COUNT(*) as count FROM `{$this->db_prefix}{$table}` {$where}"));

        if($this->configs['system']['debug']) {
            $this->addInQueryStack($query, $this->inDB->error());
        }

        return $result['count'];
    }

    private function dbGetOneById($id, $table, $add_where = null, $select = '*', $inner = null)
    {
        if(!(int)$id){
            return false;
        }

        if(count($inner)){
            $inner = sprintf(implode(' ', $inner), $this->db_prefix);
        }

        $SQL = "SELECT {$select}
                FROM `{$this->db_prefix}{$table}` as t1
                {$inner}
                WHERE `t1`.`id` = {$id}" . implode(' AND ', $add_where) . "
                LIMIT 1";

        $data = $this->inDB->fetch_assoc($this->inDB->query($SQL));
        if ($this->configs['system']['debug']){
            $this->addInQueryStack($SQL, $this->inDB->error());
        }

        return $data;
    }

    private function dbUpdate($id, $table, $setString)
    {
        $sql = "UPDATE `{$this->db_prefix}{$table}`
                SET {$setString}
                WHERE `id` = '{$id}'";

        $this->inDB->query($sql);

        $this->addInQueryStack($sql, $this->inDB->error());

        return true;
    }

    private function getByUrl($urlTitle, $table, $where = array())
    {

        switch($table){
            case self::$_table['vendor']: $where[] = "`title_url` = '{$urlTitle}'"; break;
            case self::$_table['model']: $where[] = "`title_url` = '{$urlTitle}'"; break;
            default: {
            return null;
            } break;
        }

        $SQL = "SELECT *
                FROM {$this->db_prefix}{$table}
                WHERE " . implode(' AND ', $where) ."
                LIMIT 1";

        $result =  $this->inDB->query($SQL);
        $data = $this->inDB->fetch_assoc($result);

        return $data;

    }
    // endregion

    // region query log
    public function showQueryStack()
    {
        $html = array();
        foreach($this->_debugStack as $query){
            $html[] = "QUERY: {$query['q']} <br> ERROR: {$query['e']}";
        }

        echo implode(' <hr> ', $html);
    }

    private function addInQueryStack($query, $error)
    {
        $this->_debugStack[] = array(
            'q' => $query,
            'e' => $error
        );
    }
    // endregion

    // endregion


    // region admin
    private function ru2Lat($string)
    {
        return strtr($string, array(
            "А" => "a",
            "Б" => "b",
            "В" => "v",
            "Г" => "g",
            "Д" => "d",
            "Е" => "e",
            "Ж" => "j",
            "З" => "z",
            "И" => "i",
            "Й" => "y",
            "К" => "k",
            "Л" => "l",
            "М" => "m",
            "Н" => "n",
            "О" => "o",
            "П" => "p",
            "Р" => "r",
            "С" => "s",
            "Т" => "t",
            "У" => "u",
            "Ф" => "f",
            "Х" => "h",
            "Ц" => "ts",
            "Ч" => "ch",
            "Ш" => "sh",
            "Щ" => "sch",
            "Ъ" => "",
            "Ы" => "yi",
            "Ь" => "",
            "Э" => "e",
            "Ю" => "yu",
            "Я" => "ya",
            "а" => "a",
            "б" => "b",
            "в" => "v",
            "г" => "g",
            "д" => "d",
            "е" => "e",
            "ж" => "j",
            "з" => "z",
            "и" => "i",
            "й" => "y",
            "к" => "k",
            "л" => "l",
            "м" => "m",
            "н" => "n",
            "о" => "o",
            "п" => "p",
            "р" => "r",
            "с" => "s",
            "т" => "t",
            "у" => "u",
            "ф" => "f",
            "х" => "h",
            "ц" => "ts",
            "ч" => "ch",
            "ш" => "sh",
            "щ" => "sch",
            "ъ" => "y",
            "ы" => "yi",
            "ь" => "",
            "э" => "e",
            "ю" => "yu",
            "я" => "ya",
            " " => "_",
            "." => "",
            "/" => "_",
            "'" => "",
            "`" => ""
        ));
    }

    public function adminGetVendorCat()
    {
        return $this->dbGetList(self::$_table['cats'], null, array(
            'lvl' => 0
        ));
    }

    public function adminAddVendor($title, $cat, $popular = false)
    {
        $title_url = $this->ru2Lat($title);
        $popular = ($popular) ? 1 : 0;

        $files = new UploadedFiles();
        $files->multiple = false;
        $files->varname = 'logo';

        $photoUploader = new PhotoUploader();

        $photoUploader->setImages(array(
            array(
                'name'   => 'small',
                'weight' => 27,
                'height' => 20,
                'thumb'  => 1
            )
        ))
            ->setUploadDir($this->configs['system']['upload_dir'] . "vendors/")
            ->setSaveOriginal(false)
            ->setQuality(90);

        $logo = '';
        foreach($files->getFilesList() as $file){
            if($filePaths = $photoUploader->upload($file['tmp_name'])){
                $logo = $filePaths['filename'];
            }
        }

        $table = $this->db_prefix . self::$_table['vendor'];

        $sql = "INSERT INTO {$table} SET
                `title` = '{$title}',
                `title_url` = '{$title_url}',
                `cats_id` = '{$cat}',
                `popular` = '{$popular}',
                `logo` = '{$logo}'
                ";

        $this->inDB->query($sql);

        return true;
    }

    public function adminEditVendor($vendorId, $data)
    {

        $vendor = $this->getVendor($vendorId);

        if(!$vendor){
            return false;
        }

        $title_url = $this->ru2Lat($data['title']);
        $popular = ($data['popular']) ? 1 : 0;

        $files = new UploadedFiles();
        $files->multiple = false;
        $files->varname = 'logo';

        $photoUploader = new PhotoUploader();

        $photoUploader->setImages(array(
            array(
                'name'   => 'small',
                'weight' => 27,
                'height' => 20,
                'thumb'  => 1
            )
        ))
            ->setUploadDir($this->configs['system']['upload_dir'] . "vendors/")
            ->setSaveOriginal(false)
            ->setQuality(90);

        $logo = '';

        $files = $files->getFilesList();
        $renewLogo = count($files) ? true : false;

        foreach($files as $file){
            if($filePaths = $photoUploader->upload($file['tmp_name'])){
                $logo = $filePaths['filename'];
            }
        }

        $logoQ = ($vendor['logo']) ? $vendor['logo'] : '';
        if($renewLogo){
            if($vendor['logo']){
                $this->adminRemoveVendorLogo($vendorId);
            }

            $logoQ = $logo;
        }

        $table = $this->db_prefix . self::$_table['vendor'];

        $sql = "UPDATE {$table} SET
                `title` = '{$data['title']}',
                `title_url` = '{$title_url}',
                `cats_id` = '{$data['cat']}',
                `popular` = '{$popular}',
                `logo` = '{$logoQ}'
                WHERE `id` = '{$vendorId}'
                ";
        $this->inDB->query($sql);

        if($vendor['title'] != $data['title'] or $vendor['cats_vendor'] != $data['cat']){
            $table = $this->db_prefix . self::$_table['unit'];

            $sql = "UPDATE {$table} SET
                `title_vendor` = '{$data['title']}',
                `cats_vendor` = '{$data['cat']}'
                WHERE `id_vendor` = '{$vendorId}'
                ";
            $this->inDB->query($sql);
        }
        return true;
    }

    public function adminRemoveVendor($vendorId)
    {
        $vendor = $this->getVendor($vendorId);
        if(!$vendor){
            return false;
        }

        $table = $this->db_prefix . self::$_table['vendor'];

        $this->inDB->query("DELETE FROM {$table} WHERE `id` = '{$vendorId}'");

        return true;
    }

    public function adminRemoveVendorLogo($vendorId)
    {
        $vendor = $this->getVendor($vendorId);

        if($vendor['logo']){
            unlink($_SERVER['DOCUMENT_ROOT'] . $this->configs['system']['upload_dir'] . 'vendors/small/' . $vendor['logo']);

            $this->dbUpdate($vendorId, self::$_table['vendor'], "`logo` = ''");
        }

        return true;
    }

    public function adminAddModel($title, $vendor_id, $carcase_id, $popular = false)
    {
        $title_url = $this->ru2Lat($title);
        $popular = ($popular) ? 1 : 0;

        $vendor = $this->getVendor($vendor_id);
        if(!$vendor){
            $vendor_id = 0;
        }
        $carcase_id = in_array($carcase_id, $this->getCarcaseListIds()) ? $carcase_id : 0;

        $table = $this->db_prefix . self::$_table['model'];

        $sql = "INSERT INTO {$table} SET
                `title` = '{$title}',
                `title_url` = '{$title_url}',
                `vendor_id` = '{$vendor_id}',
                `case_id` = '{$carcase_id}',
                `popular` = '{$popular}'
                ";

        $this->inDB->query($sql);

        return true;
    }

    public function adminEditModel($modelId, $data)
    {
        $model = $this->getModel($modelId);

        if(!$model){
            return false;
        }
        $title = $data['title'];
        $title_url = $this->ru2Lat($data['title']);
        $popular = ($data['popular']) ? 1 : 0;
        $vendor_id = $data['vendor_id'];


        $vendor = $this->getVendor($vendor_id);
        if(!$vendor){
            return false;
        }

        $carcase_id = in_array($data['carcase_id'], $this->getCarcaseListIds()) ? $data['carcase_id'] : 0;

        $table = $this->db_prefix . self::$_table['model'];

        $sql = "UPDATE {$table} SET
                `title` = '{$title}',
                `title_url` = '{$title_url}',
                `vendor_id` = '{$vendor_id}',
                `case_id` = '{$carcase_id}',
                `popular` = '{$popular}'
                WHERE `id` = '{$modelId}'
                ";

        $this->inDB->query($sql);

        if($model['title'] != $title or $model['id_vendor'] != $vendor_id or $model['carcase_id'] != $carcase_id){
            $table = $this->db_prefix . self::$_table['unit'];

            $sql = "UPDATE {$table} SET
                `title_model` = '{$title}',
                `id_vendor` = '{$vendor_id}',
                `title_vendor` = '{$vendor['title']}',
                `carcase_id` = '{$carcase_id}'
                WHERE `id_model` = '{$modelId}'
                ";
            $this->inDB->query($sql);
        }

        return true;
    }

    public function adminRemoveModel($modelId)
    {
        $model = $this->getModel($modelId);
        if(!$model){
            return false;
        }

        $table = $this->db_prefix . self::$_table['model'];

        $this->inDB->query("DELETE FROM {$table} WHERE `id` = '{$modelId}'");

        return true;
    }

    public function adminRemoveAd($adId)
    {
        if(!$this->getAd($adId)){
            return false;
        }
        $photoList = $this->getPhotoList($adId, false);

        foreach($photoList as $photo){
            foreach($photo as $photoSize){
                unlink($_SERVER['DOCUMENT_ROOT'] . $photoSize);
            }
        }

        $table = $this->db_prefix . self::$_table['photo'];
        $sql = "DELETE FROM `{$table}`
                WHERE `id_unit` = '{$adId}'";
        $this->inDB->query($sql);

        $table = $this->db_prefix . self::$_table['unit'];
        $this->inDB->query("DELETE FROM {$table} WHERE `id` = '{$adId}'");

        return true;
    }

    // endregion admin

    private function removeDir($dir) {

        if ($objs = glob($dir."/*")) {
            foreach($objs as $obj) {
                is_dir($obj) ? $this->removeDir($obj) : unlink($obj);
            }
        }
        rmdir($dir);

    }

    /**
     * Функция для блокировки счетчика посещений для пользователя
     *
     * @param     $id
     * @param int $time
     *
     * @return int
     */
    public function viewCounter($id, $time = 1800)
    {
        $checkView =  $_SESSION['autoview'];
        if ($checkView){

            $newCheck = array();
            $result = 0;
            foreach ($checkView as $key => $value){
                if ($value > time()){
                    $newCheck[$key] = $value;
                }

                if ($key == $id and $value > time() ){
                    $result = 1;
                }

            }
            $newCheck[$id] = !$newCheck[$id] ? time() + $time : $newCheck[$id];
            $_SESSION['autoview'] = $newCheck;

        } else { // если данных о сессии еще не было

            $_SESSION['autoview'][$id] = time() + $time;
            $result = 0;

        }

        return $result;
    }

    /**
     * Функция для очистки HTML строки от лишних тэгов и аттрибутов
     *
     * @param        $html
     * @param string $clearTags
     * @param array  $allAttr
     *
     * @return mixed|string
     */
    static function clearHtml($html, $clearTags = '', $allAttr = array())
    {
        $html = strip_tags($html, $clearTags);

        if (preg_match_all("/<[^>]*\\s([^>]*)\\/*>/msiU", $html, $res, PREG_SET_ORDER)) {
            foreach ($res as $r) {
                $tag = $r[0];
                $attrs = array();
                preg_match_all("/\\s.*=(['\"]).*\\1/msiU", " " . $r[1], $split, PREG_SET_ORDER);
                foreach ($split as $spl) {
                    $attrs[] = $spl[0];
                }
                $newattrs = array();

                foreach ($attrs as $a) {
                    $tmp = explode("=", $a);
                    if (trim($a) != "" && (!isset($tmp[1]) || (trim($tmp[0]) != "" && !in_array(strtolower(trim($tmp[0])), $allAttr)))) {

                    } else {
                        $newattrs[] = $a;
                    }
                }

                $attrs = implode(" ", $newattrs);
                $rpl = str_replace($r[1], $attrs, $tag);
                $html = str_replace($tag, $rpl, $html);
            }
        }

        return $html;
    }

    /*
     *
     * функция для крона и для админки отвечает за количество объявлений в разделах и за пометки популярных разделов
     */
    public function CheckCountAndPopular($max_count_pop_vendor = 5, $start_popular_vendor = 8, $max_count_pop_models = 3, $start_popular_models = 7)
    {
        $sql = "UPDATE cms_auto_vendors as `v` SET count_advt = (SELECT count(*) FROM cms_auto_units WHERE v.id=id_vendor and `show` = 3)";
        $this->inDB->query($sql);

        $sql = "UPDATE cms_auto_models as `v` SET count_advt = (SELECT count(*) FROM cms_auto_units WHERE v.id=id_model and `show` = 3)";
        $this->inDB->query($sql);

        return true;
    }
}

// region help classes

class UploadedFiles{

    public $varname = 'files';

    public $multiple = true;

    private $_files = array();

    public function __construct()
    {
        $this->multiple = (@is_array($_FILES[$this->varname]['name'])) ? true : false;
    }

    public function getFilesList()
    {
        if($this->multiple){
            $count = count($_FILES[$this->varname]['name']);

            for($i= 0; $i < $count; $i++ ){
                $this->_files[] = array(
                    'name' => $_FILES[$this->varname]['name'][$i],
                    'type' => $_FILES[$this->varname]['type'][$i],
                    'tmp_name' => $_FILES[$this->varname]['tmp_name'][$i],
                    'error' => $_FILES[$this->varname]['error'][$i],
                    'size' => $_FILES[$this->varname]['size'][$i]
                );
            }
        } else {
            $this->_files[] = $_FILES[$this->varname];
        }

        return $this->_files;
    }

}

class PhotoUploader{

    public $_upload_dir      = '';			// директория загрузки

    private $_dist_dir = '';

    protected $_sizes = array();

    protected $_is_saveoriginal = false;

    protected $_quality         = 90;

    private $_images = array();

    public function setImages($imagesArray)
    {
        $this->_images = $imagesArray;

        return $this;
    }

    public function setSaveOriginal($boolean)
    {
        $this->_is_saveoriginal = (bool)$boolean;

        return $this;
    }

    public function setQuality($quality = 90)
    {
        $this->_quality = (int)$quality;

        return $this;
    }

    public function setUploadDir($path)
    {
        $this->_upload_dir = $path;
        $this->_dist_dir = PATH . $this->_upload_dir;

        return $this;
    }

    public function resize(Imagick $ImagickHandleObject, $resize_w, $resize_h, $thumb = true)
    {

        $bestfit = ($resize_w and $resize_h) ? true : false;

        $imageprops = $ImagickHandleObject->getImageGeometry();

        if ($imageprops['width'] >= $resize_w && $imageprops['height'] >= $resize_h) {

            if (!$thumb){
                $ImagickHandleObject->resizeImage($resize_w, $resize_h, imagick::FILTER_LANCZOS, 0.9, $bestfit);
            } else {
                $ImagickHandleObject->cropThumbnailImage($resize_w, $resize_h);
            }

        } else {

            //масштабировать фото?

        }

        return $ImagickHandleObject;

    }
    
    public function upload($file)
    {

        if (!getimagesize($file)) {
            return false;
        }

        $image = new Imagick($file);

        $image->setformat('jpg');

        $filename = sprintf('%s.%s', substr(md5(uniqid(mt_rand(), 1)), 0, 10), $image->getformat());

        if(!is_dir($this->_dist_dir)){
            mkdir($this->_dist_dir, 0777, true);
        }

        $data = array(
            'filename' => $filename,
            'images'   => array()
        );

        if ($this->_is_saveoriginal){
            if(!is_dir("{$this->_dist_dir}original")){
                mkdir("{$this->_dist_dir}original", 0777, true);
            }

            $image->writeImage("{$this->_dist_dir}original/$filename");

            $data['images']['original'] = "{$this->_upload_dir}original/$filename";
        }


        foreach($this->_images as $reqImage){
            $imgImagick = clone $image;

            if(!is_dir("{$this->_dist_dir}{$reqImage['name']}")){
                mkdir("{$this->_dist_dir}{$reqImage['name']}", 0777, true);
            }

            $this->resize($imgImagick , $reqImage['weight'], $reqImage['height'], $reqImage['thumb']);

            if($reqImage['watermark']){
                $imgImagick = $this->centerWatermark($imgImagick);
            }

            $imgImagick->setImageCompression(imagick::COMPRESSION_JPEG);
            $imgImagick->setImageCompressionQuality($this->_quality);
            $imgImagick->writeimage("{$this->_dist_dir}{$reqImage['name']}/{$filename}");

            $data['images'][$reqImage['name']] = "{$this->_upload_dir}{$reqImage['name']}/{$filename}";

            $imgImagick->destroy();
        }


        return $data;

    }

    private function centerWatermark(Imagick $imagickObject)
    {
            $watermark = new Imagick();
            $watermark->readImage(PATH . "/images/watermark.png");

            $iWidth = $imagickObject->getImageWidth();
            $iHeight = $imagickObject->getImageHeight();

            $watermark->scaleImage($iWidth*0.4, 0);
            $wWidth = $watermark->getImageWidth();
            $wHeight = $watermark->getImageHeight();

            $x = ($iWidth - $wWidth) / 2;
            $y = ($iHeight - $wHeight) / 2;

            $imagickObject->compositeImage($watermark, imagick::COMPOSITE_OVER, $x, $y);

            return $imagickObject;

    }


}

class DateIntervalEnhanced extends DateInterval {

    public function recalculate()
    {
        $from = new DateTime;
        $to = clone $from;
        $to = $to->add($this);
        $diff = $from->diff($to);
        foreach ($diff as $k => $v) $this->$k = $v;

        return $this;
    }

}

// endregion help classes

?>