DROP TABLE IF EXISTS `#__auto_answers`;
CREATE TABLE `#__auto_answers` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_question` int(10) NOT NULL,
  `id_unit` int(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_units` (`id_unit`),
  KEY `id_questions` (`id_question`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__auto_cats`;
CREATE TABLE `#__auto_cats` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `lvl` tinyint(1) NOT NULL,
  `title` varchar(80) NOT NULL,
  `logo` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `#__auto_models`;
CREATE TABLE `#__auto_models` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(80) NOT NULL,
  `vendor_id` int(2) NOT NULL,
  `case_id` int(2) NOT NULL,
  `popular` tinyint(1) NOT NULL DEFAULT '0',
  `count_advt` int(10) NOT NULL DEFAULT '0',
  `title_url` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `vendor_id` (`vendor_id`),
  KEY `case_id` (`case_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__auto_photos`;
CREATE TABLE `#__auto_photos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_unit` int(10) NOT NULL,
  `file` varchar(80) NOT NULL,
  `ordering` int(1) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_units` (`id_unit`),
  KEY `ismain_id_units` (`ordering`,`id_unit`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__auto_questions`;
CREATE TABLE `#__auto_questions` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_unit` int(10) NOT NULL,
  `author` varchar(80) NOT NULL,
  `id_user` int(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `is_new` int(1) NOT NULL,
  `show_type` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id_units` (`id_unit`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__auto_units`;
CREATE TABLE `#__auto_units` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ordering` int(10) DEFAULT NULL,
  `id_vendor` int(10) NOT NULL,
  `title_vendor` varchar(80) NOT NULL,
  `cats_vendor` int(10) NOT NULL,
  `id_model` int(10) NOT NULL,
  `title_model` varchar(80) NOT NULL,
  `carcase_id` int(10) NOT NULL,
  `date_construct` int(10) NOT NULL,
  `id_owner` int(10) NOT NULL,
  `price` int(10) NOT NULL,
  `currency` tinyint(1) NOT NULL,
  `volume` float(2,1) DEFAULT NULL,
  `fueltype` tinyint(4) NOT NULL,
  `transmission` tinyint(4) NOT NULL,
  `drive` tinyint(4) NOT NULL,
  `mileage` int(10) NOT NULL DEFAULT '0',
  `wheel` tinyint(4) NOT NULL,
  `is_without_rus_run` tinyint(1) NOT NULL,
  `is_new_auto` tinyint(1) NOT NULL,
  `is_broken` tinyint(1) NOT NULL,
  `is_undocumented` tinyint(1) NOT NULL,
  `is_cut` tinyint(1) NOT NULL,
  `is_gt` tinyint(1) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone1` varchar(20) NOT NULL,
  `phone2` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `edited_at` datetime DEFAULT NULL,
  `count_views` int(10) NOT NULL DEFAULT '0',
  `description` text,
  `gt_description` text,
  `region_id` int(10) NOT NULL,
  `region_title` varchar(100) NOT NULL,
  `city_id` int(10) NOT NULL,
  `city_title` varchar(80) NOT NULL,
  `access_issues` tinyint(2) NOT NULL DEFAULT '1',
  `auto_status` tinyint(2) NOT NULL DEFAULT '1',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  `is_vip` tinyint(1) DEFAULT '0',
  `vip_date_expiration` datetime DEFAULT NULL,
  `background_color` varchar(30) DEFAULT '0',
  `background_date_expiration` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_vendor` (`id_vendor`),
  KEY `id_model` (`id_model`),
  KEY `cats_vendor` (`cats_vendor`),
  KEY `price` (`price`),
  KEY `currency` (`currency`),
  KEY `ordering` (`ordering`),
  KEY `carcase_id` (`carcase_id`),
  KEY `vip` (`is_vip`),
  KEY `count_views` (`count_views`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__auto_vendors`;
CREATE TABLE `#__auto_vendors` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `cats_id` int(10) NOT NULL,
  `title` varchar(80) NOT NULL,
  `logo` varchar(80) NOT NULL,
  `popular` tinyint(1) NOT NULL DEFAULT '0',
  `count_advt` int(10) NOT NULL DEFAULT '0',
  `title_url` varchar(80) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cats_id` (`cats_id`),
  KEY `title` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
